<?php
// source: /var/www/html/bp/vendor/instante/bootstrap-3-renderer/src/Instante/Bootstrap3Renderer/@form.latte

class Templateaaeee24d1abf9ab589e1449ad1c35e4f extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('a8ab9807bf', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block form
//
if (!function_exists($_b->blocks['form'][] = '_lb5edf366c7c_form')) { function _lb5edf366c7c_form($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
;echo Nette\Bridges\FormsLatte\Runtime::renderFormBegin($form = $_form = is_object($form) ? $form : $_control[$form], array()) ?>


<?php call_user_func(reset($_b->blocks['errors']), $_b, get_defined_vars())  ?>

<?php call_user_func(reset($_b->blocks['body']), $_b, get_defined_vars()) ; echo Nette\Bridges\FormsLatte\Runtime::renderFormEnd($_form) ?>

<?php
}}

//
// block errors
//
if (!function_exists($_b->blocks['errors'][] = '_lb38ce875447_errors')) { function _lb38ce875447_errors($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
;$iterations = 0; foreach ($renderer->findErrors() as $error) { ?><div class="alert alert-danger">
    <a class="close" data-dismiss="alert">×</a><?php echo Latte\Runtime\Filters::escapeHtml($error, ENT_NOQUOTES) ?>

</div>
<?php $iterations++; } 
}}

//
// block body
//
if (!function_exists($_b->blocks['body'][] = '_lb1731e598bd_body')) { function _lb1731e598bd_body($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
;$iterations = 0; foreach ($iterator = $_l->its[] = new Latte\Runtime\CachingIterator($renderer->findGroups()) as $group) { ?>

<?php call_user_func(reset($_b->blocks['group']), $_b, get_defined_vars()) ; $iterations++; } array_pop($_l->its); $iterator = end($_l->its) ?>


<?php call_user_func(reset($_b->blocks['controls']), $_b, array('controls' => $renderer->findControls()) + get_defined_vars()) ;
}}

//
// block group
//
if (!function_exists($_b->blocks['group'][] = '_lb5071915870_group')) { function _lb5071915870_group($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?>    <fieldset<?php echo $group->attrs->attributes() ?>>
<?php if ($group->label) { ?>        <legend><?php echo Latte\Runtime\Filters::escapeHtml($group->label, ENT_NOQUOTES) ?></legend>
<?php } if ($group->description) { ?>        <p><?php echo Latte\Runtime\Filters::escapeHtml($group->description, ENT_NOQUOTES) ?></p>
<?php } ?>

<?php $controls = $group->controls ;if (isset($group->template) && $group->template) { $_b->templates['a8ab9807bf']->renderChildTemplate("$group->template", array('group' => $group, 'controls' => $controls, 'form' => $form, '_form' => $form, '__form' => $form) + $template->getParameters()) ?>

<?php } else { call_user_func(reset($_b->blocks['controls']), $_b, get_defined_vars())  ?>

<?php } ?>
    </fieldset>
<?php
}}

//
// block controls
//
if (!function_exists($_b->blocks['controls'][] = '_lb27244efd19_controls')) { function _lb27244efd19_controls($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
;$iterations = 0; foreach ($iterator = $_l->its[] = new Latte\Runtime\CachingIterator($controls) as $control) { if ($renderer->isSubmitButton($control)) { ?>
                <?php if ($iterator->first) { ?><div class="form-group"><div class="form-actions<?php if (isset($skipClass)) { ?>
 <?php echo Latte\Runtime\Filters::escapeHtml($skipClass, ENT_COMPAT) ?> <?php echo Latte\Runtime\Filters::escapeHtml($inputClass, ENT_COMPAT) ;} ?>
"><?php } ?>

                <?php $_input = is_object($renderer->getControlName($control)) ? $renderer->getControlName($control) : $_form[$renderer->getControlName($control)]; echo $_input->getControl() ?>

                <?php if (!$renderer->isSubmitButton($iterator->nextValue)) { ?>
</div></div><?php } ?>

<?php continue ;} if ($renderer->horizontalMode) { $attrs = array('input' => array(), 'label' => array('class' => $labelClass)) ;} else { $attrs = array('input' => array(), 'label' => array()) ;} ?>

<?php call_user_func(reset($_b->blocks['control']), $_b, get_defined_vars())  ?>

            <?php if ($renderer->isSubmitButton($iterator->nextValue)) { ?><div class="form-group"><div class="form-actions<?php if (isset($skipClass)) { ?>
 <?php echo Latte\Runtime\Filters::escapeHtml($skipClass, ENT_COMPAT) ?> <?php echo Latte\Runtime\Filters::escapeHtml($inputClass, ENT_COMPAT) ;} ?>
"><?php } ?>

<?php $iterations++; } array_pop($_l->its); $iterator = end($_l->its) ;
}}

//
// block control
//
if (!function_exists($_b->blocks['control'][] = '_lb01c4d02cf3_control')) { function _lb01c4d02cf3_control($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
;if ($_l->ifs[] = ($control->getOption('pairContainer'))) { ?>            <div<?php echo $control->getOption('pairContainer')->attributes() ?>>
<?php } $name = $renderer->getControlName($control);
                    $description = $renderer->getControlDescription($control);
                    $error = $renderer->getControlError($control) ?>

<?php if ($controlTemplate = $renderer->getControlTemplate($control)) { $_b->templates['a8ab9807bf']->renderChildTemplate("$controlTemplate", array('name' => $name, 'description' => $description, 'error' => $error, 'form' => $form, '_form' => $form, '__form' => $form, 'attrs' => $attrs) + $template->getParameters()) ?>

<?php } elseif ($renderer->isSubmitButton($control)) { ?>

                    <?php echo Latte\Runtime\Filters::escapeHtml($renderer::mergeAttrs($form[$name]->getControl(), $attrs['input']), ENT_NOQUOTES) ?>


<?php } elseif ($renderer->isButton($control)) { if ($_l->ifs[] = (isset($inputClass))) { ?>
                    <div class="<?php echo Latte\Runtime\Filters::escapeHtml($skipClass, ENT_COMPAT) ?>
 <?php echo Latte\Runtime\Filters::escapeHtml($inputClass, ENT_COMPAT) ?>">
<?php } ?>
                        <?php echo Latte\Runtime\Filters::escapeHtml($renderer::mergeAttrs($form[$name]->getControl(), $attrs['input']), ENT_NOQUOTES) ;echo Latte\Runtime\Filters::escapeHtml($error, ENT_NOQUOTES) ;echo Latte\Runtime\Filters::escapeHtml($description, ENT_NOQUOTES) ?>

<?php if (array_pop($_l->ifs)) { ?>                    </div>
<?php } ?>

<?php } elseif ($renderer->isCheckbox($control)) { ?>

<?php $label = $renderer::mergeAttrs($form[$name]->getLabel(), $attrs['label']) ;if ($_l->ifs[] = (!$renderer->controlHasClass($control, 'inline'))) { ?>
                    <div class="checkbox<?php if (isset($skipClass)) { ?> <?php echo Latte\Runtime\Filters::escapeHtml($skipClass, ENT_COMPAT) ?>
 <?php echo Latte\Runtime\Filters::escapeHtml($inputClass, ENT_COMPAT) ;} ?>">
<?php } ?>
                        <?php echo $label->startTag() ;echo Latte\Runtime\Filters::escapeHtml($renderer::mergeAttrs($form[$name]->getControl(), $attrs['input']), ENT_NOQUOTES) ;echo Latte\Runtime\Filters::escapeHtml($renderer->getLabelBody($control), ENT_NOQUOTES) ;echo $label->endTag() ;echo Latte\Runtime\Filters::escapeHtml($error, ENT_NOQUOTES) ;echo Latte\Runtime\Filters::escapeHtml($description, ENT_NOQUOTES) ?>

                        <?php if (array_pop($_l->ifs)) { ?>                    </div>
<?php } ?>

<?php } elseif ($renderer->isRadioList($control)) { ?>

                    <?php echo Latte\Runtime\Filters::escapeHtml($renderer::mergeAttrs($form[$name]->getLabel()->addClass("control-label"), $attrs['label']), ENT_NOQUOTES) ?>


<?php if ($_l->ifs[] = (isset($inputClass))) { ?>                    <div class="<?php echo Latte\Runtime\Filters::escapeHtml($inputClass, ENT_COMPAT) ?>">
<?php } $iterations = 0; foreach ($renderer->getRadioListItems($control) as $item) { ?>
                        <div class="radio">
                        <?php echo Latte\Runtime\Filters::escapeHtml($renderer::mergeAttrs($item->html, $attrs['input']), ENT_NOQUOTES) ?>

                        </div>
                    <?php $iterations++; } echo Latte\Runtime\Filters::escapeHtml($error, ENT_NOQUOTES) ;echo Latte\Runtime\Filters::escapeHtml($description, ENT_NOQUOTES) ?>

<?php if (array_pop($_l->ifs)) { ?>                    </div>
<?php } ?>

<?php } elseif ($renderer->isCheckboxList($control)) { ?>

                    <?php echo Latte\Runtime\Filters::escapeHtml($renderer::mergeAttrs($form[$name]->getLabel()->addClass("control-label"), $attrs['label']), ENT_NOQUOTES) ?>


<?php if ($_l->ifs[] = (isset($inputClass))) { ?>                    <div class="<?php echo Latte\Runtime\Filters::escapeHtml($inputClass, ENT_COMPAT) ?>">
<?php } $iterations = 0; foreach ($renderer->getCheckboxListItems($control) as $item) { ?>
                        <div class="checkbox">
                        <?php echo Latte\Runtime\Filters::escapeHtml($renderer::mergeAttrs($item->html, $attrs['input']), ENT_NOQUOTES) ?>

                        </div>
                    <?php $iterations++; } echo Latte\Runtime\Filters::escapeHtml($error, ENT_NOQUOTES) ;echo Latte\Runtime\Filters::escapeHtml($description, ENT_NOQUOTES) ?>

<?php if (array_pop($_l->ifs)) { ?>                    </div>
<?php } ?>

<?php } else { ?>

                    <?php echo Latte\Runtime\Filters::escapeHtml($renderer::mergeAttrs($form[$name]->getLabel(), $attrs['label']), ENT_NOQUOTES) ?>


<?php $prepend = $control->getOption('input-prepend'); $append = $control->getOption('input-append') ;if ($_l->ifs[] = (isset($inputClass))) { ?>
                    <div class="<?php echo Latte\Runtime\Filters::escapeHtml($inputClass, ENT_COMPAT) ?>">
<?php } if ($_l->ifs[] = ($prepend || $append)) { ?>                        <div class="input-group">
<?php } ?>
                        
<?php if ($_l->ifs[] = ($prepend)) { ?>                            <div class="input-group-addon"><?php } echo Latte\Runtime\Filters::escapeHtml($prepend, ENT_NOQUOTES) ;if (array_pop($_l->ifs)) { ?></div>
<?php } ?>
                            <?php echo Latte\Runtime\Filters::escapeHtml($renderer::mergeAttrs($form[$name]->getControl(), $attrs['input']), ENT_NOQUOTES) ?>

<?php if ($_l->ifs[] = ($append)) { ?>                            <div class="input-group-addon"><?php } echo Latte\Runtime\Filters::escapeHtml($append, ENT_NOQUOTES) ;if (array_pop($_l->ifs)) { ?></div>
<?php } if (array_pop($_l->ifs)) { ?>                        </div>
<?php } ?>
                        <?php echo Latte\Runtime\Filters::escapeHtml($error, ENT_NOQUOTES) ;echo Latte\Runtime\Filters::escapeHtml($description, ENT_NOQUOTES) ?>

<?php if (array_pop($_l->ifs)) { ?>                    </div>
<?php } } if (array_pop($_l->ifs)) { ?>            </div>
<?php } 
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIMacros::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); } ?>

<?php if (!isset($mode)) { call_user_func(reset($_b->blocks['form']), $_b, array('form' => $form, 'renderer' => $renderer) + get_defined_vars()) ;} 
}}