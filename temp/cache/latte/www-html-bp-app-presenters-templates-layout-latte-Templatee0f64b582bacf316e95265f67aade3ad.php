<?php
// source: /var/www/html/bp/app/presenters/templates/@layout.latte

class Templatee0f64b582bacf316e95265f67aade3ad extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('b0ab23709c', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block _msgs
//
if (!function_exists($_b->blocks['_msgs'][] = '_lbd72af147ef__msgs')) { function _lbd72af147ef__msgs($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v; $_control->redrawControl('msgs', FALSE)
;echo Latte\Runtime\Filters::escapeHtml($notReadMsgs, ENT_NOQUOTES) ;
}}

//
// block _nots
//
if (!function_exists($_b->blocks['_nots'][] = '_lbe91e008488__nots')) { function _lbe91e008488__nots($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v; $_control->redrawControl('nots', FALSE)
;echo Latte\Runtime\Filters::escapeHtml($notReadNots, ENT_NOQUOTES) ;
}}

//
// block _flashes
//
if (!function_exists($_b->blocks['_flashes'][] = '_lb93fb5b43f7__flashes')) { function _lb93fb5b43f7__flashes($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v; $_control->redrawControl('flashes', FALSE)
;$iterations = 0; foreach ($flashes as $flash) { ?>							<div class="alert alert-<?php echo Latte\Runtime\Filters::escapeHtml($flash->type, ENT_COMPAT) ?>  .alert-dismissible">
											<?php echo Latte\Runtime\Filters::escapeHtml($flash->message, ENT_NOQUOTES) ?>

						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										</div>
<?php $iterations++; } 
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIMacros::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?php echo Latte\Runtime\Filters::escapeHtml($mainTitle, ENT_NOQUOTES) ?>
 | <?php Latte\Macros\BlockMacrosRuntime::callBlock($_b, 'title', $template->getParameters()) ?></title>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/img/icon.png">
		<link rel=“apple-touch-icon“ sizes=“72×72″ href=“<?php echo '"' . Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) . '"' ?>/img/logo72.png“>
        <link rel=“apple-touch-icon“ sizes=“114×114″ href=“<?php echo '"' . Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) . '"' ?>/img/logo114.png“>
		<meta name="description" content="Studentská platforma virtuálního herbáře!">
		<meta name="author" content="JiriSemmler.eu">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/plugins/bootstrap/bootstrap.css" rel="stylesheet">
				<link href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/css/jquery-ui.css" rel="stylesheet">
		<link href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/css/font-awesome.min.css" rel="stylesheet">
		<link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
		<link href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/css/style.css" rel="stylesheet">
		<link href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/css/fineuploader-5.0.5.css" rel="stylesheet">
		<link href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/css/tree.css" rel="stylesheet">
		<link href="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/css/main.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
				<script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
				<script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/cs_CZ/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="modalbox">
	<div class="devoops-modal">
		<div class="devoops-modal-header">
			<div class="modal-header-name">
				<span>Basic table</span>
			</div>
			<div class="box-icons">
				<a class="close-link">
					<i class="fa fa-times"></i>
				</a>
			</div>
		</div>
		<div class="devoops-modal-inner">
		</div>
		<div class="devoops-modal-bottom">
		</div>
	</div>
</div>
<header class="navbar">
	<div class="container-fluid expanded-panel">
		<div class="row">
			<div id="logo" class="col-xs-12 col-sm-2">
				<a href="<?php echo Latte\Runtime\Filters::escapeHtml($_presenter->link("Public:default"), ENT_COMPAT) ?>">HerbariumSystem</a>
			</div>
			<div id="top-panel" class="col-xs-12 col-sm-10">
				<div class="row">
					<div class="col-xs-7 col-sm-4">
						<div id="search">
						<form method="post" action="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Public:search"), ENT_COMPAT) ?>" class="search">
							<input type="text"  name="search" class="search" placeholder="hledej..." onkeydown="sendSearch()" <?php if (isset($searchExp)) { ?>
value="<?php echo Latte\Runtime\Filters::escapeHtml($searchExp, ENT_COMPAT) ?>"<?php } ?>>
														<input type="submit" id="searchButton" name="submit" value="Odeslat">
						</form>
						</div>
					</div>
					<div class="col-xs-5 col-sm-8 top-panel-right">
						<a href="#" class="about tomodal" data-toggle="modal" data-target="AboutModal">o aplikaci</a>


						<ul class="nav navbar-nav pull-right panel-menu">
<?php if (isset($help)) { ?>
							<li class="hidden-xs">
								<a  href="#" title="Nápověda" class="tomodal" data-toggle="modal" data-target="helpModal" class="modal-link">
									<i class="fa fa-question"></i>
								</a>
							</li>
<?php } if ($logged == true) { if (isset($notReadMsgs) && isset($notReadNots)) { ?>


							<li class="hidden-xs">

								<a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Student:messages"), ENT_COMPAT) ?>" class="modal-link">
									<i class="fa fa-envelope"></i>

									<span class="badge"><?php if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); } ?>
<div id="<?php echo $_control->getSnippetId('msgs') ?>"><?php call_user_func(reset($_b->blocks['_msgs']), $_b, $template->getParameters()) ?>
</div></span>

								</a>

							</li>


							<li class="hidden-xs">
								<a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Student:messages", array('nots')), ENT_COMPAT) ?>">
									<i class="fa fa-bell"></i>
									<span class="badge"><div id="<?php echo $_control->getSnippetId('nots') ?>
"><?php call_user_func(reset($_b->blocks['_nots']), $_b, $template->getParameters()) ?>
</div></span>
								</a>
							</li>
<?php } ?>

							<li class="dropdown">
								<a href="#" class="dropdown-toggle account" data-toggle="dropdown">
									<div class="avatar">
										<img src="<?php echo Latte\Runtime\Filters::escapeHtml($_presenter->link("Image:avatar", array($profileID, 'small')), ENT_COMPAT) ?>
" alt="<?php echo Latte\Runtime\Filters::escapeHtml($name, ENT_COMPAT) ?>" class="img-circle" alt="avatar">
									</div>
									<i class="fa fa-angle-down pull-right"></i>
									<div class="user-mini pull-right">
										<span class="welcome">Vítej,</span>
										<span><?php echo Latte\Runtime\Filters::escapeHtml($name, ENT_NOQUOTES) ?></span>
									</div>
								</a>
								<ul class="dropdown-menu">
									<li>
										<a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Student:profile"), ENT_COMPAT) ?>">
											<i class="fa fa-user"></i>
											<span>Profil</span>
										</a>
									</li>
									<li>
										<a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Public:units"), ENT_COMPAT) ?>">
											<i class="fa fa-list-alt"></i>
											<span>Položky</span>
										</a>
									</li>

									<li>
										<a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Login:out"), ENT_COMPAT) ?>">
											<i class="fa fa-power-off"></i>
											<span>Odhlásit se</span>
										</a>
									</li>
								</ul>
							</li>
<?php } else { ?>
							<li class="hidden-xs">
								<a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Login:register"), ENT_COMPAT) ?>" class="modal-link">
									<i class="fa fa-user-plus"></i>

								</a>
							</li>
							<li class="hidden-xs">
								<a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Login:default"), ENT_COMPAT) ?>" class="modal-link">
									<i class="fa fa-sign-in"></i>

								</a>
							</li>
<?php } ?>
						</ul>
											</div>
				</div>
			</div>
		</div>
	</div>
</header>
<!--End Header-->
<!--Start Container-->
<div id="main" class="container-fluid">
	<div class="row">
		<div id="sidebar-left" class="col-xs-2 col-sm-2">
			<ul class="nav main-menu">
<?php $iterations = 0; foreach ($menu as $item) { if (empty($item['sub'])) { ?>
				<li>
					<a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link($item['link']), ENT_COMPAT) ?>
" title="<?php echo Latte\Runtime\Filters::escapeHtml($item['help'], ENT_COMPAT) ?>" data-placement="right">
						<i class="fa fa-<?php echo Latte\Runtime\Filters::escapeHtml($item['icon'], ENT_COMPAT) ?>"></i>
						<span class="hidden-xs"><?php echo Latte\Runtime\Filters::escapeHtml($item['name'], ENT_NOQUOTES) ?></span>
					</a>
				</li>
<?php } else { ?>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle">
						<i class="fa fa-<?php echo Latte\Runtime\Filters::escapeHtml($item['icon'], ENT_COMPAT) ?>"></i>
						<span class="hidden-xs"><?php echo Latte\Runtime\Filters::escapeHtml($item['name'], ENT_NOQUOTES) ?></span>
					</a>
					<ul class="dropdown-menu">

<?php $iterations = 0; foreach ($item['sub'] as $subitem) { ?>						<li>
							<a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link($subitem['link']), ENT_COMPAT) ?>
"  title="<?php echo Latte\Runtime\Filters::escapeHtml($subitem['help'], ENT_COMPAT) ?>" data-placement="right">
								<i class="fa fa-<?php echo Latte\Runtime\Filters::escapeHtml($subitem['icon'], ENT_COMPAT) ?>"></i>
								<span class="hidden-xs"> <?php echo Latte\Runtime\Filters::escapeHtml($subitem['name'], ENT_NOQUOTES) ?></span>
							</a></li>
<?php $iterations++; } ?>

					</ul>
				</li>
<?php } $iterations++; } ?>




			</ul>
		</div>
		<!--Start Content-->
		<div id="content" class="col-xs-12 col-sm-10">
			<!--Start Breadcrumb-->
			<div class="row">
				<div id="breadcrumb" class="col-xs-12">
					<a href="#" class="show-sidebar">
						<i class="fa fa-bars"></i>
					</a>
					<ol class="breadcrumb pull-left">
<?php if (empty($path)) { ?>
						<li>Herbarium system</li>
<?php } else { $iterations = 0; foreach ($path as $pathItem) { ?>						<li>
<?php if ($pathItem['link'] == NULL) { ?>
							<?php echo Latte\Runtime\Filters::escapeHtml($pathItem['name'], ENT_NOQUOTES) ?>

<?php } else { if (isset($pathItem['arg'])) { ?>

								<a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link($pathItem['link'], array($pathItem['arg'])), ENT_COMPAT) ?>">
									<?php echo Latte\Runtime\Filters::escapeHtml($pathItem['name'], ENT_NOQUOTES) ?>

								</a>
<?php } else { ?>
								<a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link($pathItem['link']), ENT_COMPAT) ?>">
	                              <?php echo Latte\Runtime\Filters::escapeHtml($pathItem['name'], ENT_NOQUOTES) ?>

	                            </a>
<?php } } ?>
						</li>
<?php $iterations++; } } ?>
											</ol>
					<div id="social" class="pull-right">
																		<a href="http://www.facebook.com/sharer.php?u=<?php echo Latte\Runtime\Filters::escapeHtml($_control->link('//this'), ENT_COMPAT) ?>" target="_blank"><i class="fa fa-facebook"></i></a>



																							</div>
				</div>
			</div>
			<!--End Breadcrumb-->
										
			<div id="dashboard-header" class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<h3><?php Latte\Macros\BlockMacrosRuntime::callBlock($_b, 'header', $template->getParameters()) ?></h3>
				</div>
				<div class="clearfix visible-xs"></div>
				<div class="col-xs-12 col-sm-6 col-md-6 pull-right">
					<div class="row">
						<div class="col-xs-8" style="padding-top: 10px">
<div id="<?php echo $_control->getSnippetId('flashes') ?>"><?php call_user_func(reset($_b->blocks['_flashes']), $_b, $template->getParameters()) ?>
</div>
						</div>


					</div>
				</div>
			</div>



<?php Latte\Macros\BlockMacrosRuntime::callBlock($_b, 'content', $template->getParameters()) ?>
			<div id="ajax-content"></div>
		</div>
		<!--End Content-->
	</div>
</div>
<!--End Container-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="http://code.jquery.com/jquery.js"></script>-->
<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/jquery.js"></script>
<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/jquery-ui.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/plugins/bootstrap/bootstrap.js"></script>

<!-- All functions for this theme + document.ready processing -->
<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/devoops.js"></script>

<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/netteForms.js"></script>

<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/jquery.ajaxform.js"></script>
<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/nette.ajax.js"></script>
<script src="http://ethaizone.github.io/Bootstrap-Confirmation/assets/js/bootstrap-tooltip.js"></script>
<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/plugins/datatables/ZeroClipboard.js"></script>
<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/plugins/datatables/TableTools.js"></script>
<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/bootstrap-confirmation.js"></script>
<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/fineuploader/jquery.fineuploader-5.0.5.js"></script>
<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/jstree.js"></script>


<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/treeInit.js"></script>
<?php Latte\Macros\BlockMacrosRuntime::callBlock($_b, 'scripts', $template->getParameters()) ?>
<script src="<?php echo Latte\Runtime\Filters::escapeHtml(Latte\Runtime\Filters::safeUrl($basePath), ENT_COMPAT) ?>/js/main.js"></script>


<div class="modal fade" id="AboutModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">
			O aplikaci
		</h4>
	  </div>
	  <div class="modal-body">
	  <b>Herbarium System</b><br>
	  Informační systém pro evidování
	Tato aplikace vznikla jako bakalářská práce na FIT VUT.<br>
	<br>
	Jedná se o virtuální digitální herbář pro studenty středních škol.
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>

	  </div>
	</div>
  </div>
</div>

<?php if (isset($help)) { ?>
<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="myModalLabel">
			Nápověda - <?php echo Latte\Runtime\Filters::escapeHtml($help->title, ENT_NOQUOTES) ?>

		</h4>
	  </div>
	  <div class="modal-body">
		<?php echo $help->text ?>

	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>

	  </div>
	</div>
  </div>
</div>
<?php } ?>


</body>
</html>
<?php
}}