<?php

namespace App\Presenters;

use App\Model\Repositories\FileRepository;
use App\Model\Repositories\UserRepository;
use App\Model\Entities\User;
use Nette,
	App\Forms\RegisterFormFactory,
    App\Model\UserManager,
    App\Forms\LoginFormFactory,
    App\Forms\ForgottenPasswordFormFactory;
use    Nette\Mail\Message,
    Nette\Mail\SendmailMailer;
use App\Model\UploadHandler;
use Nette\Utils\Image;

/**
 * Sign in/out presenters.
 */
class LoginPresenter extends BasePresenter
{
	/** @var LoginFormFactory @inject */
	public $loginFactory;

    /** @var RegisterFormFactory @inject */
    public $registerFactory;

    /** @var FileRepository @inject */
    public $fileRepository;

    /** @var UserManager @inject */
    public $model;
    /** @var UserRepository @inject */
    public $userRepository;

    /** @var User @inject */
    public $oneUser;

    /** @var ForgottenPasswordFormFactory @inject */
    public $forgottenPasswordFormFactory;



    /**
     * renders login page
     */
    public function renderDefault()
    {
	    $this->template->help = $this->helpRepository->getHelp("login");
        $this->template->title = "Přihlášení";
        $this->template->header = "Přihlášení";
    }

    /**
	 * Login form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentLoginForm()
	{
		$form = $this->loginFactory->create();
        $form->onSuccess[] = array($this, 'loginSuccess');

		return $form;
	}

    /**
     * method to login user
     * @param $form
     * @internal param $values - data from form
     */
    public function loginSuccess($form)
    {
        $values = $form->getValues();
        try{
            $this->getUser()->login($values->email,$values->password);
            $roles = $this->getUser()->getRoles();
            if($roles[0] == "student")
            {
                $this->redirect("Student:");
            }
            else if ($roles[0] == "teacher")
            {
                $this->redirect("Teacher:");
            }

        }
        catch(Nette\Security\AuthenticationException $e)
        {
            $this->flashMessage($e->getMessage(),"warning");
        }

    }

    /**     ---^^^--- Login ---^^^^--- */
    /**     ---vvv--- REGISTRATION ---vvv--- */

    /**
     * renders registration page
     */
    public function actionRegister()
    {
        $this->template->title = "Registrace";
        $this->template->header = "Registrace";
//
	    $this->template->help = $this->helpRepository->getHelp("reg");
	    $position = ["name"=>"Registrace","link"=>"Login:register"];
	    $this->template->path = $position;

    }

	/**
	 * Register form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentRegisterForm()
	{
		$form = $this->registerFactory->create();
        $form->onSuccess[] = array($this, 'registerSucceed');

		return $form;
	}

    /**
     * method to register a new user
     * @param $form 
     * @param $values - data from form
     */
    public function registerSucceed($form,$values)
    {
        if(!$this->model->isRegistrationTime())
        {
            $this->flashMessage("Omlouváme se, ale registrace není otevřena.","danger");
            $this->redirect("Login:");
        }
        $ret = $this->model->registerUser($values);

        switch($ret)
        {
            case "OK":
            {
	            $body = "
				   Dobrý den,

				   byl(a) jste registrována na serveru herbarium-system.
				   Váš přihlašovací email je ".$values->email."

				   Herbarium-system Team
				   ";

	            $msg = new Message;
	            $msg->setFrom("registrace@herbarium-system.eu")
		            ->addTo( $values->email)
		            ->setSubject("Registrace na Herbarium-System")
		            ->setBody($body);


	            $mailer = new SendmailMailer;

	            $mailer->send($msg); //send mail to user
                $this->flashMessage("Výborně, byl(a) jste úspěšně zaregistrován(a)!","success");
            };break;

            case "DUPLICATE":
            {
                $this->flashMessage("Bohužel Váš email již v systému zaveden. Zkuste zvolit jiný.","warning");
	            $this->redirect("Login:register");
            };break;

            case "ERR":
            {
                $this->flashMessage("Bohužel došlo k neznámé chybě. Kontaktujte prosím administrátora","danger");
            };
        }
        $this->redirect("Login:default");
    }

    /**     ---^^^--- REGISTRATION ---^^^^--- */
    /**     ---vvv--- forgotten pass ---vvv--- */

    /**
     * action for forgotten password - shows form for ASK for change pass
     */
    public function actionForgottenPassword()
    {
	    $this->template->help = $this->helpRepository->getHelp("fogpass");
        $this->template->title = "Zapomenuté heslo";
        $this->template->header = $this->template->title;
    }

    /**
     * ForgottenPassword form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentForgottenPasswordForm()
    {
        $form = $this->forgottenPasswordFormFactory->createAskForPass();
        $form->onSuccess[] = array($this, 'askForPassSuccess');

        return $form;
    }

    /**
     * send data to model to generate link
     * @param $form
     * @param $values - data from form
     *
     */
    public function askForPassSuccess($form,$values)
    {
        $token = $this->model->generateResetLink($values);
        if($token != false)
        {
            $body = "Dobrý den,
             na herbarium-system.eu byl evidován požadavek na změnu hesla Vašeho účtu. Pro změnu hesla pokračujte na následujícím odkazu: ".$this->link("//Login:changePassword",$token)."

             Pokud si nejste vědom(a) tohoto požadavku, prosím tento email ignorujte.

             herbarium-systen Team
             ";

            $msg = new Message;
            $msg->setFrom("noreply@herbarium-system.eu")
                ->addTo( $values->email)
                ->setSubject("Reset hesla herbarium-system.eu")
                ->setBody($body);

            $mailer = new SendmailMailer;
            $mailer->send($msg);

            $this->flashMessage("Výborně! Na zadaný email byla odeslána zpráva s dalším krokem resetu hesla.","success");
        }
        else
        {
            $this->flashMessage("Omlouváme se, ale uživatel se zadaným emailem nebyl nalezen.","warning");
        }
    }

    /**     ---^^^--- forgotten pass ---^^^^--- */
    /**     ---vvv--- change pass ---vvv--- */

    /**
     * action for forgotten password - shows form for change password
     * @param $token - security token
     */
    public function actionChangePassword($token)
    {
        $this->template->title = "Změna hesla";
        $this->template->header = $this->template->title;

        if($this->model->isTokenValid($token))
        {

        }
        else
        {
            $this->flashMessage('Bezpečnostni token neni valiudni');
            $this->redirect("Login:");
        }
    }

    /**
     * ForgottenPassword form factory.
     * @return Nette\Application\UI\Form
     */
    protected function createComponentChangePasswordForm()
    {
        $form = $this->forgottenPasswordFormFactory->createChangePass();
        $form->onSuccess[] = array($this, 'changePassSuccess');

        return $form;
    }

    /**
     * writing data form change of password
     * @param $form - form where data come from
     * @internal param $values - data
     */
    public function changePassSuccess($form)
    {
        $values = $form->getValues();

        if($this->model->updateUser(NULL,$values))
        {
            $this->flashMessage("Heslo bylo úspěšně změněno!!","success");
        }
	    else
	    {
		    $this->flashMessage(":( Došlo k chybě a něco se pokazilo, zkus to oznámit správci","warning");
	    }
    }

    /**** ^^^^ forgotten password ^^^^ ****/
    /**** vvvv update user vvvv ****/


    /**
     * action to logout
     */
	public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('Byl(a) jsi úspěšně odhlášen(a).');
		$this->redirect('Public:');
	}

}
