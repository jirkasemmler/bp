<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 12.4.15
 * Time: 22:37
 */

namespace App\Presenters;
use App\Model\Repositories\AttributeRepository;
use App\Model\Repositories\GradeRepository;
use App\Model\Repositories\DirectoryRepository;
use App\Model\Repositories\UnitRepository;
use App\Model\Repositories\TaxonomyRepository;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\FileRepository;
use App\Model\Repositories\MessageRepository;
use App\Model\Repositories\NotificationRepository;
use App\Forms\UnitFormFactory;
use App\Model\UploadHandler;

use Nette;
class PublicPresenter extends BasePresenter{

	/** @var GradeRepository @inject */
	public $gradeRepository;

	public $searchData = NULL;
	public $unitId = 0;
	/** @var AttributeRepository @inject */
	public $attributeRepository;

	/** @var TaxonomyRepository @inject */
	public $taxonomyRepository;

	/** @var DirectoryRepository @inject */
	public $directoryRepository;

	/** @var UnitRepository @inject */
	public $unitRepository;
	/** @var FileRepository @inject */
	public $fileRepository;

	/** @var UserRepository @inject */
	public $userRepository;

	/** @var MessageRepository @inject */
	public $messageRepository;

	/** @var NotificationRepository @inject */
	public $notificationRepository;

	/** @var UnitFormFactory @inject */
	public $unitFormFactory;

	public $user; //internal variables
	public $dirID = 0;
	public $dirFormId = 0;
	public $threadId = 0;
	public $type = "";
	private $typesOfDirsToShow;
	public function beforeRender()
	{
		$typesToShow= [
			"student"=>["public","school"],
			"teacher"=>["public","school"],
			"public"=>["public"]
		]; //for each role I can show just few types of dir

		parent::beforeRender();
		if($this->getUser()->isLoggedIn())
		{
			$role = $this->getUser()->roles[0];

			$this->template->notReadMsgs = $this->messageRepository->getNotReadMsg($this->getUser()->id);
			$this->template->notReadNots = $this->notificationRepository->findBy(["user_id"=>$this->getUser()->id,"read"=>false])->count();
		}
		else
		{
			$role = "public";
		}
		$this->typesOfDirsToShow = $typesToShow[$role];
		$this->template->role = $role;
	}


	/**
	 * renders nothing but everything
	 */
	public function renderDefault()
	{
		$this->template->help = $this->helpRepository->getHelp("homepage");

		$this->template->title = "Student";
		$this->template->header = "Vítej v herbariu!";

		$this->template->units = $this->unitRepository->getRandomUnits(); //show 9 random units on the front
	}

	/**
	 * renders list of set grades in the system
	 */
	public function renderGrades()
	{
		$this->template->title = "Třídy";
		$this->template->header = "Třídy v systému";

		$position = ["name"=>"Herbářové položky","link"=>NULL];
		$this->template->path[] = $position;
		$position = ["name"=>"Dle tříd","link"=>"Public:grades"];
		$this->template->path[] = $position;

		$this->template->grades = $this->gradeRepository->findAll();
		$this->template->addGrades = $this->gradeRepository->getAdditional();

	}

	/**
	 * renders list of students in grade (by the ID) . if not, then all of them
	 * @param null $id - ID of grade
	 */
	public function renderStudents($id = NULL)
	{
		$this->template->title = "Studenti";
		$this->template->header = "Studenti v systému";

		$position = ["name" => "Herbářové položky", "link" => NULL];
		$this->template->path[] = $position;
		$position = ["name" => "Dle studentů", "link" => "Public:students"];
		$this->template->path[] = $position;


		$this->template->users = $this->userRepository->getStudents($id);
		if ($this->getUser()->roles[0] == "teacher")
		{
			$this->template->teacher = true;
				$this->template->addTeacherUsers = $this->userRepository->getAdditionalUsers($id, "school");
		}
		$this->template->addUsers = $this->userRepository->getAdditionalUsers($id);
	}

	// UNITS

	/**
	 * renders table of units of set directory
	 * @param $id - id of directory
	 */
	public function renderUnits($id = 0)
	{
		$faTypes = ["public"=>"users","private"=>"user","school"=>"university"];
		$this->template->types = $faTypes;
		/*
		 * there are three use cases where I need to render units
		 * a) $id = 0 -> it is my account -> get first directory by logged account ID
		 * b) $id != 0 and $id is id of directory which units to show. In that case I need to get Id of user and other directories by the set $id
		 * c) it is called by handler and $id = 0 but $this->dirID != 0 -> get everything as in the case b) but due the $this->dirID
		 */
		$idUser = 0;
		if($id == 0 && $this->dirID == 0) //I have no ID -> get directories of logged user a)
		{
			if(!$this->getUser()->isLoggedIn())
			{
				$this->redirect("Login:");
			}
//			$usersDirs = $this->directoryRepository->findByUserId($this->getUser()->id); //get the dirs by user
			$usersDirs = $this->directoryRepository->findBy(["NOT status"=>"deleted","user_id"=>$this->getUser()->id]);

			$idUser = $this->getUser()->id; //set user ID
			$this->dirID = $usersDirs->toArray()[0]->id; //set $this->dirID which is user later
		}
		else// I dont know the user but I know ID of directory
		{
			if($id != 0) // b) lets set the dirID
			{
				$this->dirID = $id;
			}

			$idUser = $this->directoryRepository->getById($this->dirID)->userId; //b) || c) - dirID is already set and I get idUesr by the directory
		}

		if($idUser == $this->getUser()->id)
		{

			$this->template->help = $this->helpRepository->getHelp("unitsOwner");
			$this->template->showAddEdit = true;
			$position = ["name"=>"Moje položky","link"=>"Public:units"];
			$this->template->header = "Moje položky";
		}
		else
		{
			$name = $this->userRepository->getById($idUser)->name;
			$position = ["name"=>"Položky - ".$name,"link"=>"Public:units","arg"=>$this->dirID];
			$this->template->header = "Položky - ".$name;
		}
//		echo $id;
		$thisDir = $this->directoryRepository->getById($this->dirID);
		if($thisDir->userId == $this->getUser()->id && $this->getUser()->isLoggedIn())
		{
			$this->typesOfDirsToShow[] = "private";
		}
		$dirs = $this->directoryRepository->findBy(["NOT status"=>"deleted","user_id"=>$idUser,"type"=>$this->typesOfDirsToShow]); //get all the directories of user

		$this->template->directories = $dirs; // sending dirs to template
		if( in_array($thisDir->type,$this->typesOfDirsToShow))
		{
			$units = $this->unitRepository->getUnits($this->dirID,$thisDir->type); // getting units of the directory
		}
		else
		{
			$units = [];
		}
		$this->template->type = $thisDir->type;
		$tmpRating = $thisDir->avgRating();
		$this->template->avgRating = $tmpRating["avg"];
		$this->template->rated = $tmpRating["rated"];
		$this->template->dirId = $thisDir->id;

		if($thisDir->rating == 0)
		{
			$rating = "-";
			$comment = "-";
		}
		else
		{
			$rating = $thisDir->rating." %";
			$comment = $thisDir->comment;
		}
		$this->template->dirRating = $rating;
		$this->template->dirComment = $comment;

		$this->template->units = $units; //sending to template vvvvv
		$this->template->dirID = $this->dirID;

		// have dirs and units vvvvvvvv

		$this->template->title = "Student | Položky";

		//get attributes to show
		$attributes = $this->attributeRepository->findBy(["show_in_table"=>true,"state"=>true])->orderBy("order");
		$this->template->attributes = $attributes;
		$this->template->activeDir = $this->dirID;

		$this->template->path[] = $position;
	}

	/**
	 * @param $data - data from search form
	 */
	public function renderSearch($data = NULL)
	{
		if($this->searchData == NULL )
		{
			$this->searchData = $_POST;

		}

		$data = $this->searchData;
		$this->template->title = "Student | Položka";
		$this->template->header = "Výsledky hledání";
		$this->template->showSearchBox = true;
		$this->setView("units"); //use the same view as units

		$attributes = $this->attributeRepository->findBy(["show_in_table"=>"true","state"=>true])->orderBy("order"); //which attributes to show?
		$this->template->attributes = $attributes;

		if(isset($data["search"]))
		{
			$this->template->searchExp = $data["search"];
		}
		$units = $this->unitRepository->search($data,$this->getUser()->roles[0]); //search units

		$this->template->units = $units;
	}

	/**
	 * creates form for searching
	 * @return \Nette\Application\UI\Form
	 */
	public function createComponentSearchBox()
	{
		$form = $this->unitFormFactory->createSearch();
		$form->onSubmit[] = array($this, 'searchSuccess');

		return $form;
	}

	/**
	 * searched
	 * @param $form
	 */
	public function searchSuccess($form)
	{
		$this->searchData = $form->getValues(TRUE);
		if ($this->presenter->isAjax())
		{
			$this->redrawControl('unitSnippet');
		}
	}

	/**
	 * handler saves ID of directory and invalidates snippet for units to load new units due to selected directory.
	 * @param $id
	 */
	public function handleGetUnits($id) //id - id directory
	{
		$this->dirID = $id; //saving ID of directory
		$this->template->dirID = $id; //sending ID of directory to template to link for new unit. not so good looking solution but the easiest

		if ($this->presenter->isAjax()) //redraw snippets for units to load new data from render due new ID of directory
		{
			$this->redrawControl("unitSnippet");
			$this->redrawControl("EditSnippet");
			$this->redrawControl("newUnitLinkSnippet");
		}
		else{
			$this->redirect('this');
		}
	}

	//DIRS
	/**
	 * handler to add new or edit directory
	 * @param int $id
	 */
	public function handleNewDirectory($id)
	{
		$this->dirFormId = $id; //saving directory
		$this->template->showDirForm = true; //show components or title
		$this->template->showTitle = true;

		if($id == 0) //new dir
		{
			$this->template->modalTitle = "Přidat herbář";
		}
		else //update
		{
			$this->dirFormId = $id; //saving id
			$this->template->modalTitle = "Editovat herbář ";
		}

		if ($this->presenter->isAjax()) {
			$this->redrawControl('dirFormSnippet');
			$this->redrawControl('titleSnippet');
		}

	}

	/**
	 * handler for deleting directory - JSON API
	 * @param $id - id of directory to delete
	 */
	public function handleDelDirectory($id)
	{
		if($this->directoryRepository->del($id))
		{
			$this->payload->status = "ok";
		}
		else
		{
			$this->payload->status = "err";
		}
		$this->sendPayload();
	}

	/**
	 * creates form to add directory
	 * @return \Nette\Application\UI\Form
	 */
	protected function createComponentDirForm()
	{
		$form = $this->unitFormFactory->createDir($this->dirFormId);
		$form->onSubmit[] = array($this, 'addEditDirSuccess');

		return $form;
	}

	/**
	 * it cares about saving data to db about new/edited directory
	 * @param $form
	 */
	public function addEditDirSuccess($form)
	{
		$data = $form->getValues();
		$this->directoryRepository->manage($data,$this->getUser()->id);

		$this->redirect('this');
	}


	/**
	 * showes unit with all the details. if you are teacher you can rate the unit everybody (even not registred users) can send a msg to author of the unit
	 * @param $id - ID of unit to show
	 */
	public function renderUnit($id)
	{
		$attributes = $this->attributeRepository->findBy(["state"=>true])->orderBy("order");
		$this->template->attributes = $attributes;

		$tmpUnit = $this->unitRepository->getByID($id);
		$this->template->showRating = $tmpUnit->isSchoolType();
		$this->template->rating = ($tmpUnit->rating == 0 ? "-":$tmpUnit->rating." %");
		$this->template->comment = ($tmpUnit->comment == "" ? "-":$tmpUnit->comment);
		$own = "public";
		if($tmpUnit->getUser() == $this->getUser()->roles[0])
		{
			$own = "my";

		}
		else if($this->getUser()->roles[0] == "teacher" || $this->getUser()->roles[0] == "student")
		{
			$own = "school";
		}

		$unit = $this->template->unit = $this->unitRepository->getUnit($id,$own);

		$this->template->unit = $unit; //I have unit, so send it to template

		$unitEntity = $this->unitRepository->getById($id);
		$this->template->device = $unitEntity->getDeviceName(); //deveice is additional info
		$owner = $this->unitRepository->getById($id)->getUser(); //user is additional info as well
		$this->template->author = $this->userRepository->getById($owner);
		if($unit["taxonomy"] != 0) //do I have the info about the taxonomy?
		{
			$this->template->taxonomy = $this->taxonomyRepository->getPath($unit["taxonomy"],"string");
		}
		$this->template->title = "Student | Položka";
		$this->template->header = "Položka";

		$position = ["name"=>"Detail položky","link"=>""];
		$this->template->path[] = $position;

		if($this->getUser()->roles[0] == "teacher")
		{
			$this->template->showRatingButton = true;
		}

		if($this->getUser()->id == $owner)
		{
			$this->template->owner = true;
		}
	}


	/**
	 * handler to delete unit - JSON API
	 * @param $id - ID of unit to delete
	 */
	public function handleDeleteUnit($id)
	{
		if($this->unitRepository->del($id))
		{
			$this->payload->status = "ok";
		}
		else
		{
			$this->payload->status = "err";
		}
		$this->sendPayload();
	}

	/**
	 * handler to send a message for a unit.
	 * @param $id - id of unit
	 */
	public function handleMsg($id)
	{
		$this->unitId = $id;
		$this->template->showMsgForm = true;
		$this->template->showRatingForm = false;
		$this->template->showTitle = true;
		$this->template->modalTitle = "Napsat autorovi zprávu...";
		// ^^ setting
		// vv redrawing controls
		if ($this->presenter->isAjax()) {
			$this->redrawControl('msgFormSnippet');
			$this->redrawControl('ratingFormSnippet');
			$this->redrawControl('titleSnippet');
		}
	}

	/**
	 * component for send a message
	 * @return \Nette\Application\UI\Form
	 */
	public function createComponentMsgForm()
	{
		$idUser = 0;
		if($this->getUser()->isLoggedIn())
		{
			$idUser = $this->getUser()->id;
		}
		$form = $this->unitFormFactory->createMsg($idUser,$this->unitId);
		$form->onSuccess[] = array($this, 'msgSuccess');

		return $form;
	}

	/**
	 * sending a message
	 * @param $form
	 */
	public function msgSuccess($form)
	{
		file_put_contents("xxaa","sdfsdfsfd");

		$data = $_POST; // I know it is aweful but I need to be quick and I dont know how to do it without refreshing form

		if($this->getUser()->isLoggedIn())
		{
			$user = $this->getUser()->id;
		}
		else
		{
			$user = NULL;
		}
		$msgSent = $this->messageRepository->sendMsg($data,$user);

		if($msgSent != NULL)
		{
			if(isset($data["target"])) //I reply to registered user
			{
				$to = $this->userRepository->getByID($data["target"])->email;
			}
			else // I reply to not registered user so his email is in msg (parent) which I reply to
			{
				$to = $this->messageRepository->getById($msgSent["parentId"])->email;
			}

			if($to != "") //ok, I have a recipient, lets send him a msg
			{
				$body = "Vážený uživateli,<br><br>na serveru herbarium-system.eu Vám byla zaslána osobní zpráva s následujícím obsahem<br><br>
<table>
	<tr>
		<td>Jméno</td>
		<td>".$data["name"]."</td>
	</tr>
	<tr>
		<td>Email</td>
		<td>".$data["email"]."</td>
	</tr>
	<tr>
		<td>Zpráva</td>
		<td>".$data["text"]."</td>
</tr>
	</table>";
				$this->sendMail($to,"noreply@herbarium-system.eu","Osobní zpráva",$body); //send it as an email too
			}
			$this->flashMessage('Výborně! Tvoje zpráva zaslána',"success");
		}
		else
		{
			$this->flashMessage(':( Bohužel došlo k nějaké chybě. Prosím kontaktuj správce.',"danger");
		}

		if (!$this->isAjax())
		{
			$this->redirect('this');
		}
		else
		{
			$this->template->showMsgForm = false; //hide form

			$this->redrawControl("msgFormSnippet"); //redraw controls
			$this->redrawControl("flashes");
			$this->redrawControl('msgs');
		}
	}

	//RATING
	/**
	 * handler to update rating of unit - just show the form
	 * @param $id
	 */
	public function handleRating($id)
	{
		$this->unitId = $id;
		$this->template->showRatingForm = true;
		$this->template->showMsgForm = false;
		$this->template->showTitle = true;
		$this->template->modalTitle = "Hodnotit položku";

		$this->template->rating = $this->unitRepository->getByID($id)->rating;

		if ($this->presenter->isAjax()) {
			$this->redrawControl('ratingFormSnippet');
			$this->redrawControl('msgFormSnippet');
			$this->redrawControl('titleSnippet');
		}
	}

	/**
	 * shows form for set the rating of unit
	 * @return \Nette\Application\UI\Form
	 */
	public function createComponentRatingForm()
	{
		$form = $this->unitFormFactory->createRating($this->unitId);
		$form->onSuccess[] = array($this, 'ratingSuccess');

		return $form;
	}

	/**
	 * saving rating and generating notification
	 * @param $form
	 */
	public function ratingSuccess($form)
	{
		$data = $form->getValues();
		$this->unitId = $data->unit;

		if($this->unitRepository->saveRating($data) && $this->notificationRepository->rating($data)) //save and generate internal notification
		{
			$body = "Vážený uživateli,<br><br>na serveru herbarium-system.eu Vám bylo uděleno hodnocení Vaší položky<br><br>
	<table>
		<tr>
			<td>Hodnocení:</td><td>".$data->rating."</td>
			</tr>
			<td>Komentář:</td><td>".$data->comment."</td>
</tr>
</table>
";
			$to = $this->unitRepository->getById($data->unit)->getUserEmail();
			$this->sendMail($to,"noreply@herbarium-system.eu","Hodnocení položky",$body); //send email notification

			$this->flashMessage('Děkujeme! Hodnocení bylo zasláno autorovi',"success");
		}
		else
		{
			$this->flashMessage(":( Bohužel došlo k chybě","danger");
		}
		if (!$this->isAjax())
		{
			$this->redirect('this');
		}
		else
		{
			$this->redrawControl("flashes"); //redrawing flash messages
			$this->redrawControl('nots'); //updateing notification counter
		}
	}


	//RATING
	/**
	 * handler to update rating of unit - just show the form
	 * @param $id
	 */
	public function handleRateDir($id)
	{
		$this->dirID = $id;
		$this->template->showRatingForm = true;
		$this->template->showMsgForm = false;
		$this->template->showTitle = true;
		$this->template->modalTitle = "Hodnotit herbář";

		$this->template->rating = $this->directoryRepository->getByID($id)->rating;

		if ($this->presenter->isAjax()) {
			$this->redrawControl('ratingFormSnippet');
			$this->redrawControl('msgFormSnippet');
			$this->redrawControl('titleSnippet');
		}
	}

	/**
	 * shows form for set the rating of unit
	 * @return \Nette\Application\UI\Form
	 */
	public function createComponentRatingDirForm()
	{
		$form = $this->unitFormFactory->createRatingDir($this->dirID);
		$form->onSuccess[] = array($this, 'ratingDirSuccess');

		return $form;
	}

	/**
	 * saving rating and generating notification
	 * @param $form
	 */
	public function ratingDirSuccess($form)
	{
		$data = $form->getValues();
		$this->dirID = $data->dir;

		if($this->directoryRepository->saveRating($data) && $this->notificationRepository->ratingDir($data)) //save and generate internal notification
		{

			$body = "Vážený uživateli,<br><br>na serveru herbarium-system.eu Vám bylo uděleno hodnocení Vaší položky<br><br>
	<table>
		<tr>
			<td>Hodnocení:</td><td>".$data->rating."</td>
			</tr>
			<td>Komentář:</td><td>".$data->comment."</td>
</tr>
</table>
";
			$to = $this->directoryRepository->getById($data->dir)->user->email;
			$this->sendMail($to,"noreply@herbarium-system.eu","Hodnocení položky",$body); //send email notification

			$this->flashMessage('Děkujeme! Hodnocení bylo zasláno autorovi',"success");
		}
		else
		{
			$this->flashMessage(":( Bohužel došlo k chybě","danger");
		}
		if (!$this->isAjax())
		{
			$this->redirect('this');
		}
		else
		{
			$this->redrawControl("flashes"); //redrawing flash messages
			$this->redrawControl('nots'); //updateing notification counter
		}
	}

	//END RATING
}
