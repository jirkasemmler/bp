<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 14.3.15
 * Time: 23:10
 */

namespace App\Presenters;
use Nette,
	Nette\Application\UI\Form;
use App\Model\Repositories\GradeRepository;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\TaxonomyRepository;
use App\Model\Repositories\AttributeRepository;
use App\Forms\GradeFormFactory;
use App\Forms\TaxonomyTypeFormFactory;
use App\Forms\AttributeFormFactory;

/**
 * Class TeacherPresenter
 * @package App\Presenters
 */
class TeacherPresenter extends BasePresenter
{
	public $user;

	public function beforeRender()
	{
		parent::beforeRender();
		try{ //check if the logged user is realy teacher
			$this->user = $this->getUser();
			if($this->user->getRoles()[0] != "teacher")
			{
				$this->redirect("Login:");
			}
		}
		catch(\Exception $e)
		{
			$this->redirect("Login:");
		}

	}

	/** @var GradeFormFactory @inject */
	public $gradeFormFactory;

	/** @var AttributeFormFactory @inject */
	public $attributeFormFactory;

	/** @var TaxonomyTypeFormFactory @inject */
	public $taxonomyTypeFormFactory;

	/** @var GradeRepository @inject */
	public $gradeRepository;

	/** @var UserRepository @inject */
	public $userRepository;

	/** @var TaxonomyRepository @inject */
	public $taxonomyRepository;

	/** @var AttributeRepository @inject */
	public $attributeRepository;


	private $id = 0;

	/**
	 * rennders the main page for teacher
	 */
	public function renderDefault()
	{
		$this->redirect("Public:default");
		$this->template->title = "Učitel";
		$this->template->header = "Vítejte v učitelské sekci";
		$this->template->modalTitle = "Vítejte v učitelské sekci";
	}

	/**
	 * shows list of grades
	 */
	public function renderGrades()
	{
		$this->template->title = "Třídy";
		$this->template->header = "Výpis jednotlivých ročníků";

		$position = ["name"=>"Profily","link"=>NULL];
		$this->template->path[] = $position;
		$position = ["name"=>"Třídy","link"=>"Teacher:grades"];
		$this->template->path[] = $position;

		$this->template->grades = $this->gradeRepository->findAll();
	}

	/**
	 * handler to add new grade or update old
	 * @param $id - id of grade to update
	 */
	public function handleManageGrade($id)
	{
		$this->template->showGradeForm = true; //show components or title
		$this->template->showTitle = true;

		if($id == 0) //new grade
		{
			$this->template->modalTitle = "Přidat třídu";
		}
		else //update
		{
			$this->id = $id; //saving id
			$grade = $this->gradeRepository->getById($id);
			$this->template->modalTitle = "Editovat třídu ".$grade->name;
		}

		if ($this->presenter->isAjax()) {
			$this->redrawControl('gradeFormSnippet');
			$this->redrawControl('titleSnippet');
		}
	}


	/**
	 * shows students of current (id) grade
	 * @param $id - id of grade
	 */
	public function handleShowGradeStudents($id)
	{
		$this->template->showGradeStudents = true;
		$this->template->showTitle = true; //settings grades name as name
		$this->template->modalTitle = $this->gradeRepository->getById($id)->name;
		$this->id = $id;

		$this->template->gradeStudents = $this->userRepository->getStudents($id);

		if ($this->presenter->isAjax()) {
			$this->redrawControl('gradeFormSnippet');
			$this->redrawControl('titleSnippet');

		}
	}

	/**
	 * ajax API handler to delete grade
	 * returns json answer in payload
	 * @param $id - id of grade to delete
	 */
	public function handleDelGrade($id)
	{
		try
		{
			$this->gradeRepository->del($id);
			$this->payload->status = "ok";
		}
		catch(\Exception $e)
		{
			$this->payload->status = "err";
		}
		$this->sendPayload();
	}

	/**
	 * change right of registration - is registration for the grade allowed of not?
	 * @param array $data [ID_of_grade , bool if allow or deny]
	 */
	public function handleChangeRegistration(array $data)
	{
		try
		{
			$this->gradeRepository->changeRegistration($data[0],$data[1]);
			$this->payload->status = "ok"; //changed. return ok
		}
		catch(\Exception $e)
		{
			$this->payload->status = "err";
		}
		$this->sendPayload(); //send payload
	}

	/**
	 * creates form for add or edit grade
	 * @return Form
	 */
	protected function createComponentGradeForm()
	{
		$form = $this->gradeFormFactory->create($this->id);

		$form->onSubmit[] = array($this, 'addEditGradeSuccess');


		return $form;
	}

	/**
	 * handler for write data of editing or adding grade
	 * @param Form $form
	 */
	public function addEditGradeSuccess(Form $form)
	{

		if($this->gradeRepository->insert($form->getValues()))
		{
			$this->flashMessage('Váš příspěvek byl uložen. Děkujeme za Váš čas.');
		}
		else
		{
			$this->flashMessage('došlo k chybě');
		}

		if (!$this->isAjax())
		{
			$this->redirect('this');
		}
		else
		{
			$this->redrawControl('listOfGrades');
			$this->redrawControl('gradeFormSnippet');
			$form->setValues(array(), TRUE); //clear form for next usage
		}
	}

	/**
	 * renders tree of of taxonomy (or the titles). the rest is the by JS in the template
	 */
	public function renderTree()
	{
		$this->template->title = "Taxonomie | Taxonomický strom";
		$this->template->header = "Taxonomický strom";
		$this->template->modalTitle = "strom";

		$position = ["name"=>"Taxonomie","link"=>NULL]; //show path of page
		$this->template->path[] = $position;
		$position = ["name"=>"Taxonomický strom","link"=>"Teacher:tree"];
		$this->template->path[] = $position;
	}

	/**
	 * just shows JSON of the tree which gets the JS to create the tree on front end - JSON API
	 */
	public function handleGetTree()
	{
		$data = $this->taxonomyRepository->getTaxonomy(0); //get data
		echo json_encode($data); //make the json from data and show it
		exit(); //do not show anything else
	}

	/**
	 * handler for update/insert node to the tree
	 * @param $idNode - id for update/insert . if insert -> than it is generic ID like j<idROOT>_<COUNT of nodes> -> it is renamed in insering process - JSON API
	 * @param $text - name of the node
	 * @param $id_parent - id of parent node
	 */
	public function handleupdateNode($idNode, $text, $id_parent)
	{
		if($this->taxonomyRepository->updateNode($idNode,$id_parent,$text))
		{
			$this->payload->status = "ok";
		}
		else
		{
			$this->payload->status = "err";
		}
		$this->sendPayload();
	}

	/**
	 * delete the subtree with the $idnode as a root - JSON API
	 * @param $idNode - root of deleting subtree
	 */
	public function handleDeleteNode($idNode)
	{

		if($this->taxonomyRepository->deleteNodes($idNode))
		{
			$this->payload->status = "ok";
		}
		else
		{
			$this->payload->status = "err";
		}
		$this->sendPayload();
	}

	/**
	 * types have one render
	 */
	public function renderTypes()
	{
		$this->template->title = "Taxonomie | Hierarchie taxonů";
		$this->template->header = "Hierarchie taxonů";

		$position = ["name"=>"Taxonomie","link"=>NULL]; //setting path of page
		$this->template->path[] = $position;
		$position = ["name"=>"Úrovně taxonů","link"=>"Teacher:types"];
		$this->template->path[] = $position;

		$this->template->types = $this->taxonomyRepository->getTaxonomyTypes();
	}

	/**
	 * to add or to update taxonomy type . if id=0 -> insert
	 * @param $id
	 */
	public function handleAddTaxonomyType($id)
	{
		$this->template->showTypeForm = true; //show components or title
		$this->template->showTitle = true;


		if($id == 0) //new grade
		{
			$this->template->modalTitle = "Přidat typ";
		}
		else //update
		{
			$this->id = $id; //saving id
			$this->template->modalTitle = "Editovat typ ";
		}

		if ($this->presenter->isAjax()) {
			$this->redrawControl('typeFormSnippet');
			$this->redrawControl('titleSnippet');
		}
	}

	/**
	 * creates form for add or edit grade
	 * @return Form
	 */
	protected function createComponentTypeForm()
	{
		$form = $this->taxonomyTypeFormFactory->create($this->id);
		$form->onSubmit[] = array($this, 'ManageTaxonomyTypeSuccess');
		return $form;
	}
	/**
	 * handler for write data of editing or adding grade
	 * @param Form $form
	 */
	public function ManageTaxonomyTypeSuccess(Form $form)
	{
		if($this->taxonomyRepository->manageType($form->getValues(),$this->id))
		{ //write to database
			$this->flashMessage('Váš příspěvek byl uložen. Děkujeme za Váš čas.');
		}
		else
		{
			$this->flashMessage('došlo k chybě');
		}

		if (!$this->isAjax())
		{
			$this->redirect('this');
		}
		else
		{
			$this->redrawControl('listOfTypes');
			$this->redrawControl('gradeFormSnippet');
			$form->setValues(array(), TRUE); //clear form for next usage
		}
	}

	/**
	 * updateing sort of types. in $pos are serialized IDs of types and due order of IDs is set (vie updateTypesSort) new order . JSON API
	 * @param array $pos
	 */
	public function handleupdateTypesSort(array $row)
	{
		if($this->taxonomyRepository->updateTypesSort($row))
		{
			$this->payload->status = "ok";
		}
		else
		{
			$this->payload->status = "err";
		}
		$this->sendPayload();
	}

	/**
	 * handler for editing type(taxon). just enable display, set title and the component (control) will care about the rest
	 * @param $id - ID of type(taxon) to edit
	 */
	public function handleEditType($id)
	{
		$this->template->showTypeForm = true; //show component
		$this->template->showTitle = true; //show title

		$this->id = $id; //saving id
		$type = $this->taxonomyRepository->getType($id); //get type to show the title
		$this->template->modalTitle = "Editovat třídu ".$type->name;

		if ($this->presenter->isAjax())
		{
			$this->redrawControl('typeFormSnippet'); //redrawing controls
			$this->redrawControl('titleSnippet');
		}

	}

	/**
	 * handler to delete type(taxon) - JSON api
	 * @param $id - ID of type (taxon) to delete
	 */
	public function handleDelType($id)
	{
		if($this->taxonomyRepository->deleteType($id)) //delete it
		{
			$this->payload->status = "ok"; //it is ok
		}
		else
		{
			$this->payload->status = "err";
		}
		$this->sendPayload();
	}

	/**
	 * renders list (table) of all attributes with main deatails and buttons to access to CRUD
	 */
	public function renderAttributes()
	{
		$this->template->title = "Formulářové položky";
		$this->template->header = "Formulářové položky";
		$this->template->modalTitle = "Formulářové položky";
		$this->template->names  = ["file"=>"Fotografie","input"=>"Textové pole","textarea"=>"Textová oblast","select"=>"Rozklikávací nabídka"];

		$position = ["name"=>"Formulářové položky","link"=>"Teacher:attributes"];
		$this->template->path[] = $position;

		$this->template->attributes = $this->attributeRepository->getAttributes();
		$this->template->options = $this->attributeRepository->getOptions();
	}

	/**
	 * to add or to update atribute . if id=0 -> insert
	 * @param $id
	 */
	public function handleManageAttribute($id)
	{
		$this->template->showAttributeForm = true; //show components or title
		$this->template->showFullForm = false;
		$this->template->showTitle = true;

		if($id == 0) //new grade
		{
			$this->template->modalTitle = "Přidat atribut";
		}
		else //update
		{
			$this->id = $id; //saving id
			$this->template->modalTitle = "Editovat atribut ";
		}

		if ($this->presenter->isAjax()) {
			$this->redrawControl('attributeFormSnippet');
			$this->redrawControl('attributeFullFormSnippet');
			$this->redrawControl('titleSnippet');
		}
	}

	/**
	 * to add or to update atribute . if id=0 -> insert
	 * @param $id
	 */
	public function handleShowForm()
	{
		$this->template->showFullForm = true; //show components or title
		$this->template->showAttributeForm = false; //show components or title
		$this->template->showTitle = true;

		$this->template->modalTitle = "Přidat atribut";

		if ($this->presenter->isAjax()) {
			$this->redrawControl('attributeFormSnippet');
			$this->redrawControl('attributeFullFormSnippet');
			$this->redrawControl('titleSnippet');
		}
	}

	/**
	 * creates form of attributes
	 * @return \Nette\Application\UI\Form
	 */
	public function createComponentAttributeFullForm()
	{
		$form = $this->attributeFormFactory->showForm();
		return $form;
	}
	/**
	 * creates form for add or edit grade
	 * @return Form
	 */
	protected function createComponentAttributeForm()
	{
		$form = $this->attributeFormFactory->create($this->id);
		$form->onSubmit[] = array($this, 'ManageAttributeSuccess');

		return $form;
	}

	/**
	 * cares about creating new attributes to form or update old ones
	 * @param $form - form where to get data from. But in this case I use $_POST because I add new inputs dynamicaly
	 */
	public function ManageAttributeSuccess($form)
	{
		if($this->attributeRepository->manage($_POST)) //i know, it doesnt look so well, but i add inputs dynamically
		{
			$this->flashMessage('Váš příspěvek byl uložen. Děkujeme za Váš čas.');
		}
		else
		{
			$this->flashMessage('Omlouváme se, ale došlo k chybě.');
		}

		if (!$this->isAjax())
		{
			$this->redirect('this');
		}
		else
		{
			$this->redrawControl('attributesList');
			$form->setValues(array(), TRUE); //clear form for next usage
		}

	}

	/**
	 * handler for saving order of attributes in form.  JSON API
	 * @param array $row - serialized form of IDs of new order
	 */
	public function handleupdateAttributesSort( array $row)
	{
		if($this->attributeRepository->updateAttributesSort($row)) //saving order
		{
			$this->payload->status = "ok";  //everything is ok
		}
		else
		{
			$this->payload->status = "err"; //ooops
		}

		$this->sendPayload(); //it is just a JSON API
	}

	/**
	 * ajax API handler to delete grade
	 * returns json answer in payload
	 * @param $id - id of grade to delete
	 */
	public function handleDeleteAttribute($id)
	{
		$this->attributeRepository->del($id);
		try
		{

			$this->payload->status = "ok";
		}
		catch(\Exception $e)
		{

			$this->payload->status = "err";
		}
		$this->sendPayload();
	}

	/**
	 * showes list of ALL users - there is datatables to sort and find in it
	 */
	public function renderUsers()
	{
		$this->template->title = "Výpis uživatelů";
		$this->template->header = "Výpis uživatelů";

		$position = ["name"=>"Profily","link"=>NULL];
		$this->template->path[] = $position;
		$position = ["name"=>"Studenti","link"=>"Teacher:users"];
		$this->template->path[] = $position;

		$this->template->data = $this->userRepository->getAllStudents();

	}

	public function handleTeachers($id)
	{
		$this->template->showTeachers = true; //show components or title
		$this->template->showTitle = true;
		$this->id = $id;
		$this->template->modalTitle = "Editovat učitele";
		$this->template->teachers = $this->gradeRepository->getById($id)->getTeachers();

		if ($this->presenter->isAjax()) {
			$this->redrawControl('gradeFormSnippet');
			$this->redrawControl('titleSnippet');
		}
	}

	public function createComponentTeachersForm()
	{
		$form = $this->gradeFormFactory->createTeachersForm($this->id);
		$form->onSubmit[] = array($this, 'addTeacherSuccess');

		return $form;
	}

	/**
	 * handler for write data of editing or adding grade
	 * @param Form $form
	 */
	public function addTeacherSuccess(Form $form)
	{
		if($this->gradeRepository->addTeacher($form->getValues()))
		{
			$this->flashMessage('Váš příspěvek byl uložen. Děkujeme za Váš čas.');
		}
		else
		{
			$this->flashMessage('došlo k chybě');
		}

		if (!$this->isAjax())
		{
			$this->redirect('this');
		}
		else
		{
			$this->redrawControl('listOfGrades');
			$this->redrawControl('gradeFormSnippet');
			$form->setValues(array(), TRUE); //clear form for next usage
		}
	}

	/**
	 * set the user as a student from the teacher post
	 * @param $id -id of user
	 */
	public function handleDelTeacher($id)
	{

		try
		{
			$this->userRepository->setAsStudent($id);
			$this->payload->status = "ok";
		}
		catch(\Exception $e)
		{

			$this->payload->status = "err";
		}
		$this->sendPayload();
	}
}

