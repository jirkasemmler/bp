<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 14.3.15
 * Time: 23:10
 */

namespace App\Presenters;
use App\Model\Repositories\AttributeRepository;
use App\Model\Repositories\DirectoryRepository;
use App\Model\Repositories\UnitRepository;
use App\Model\Repositories\TaxonomyRepository;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\FileRepository;
use App\Model\Repositories\MessageRepository;
use App\Model\Repositories\NotificationRepository;
use App\Model\UserManager;
use App\Forms\UnitFormFactory;
use App\Forms\LoginFormFactory;
use App\Model\UploadHandler;
use App\Forms\RegisterFormFactory;

use Nette;
class StudentPresenter extends BasePresenter{


	public $unitId = 0;
	/** @var AttributeRepository @inject */
	public $attributeRepository;

	/** @var UserManager @inject */
	public $userManager;

	/** @var TaxonomyRepository @inject */
	public $taxonomyRepository;

	/** @var DirectoryRepository @inject */
	public $directoryRepository;

	/** @var LoginFormFactory @inject */
	public $loginFormFactory;

	/** @var UnitRepository @inject */
	public $unitRepository;
	/** @var FileRepository @inject */
	public $fileRepository;

	/** @var UserRepository @inject */
	public $userRepository;

	/** @var MessageRepository @inject */
	public $messageRepository;

	/** @var NotificationRepository @inject */
	public $notificationRepository;

	/** @var UnitFormFactory @inject */
	public $unitFormFactory;

	/** @var RegisterFormFactory @inject */
	public $registerFactory;

	public $user; //internal variables
	public $dirID = 0;
	public $dirFormId = 0;
	public $threadId = 0;
	public $type = "";

	public $profileValues = NULL; //prom for data about user to edit
	/**
	 * are you logged in?
	 */
	public function beforeRender()
	{
		parent::beforeRender();
		try{

//			$this->user = $this->getUser();
			if(!$this->getUser()->isLoggedIn() && $this->action !== "profile")
			{
				echo $this->action;
			exit();
//				$this->redirect("Login:");
			}
		}
		catch(\Exception $e)
		{
			$this->redirect("Login:");
		}

	//get number of unread messages and notifications to bar
		$this->template->notReadMsgs = $this->messageRepository->getNotReadMsg($this->getUser()->id);
		$this->template->notReadNots = $this->notificationRepository->findBy(["user_id"=>$this->getUser()->id,"read"=>false])->count();
	}

	/**
	 * renders nothing but everything
	 */
	public function renderDefault()
	{
		$this->redirect("Public:units");
		$this->template->title = "Student";
		$this->template->header = "Vítej studente!";
	}

	/**
	 * creates form fo new unit
	 * @return \Nette\Application\UI\Form
	 */
	public function createComponentNewUnitForm()
	{
		$form = $this->unitFormFactory->create($this->dirID,$this->unitId,$this->getUser()->id);
		$form->onSubmit[] = array($this, 'newUnitSuccess');
		return $form;
	}

	/**
	 * to save data to db about new unit
	 * @param $form
	 */
	public function newUnitSuccess($form)
	{
		$data = $form->getValues();

		$this->unitRepository->manage($data);
		if($data["unit"] == 0)
		{
			$this->flashMessage('Výborně, tvoje položka byla přidána!',"success");
		}
		else
		{
			$this->flashMessage('Výborně, tvoje položka byla editována!',"success");
		}

		$this->redirect("Public:units");
	}

	/**
	 * action to add new unit - it is own page because new unit is really huge thing and there are 2 part off adding
	 * @param $id - id of directory
	 */
	public function renderNewUnit($id)
	{
		$this->dirID = $id;
		$dir = $this->directoryRepository->getById($id);
		$this->template->title = "Student | Položky";
		$this->template->header = "Nová položka v \"".$dir->name."\"";

		$this->template->files = $this->attributeRepository->getFileAttributes();

	}

	/**
	 * handler to upload img for unit . Using "uploadHandler" from model
	 * @param $attId
	 */
	public function handleUploadPicture($attId) {
		$uploader = new UploadHandler($this->fileRepository);
		$uploader->allowedExtensions = array("jpeg", "jpg", "png", "gif");
		$result = $uploader->handleUpload(__DIR__ . '/../../data',$this->getUser()->id);
		if(isset($result["name"]))
		{
			$row = $this->fileRepository->insertFile($result["name"],$this->getUser()->id);
			$result["idFile"] = $row->id;
			$result["attId"] = $attId;
		}
		else
		{
			$result["err"] = "err";
		}

		$this->sendResponse(new Nette\Application\Responses\JsonResponse($result));
	}

	public function handleDelPic($id){
		$result["isError"] = "false"; //it just sends posiitve reply
		$this->sendResponse(new Nette\Application\Responses\JsonResponse($result));
	}


	/**
	 * renders form and editing enviroment for edit unit - change info, pictures and taxonomy
	 * @param $id - id of unit to edit
	 */
	public function renderEditUnit($id)
	{
		$this->setView("newUnit");
		$this->unitId = $id;
//		$dir = $this->directoryRepository->getById($id);
		$this->template->title = "Student | Položky";
		$this->template->header = "Editovat položku";
		$this->template->files = $this->attributeRepository->getFileAttributes($id);
	}

	//END UNITS



	/**
	 * just shows JSON of the tree which gets the JS to create the tree on front end
	 */
	public function handleGetTree()
	{
		$data = $this->taxonomyRepository->getTaxonomy(0); //get data
		echo json_encode($data); //make the json from data and show it
		exit(); //do not show anything else
	}

	//MSGS & NOTS (from unit and from bar too)
	/**
	 * shows a list of threads for the user. It works in cooperation with notification - via one renderer but with two handlers and user can switch between msgs and nots (notifications) using ajax
	 * @param null $type - what to show ? is null -> msgs
	 */
	public function renderMessages($type = NULL)
	{
		//dictionary - msgs - messages
		//dictionary - nots - notifications

		if($this->type == "" && $type != NULL)
		{
			$this->type = $type;
		}

		$this->template->title = "Zprávy & notifijace";

		if($this->type == "nots" || $type == "nots") //show nots
		{
			$this->template->type = "nots"; //Set type
			$this->template->items = $this->notificationRepository->getNots($this->getUser()->id); //send array of nots
			$this->template->notReadNots = $this->notificationRepository->findBy(["user_id"=>$this->getUser()->id,"read"=>"false"])->count(); //send count of nots
			$this->redrawControl("nots"); //update number of nots
			$this->redrawControl("notsHeader");
		}
		else if($this->type == "" || $this->type == "msgs") //show msgs
		{
			$this->type = "msgs"; //set type
			$this->template->items = $this->messageRepository->getMsgs($this->getUser()->id); //send list of msgs
			$this->template->type = "msgs"; //send type to template too
		}

		if($this->threadId != 0)  //if is set a thread to show -> via handleShowThread
		{
			$this->template->msgs = $this->messageRepository->getTopic($this->threadId,$this->getUser()->id); //send open thread via ajax
		}
	}

	/**
	 * handler to show nots
	 */
	public function handleGetNots()
	{
		$this->type = "nots"; //set type
		$this->redrawControl("listMsgsNots"); //redraw it
	}

	/**
	 * handle to show msgs
	 */
	public function handleGetMsgs()
	{
		$this->type = "msgs"; //Set type
		$this->redrawControl("listMsgsNots"); //redraw
	}

	/**
	 * show chat of one thread
	 * @param $id  - id of thread to show
	 */
	public function handleShowThread($id)
	{

		$this->threadId = $id; //save id
		$this->messageRepository->setThreadRead($id,$this->getUser()->id); // mark all messages in the thread as read. user opened the thread it means that he reads all msgs in it -> but set as read just as the user who opens it
		$thread = $this->messageRepository->getThread($id);

		if($thread["author_id"] != NULL)
		{
			$this->template->showReply = true;
		}
		$this->template->showThread = true;
		$this->template->showTitle = true;
		$this->template->modalTitle = "Osobní pošta";
		if ($this->presenter->isAjax())
		{
			$this->redrawControl("msgSnippet");
			$this->redrawControl("titleSnippet");
			$this->redrawControl("msgSnippet");
			$this->redrawControl("listMsgsNots");
			$this->redrawControl("msgsHeader");
			$this->redrawControl("msgs");
		}
		else{
			$this->redirect('this');
		}
	}

	/**
	 * creates form for reply
	 * @return \Nette\Application\UI\Form
	 */
	public function createComponentReplyForm()
	{
		$form = $this->unitFormFactory->createReply($this->threadId);
		$form->onSuccess[] = array($this, 'replySuccess');

		return $form;
	}

	/**
	 * sends data from reply
	 * @param $form
	 */
	public function replySuccess($form)
	{
		$data = $form->getValues();
		$msgSent = $this->messageRepository->sendMsg($data,$this->getUser()->id,false);

		if($msgSent != NULL)
		{
			$this->flashMessage('Výborně! Zpráva byla zaslána.',"success");
		}
		else
		{
			$this->flashMessage(':( Bohužel došlo k chybě',"danger");
		}

		if (!$this->isAjax())
		{
			$this->redirect('this');
		}
		else
		{
			$this->redrawControl("msgFormSnippet");
			$this->redrawControl("flashes");
			$this->redrawControl('msgs');
		}
	}
	//END MSGS & NOTS

	/**
	 * shows users profile
	 * @param null $id - can be shown another user (id) . if NULL -> show me my logged profile
	 */
	public function renderProfile($id = NULL)
	{
		if($id == NULL || $id == $this->getUser()->id) //show me my profile
		{
			$id = $this->getUser()->id;
			$this->template->editProfile = true;
		}
		else
		{
			$this->template->editProfile = false;
		}

		$user = $this->userManager->userRepository->getById($id);
		$this->template->title = "Profil ".$user->name;
		$this->template->header = "Profil ".$user->name;
		$this->template->profile = $user;
		$this->template->grades = $user->getUserGrade();
		$this->template->devices = $user->getDevices();
	}


	/**
	 * creates a form for update user. fills this form with data about user
	 * @return Nette\Application\UI\Form
	 */
	public function createComponentUpdateUserForm()
	{
//        $form = $this->registerFactory->create();
		$form = $this->registerFactory->create($this->profileValues);
		$form->onSuccess[] = array($this, 'updateUserSuccess');

		return $form;
	}


	/**
	 * handler to update user - creates form for update
	 * @param $id : ID of user to update
	 */
	public function handleUpdateUser($id)
	{
		if($this->getUser()->id != $id && $this->getUser()->getRoles()[0] != "teacher")
		{
			$this->redirect("Login:");
		}
		$this->template->showUpdateForm = true;
		$this->template->showTitle = true;
		$this->profileValues = $this->userManager->getProfile($id);
		$this->template->modalTitle = $this->profileValues["name"];

		if ($this->presenter->isAjax()) {
			$this->redrawControl('updateUserFormSnippet');
			$this->redrawControl('titleSnippet');
			$this->redrawControl('profile');
		}
	}

	/**
	 * method for write data from form of updating user
	 * @param $form - form where data come from
	 * @internal param $values - data
	 */
	public function updateUserSuccess($form)
	{
		$values = $form->getValues();
		if($this->userManager->updateUser($values->id,$values))
		{
			$this->flashMessage("Tvůj profil byl úspěšně upraven! Teď vypadá skvěle!","success");
		}
		else
		{
			$this->flashMessage(":( Došlo k chybě a něco se pokazilo, zkus to oznámit správci","warning");
		}

		$this->redirect('this');
	}


	/**
	 * form to add the device
	 * @return \Nette\Application\UI\Form
	 */
	public function createComponentDeviceForm()
	{
		$form = $this->loginFormFactory->createDevices($this->getUser()->id);
		$form->onSuccess[] = array($this, 'updateDevicesSuccess');

		return $form;
	}

	/**
	 * handler to show the dialog for updateing devices
	 * @param $id
	 */
	public function handleUpdateDevices($id)
	{
		if($this->getUser()->id != $id && $this->getUser()->getRoles()[0] != "teacher")
		{
			$this->redirect("Login:");
		}
		$this->template->showDeviceForm = true;
		$this->template->showTitle = true;
		$this->template->modalTitle = "Moje zařízení";

		if ($this->presenter->isAjax()) {
			$this->redrawControl('deviceFormSnippet');
			$this->redrawControl('titleSnippet');
		}
	}

	/**
	 * methot to care about update devices from form
	 * @param $form
	 */
	public function updateDevicesSuccess($form)
	{
		$data = $form->getValues();

		if($this->userRepository->updateDevices($data,$this->getUser()->id))
		{
			$this->flashMessage("Výborně! Tvoje foťáky byly změněny.","success");
		}
		else
		{
			$this->flashMessage(":( Došlo k chybě a něco se pokazilo, zkus to oznámit správci","warning");
		}

		$this->redirect('this');
	}


	/**
	 * upload handler to upload the avatar photo from user view
	 */
	public function handleUploadAvatar() {
		$uploader = new UploadHandler($this->fileRepository);
		$uploader->allowedExtensions = array("jpeg", "jpg", "png", "gif");
		$result = $uploader->handleUpload(__DIR__ . '/../../data',$this->getUser()->id);

		$row = $this->fileRepository->insertFile($result["name"],$this->getUser()->id);
		$this->userManager->updateAvatar($this->getUser()->id,$row->id);
		$this->sendResponse(new Nette\Application\Responses\JsonResponse($result));
	}

}