<?php

namespace App\Presenters;

use Nette,
	App\Model;
use Nette\Mail\Message,
	Nette\Mail\SendmailMailer;
use App\Model\Repositories\HelpRepository;
/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    private $mainTitle;
    public $helpKey;

	/** @var HelpRepository @inject */
	public $helpRepository;

    public function beforeRender()
    {
        $this->template->path = array();

        parent::beforeRender();
        $this->template->mainTitle = "Herbarium";

        $this->template->logged = $this->getUser()->isLoggedIn();

	    if($this->getUser()->isLoggedIn())
	    {
		    $this->template->profileID = $this->getUser()->id;
		    $this->template->name = $this->getUser()->getIdentity()->name;
	    }
        $role = $this->getUser()->getRoles()[0];


        $menu = array();
        $menuItem = array("name"=>"",
            "link"=>"",
            "sub"=>[],
            "tooltip"=>"",
            "icon"=>"" //fa-icon without "fa-" prefix
        );
        if($role == "student")
        {
	        $this->template->path[]= ["name"=>"Student","link"=>"Student"];

            $menuItem["name"] = "Moje herbáře a položky";
            $menuItem["link"] = "Public:units";
	        $menuItem["icon"] = "list-alt";
	        $menuItem["help"] = "Spravovat vlastní herbáře a položky";

	        $menu[] = $menuItem;

	        $menuItem["name"] = "Profil";
	        $menuItem["link"] = "Student:profile";
	        $menuItem["icon"] = "user";
	        $menuItem["help"] = "Zobrazit svůj profil";

	        $menu[] = $menuItem;
        }
        elseif($role == "teacher")
        {
	        $this->template->path[]= ["name"=>"Učitel","link"=>"Teacher"];

	        $menuItem["name"] = "Moje herbáře a položky";
            $menuItem["link"] = "Public:units";
	        $menuItem["icon"] = "list-alt";
	        $menuItem["help"] = "";

	        $menu[] = $menuItem;

	        $menuItem["name"] = "Uživatelské profily";
	        $menuItem["link"] = "#";
	        $menuItem["icon"] = "shield";
	        $menuItem["help"] = "";


            $menuItemSub["name"] = "Třídy";
            $menuItemSub["link"] = "Teacher:grades";
            $menuItemSub["icon"] = "university";
	        $menuItemSub["help"] = "Studentské profily dle tříd";

	        $menuItem["sub"][] = $menuItemSub;

	        $menuItemSub["name"] = "Studenti";
	        $menuItemSub["link"] = "Teacher:users";
	        $menuItemSub["icon"] = "users";
	        $menuItemSub["help"] = "Přehled všech studenstkých profilů";

	        $menuItem["sub"][] = $menuItemSub;
	        $menu[] = $menuItem;

            $menuItem["name"] = "Taxonomie";
            $menuItem["link"] = "#";
            $menuItem["icon"] = "list-alt";
	        $menuItem["sub"] = [];
	        $menuItem["help"] = "";

            $menuSub["name"] = "Taxonomický strom";
            $menuSub["link"] = "Teacher:tree";
            $menuSub["icon"] = "share-alt";
	        $menuSub["help"] = "Upravit zavedený strom taxonomie";

            $menuItem["sub"][] = $menuSub;


            $menuSub["name"] = "Úrovně taxonů";
            $menuSub["link"] = "Teacher:types";
            $menuSub["icon"] = "sort-amount-desc";
	        $menuSub["help"] = "Upravit jednotlivé taxonomické úrovně";

            $menuItem["sub"][] = $menuSub;

            $menu[] = $menuItem;

            $menuItem["name"] = "Nastavení formuláře";
            $menuItem["link"] = "Teacher:attributes";
            $menuItem["icon"] = "list";
            $menuItem["sub"] = [];
	        $menuItem["help"] = "Nastavení formuláře pro položky";
            $menu[] = $menuItem;


        }
        else //default - not logged in
        {
            $menuItem["name"] = "Registrace";
            $menuItem["link"] = "Login:register";
            $menuItem["icon"] = "user-plus";
	        $menuItem["help"] = "Registrovat se do systému";

            $menu[] = $menuItem;

            $menuItem["name"] = "Přihlášení";
            $menuItem["link"] = "Login:";
            $menuItem["icon"] = "sign-in";
	        $menuItem["help"] = "Přihlásit se do systému";

            $menu[] = $menuItem;
        }

	    $menuItem["name"] = "Veřejné herbáře";
	    $menuItem["link"] = "#";
	    $menuItem["icon"] = "pagelines";
	    $menuItem["help"] = "Procházet a studovat herbáře";


	    $menuItemSub["name"] = "Třídy";
	    $menuItemSub["link"] = "Public:grades";
	    $menuItemSub["icon"] = "university";
	    $menuItemSub["help"] = "Zobrazit přehled tříd s herbáři";

	    $menuItem["sub"][] = $menuItemSub;

	    $menuItemSub["name"] = "Studenti";
	    $menuItemSub["link"] = "Public:students";
	    $menuItemSub["icon"] = "users";
	    $menuItemSub["help"] = "Zobrazit přehled studentů s herbáři";

	    $menuItem["sub"][] = $menuItemSub;

	    $menu[] = $menuItem;

        $this->template->menu = $menu;
    }

	public function sendMail($to,$from,$sub,$body)
	{
		$msg = new Message;
		$msg->setFrom($from)
			->addTo( $to)
			->setSubject($sub)
			->setHTMLBody($body);

		$mailer = new SendmailMailer;
		$mailer->send($msg);
	}
}
