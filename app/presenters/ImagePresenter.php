<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 2.4.15
 * Time: 23:39
 */

namespace App\Presenters;
use Nette\Utils\Image;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\FileRepository;
use App\Model\Repositories\NotificationRepository;
use App\Model\Repositories\SettingRepository;
use Nette\Caching\Cache;
use Nette\Caching\Storages;

class ImagePresenter  extends BasePresenter {

	/** @var UserRepository @inject */
	public $userRepository;

	/** @var FileRepository @inject */
	public $fileRepository;

	/** @var SettingRepository @inject */
	public $settingRepository;

	/** @var NotificationRepository @inject */
	public $notificationRepository;

	/**
	 * jsut to have the Default action
	 */
	public function actionDefault($data)
	{

	}

	/**
	 * sends picture of avatar of user . the same idea as in the next method
	 * @param $id - id of user
	 * @param $size - size to show - big - in profile | small - to icon in panel
	 */
	public function actionAvatar($id,$size)
	{
		$prefix = ["..","data"];

		$user = $this->userRepository->getById($id);
		$file = $user->getFileAvatar();

		if($file == false) // default
		{
			$url = "../data/0/defaultAvatar.png";
		}
		else
		{
			$urlAr = array_merge($prefix,[$id,$file->name]);
			$url = join("/",$urlAr); //make url from parts, / is delimiter
		}

		$h = 100; //just inicialisation
		$w = 100;
		switch($size)
		{
			case "small": // to panel
			{
				$w = 50;
				$h = 50;
			};break;
			case "big": //to profile
			{
				$w = 100;
				$h = 100;
			};break;
		}

			$image = Image::fromFile($url);
			$image->resize($w,$h);

			$image->send(Image::PNG);

		$this->terminate();

	}

	/**
	 * action to show picture for client. it works with cache. for the image request it look to the cache if the requested size of image isnt there. if not, it creates the resized image, save it to cache and then send it to client
	 *
	 * @param $id - id of file
	 * @param $size - requested size of image
	 *
	 * @throws \Nette\Application\AbortException
	 * @throws \Nette\Utils\UnknownImageFileException
	 */
	public function actionUnitPhoto($id,$size)
	{
		$prefix = ["..","data"]; //prefixes to create the path

		$file = $this->fileRepository->getById($id); //get the

		$w = 0; //default values
		$h = 0;

		$suffix = "";
		switch($size) //this action cares about different requested sizes
		{
			case "small": // to panel
			{
				$w = 50;
				$h = 50;
				$suffix = "_small";
			};break;

			case "medium": //to overview
			{
				$w = 200;
				$h = 200;
				$suffix = "_medium";
			};break;

			case "checksize": //to send it to check server for detecting plagiarism
			{
				$w = 2900;
				$h = 2900;
				$suffix = "_checksize";
			}
		}

		$fileNameTmpAr = explode(".",$file->name); //prepare array to make the path
		$fileNameTmp = $fileNameTmpAr[0].$suffix; //add the suffix
		$fileName = $fileNameTmp.".".$fileNameTmpAr[1];

		$urlAr = array_merge($prefix,[$file->userId,$fileName]); //merge array for the pth
		$url = join("/",$urlAr); //make url from parts, / is delimiter
//echo $url;
		$cache = new \Nette\Caching\Cache( new \Nette\Caching\Storages\FileStorage("../temp") , 'images'); //initialize the cache
		$cachedImg = $cache->load($fileName);//load from cache

		if($cachedImg == NULL) //it isnt in the cache, so i need to make it again and save it to the cache
		{
			$srcUrl = str_replace($suffix,"",$url); //remove the suffix
			if(!is_file($srcUrl))
			{
				$url = "../data/0/defaultUnit.png"; //default
			}
			$image = Image::fromFile($srcUrl); //load the origin
			if($w != 0 && $h !=0)
			{
				$image->resize($w,$h); //resize the picure by the need
			}

			$cache->save($fileName, (string)$image,array(\Nette\Caching\Cache::FILES => $url)); //save it to cache

		}
		else
		{
			$image = Image::fromString($cachedImg); //it is in the cache - just load it
		}
		$image->send(); //send to browser
		$this->terminate();
	}


	/**
	 * action for cron job - it checks plagiarisms of pictures. sends request to check the picture with local database of images to extern server. if it finds a plagiarsm it call notification creating
	 */
	public function actionCheckPlag()
	{
		$fileToCheck = $this->fileRepository->findByChecked(false);

		$url = $this->settingRepository->getValue("plagServerPath"); //get url from setting. at this url should be the script comparing images
		$thisURL = $this->settingRepository->getValue("thisURL"); //get this url. just because of create url

		foreach($fileToCheck as $file) //for each not checked file...
		{
			$data = array('url' => $thisURL."image/unit-photo/".$file->id."?size=checksize", 'id' => $file->id);

			$options = array( //prepare the structure for the POST request
				'http' => array(
					'header'  => "Content-type: application/x-www-form-urlencoded\r\n", //set the header
					'method'  => 'POST', //method is POST
					'content' => http_build_query($data), //join the $data array as the POST
				),
			);
			$context  = stream_context_create($options); //create the request
			$result = file_get_contents($url, false, $context); //send the request, returned data from request is in $result

			/* structure of returned JSON
			 * {
			 *   state      : ok|else - was it ok?
			 *   computed   : ok/fail,  - could you compete the signature?
			 *   found      : yes/no    - did you find a plaiarism?
			 *   duplicates : [id1, id2, id3...]  - ids of plags
			 * }
			 */
			$jsonResult = json_decode($result); //decode it to array

			if($jsonResult->state == "ok" && $jsonResult->computed == "ok")
			{ //everything went good
				if($jsonResult->found == "yes") //did you find something?
				{
					$this->notificationRepository->plagiarism($file->id,$jsonResult->duplicates); //make notification about found plagiarism to the teacher
				}
			}

			$this->fileRepository->setAsChecked($file); //set this file as checked
		}
		$this->terminate(); //kill me, kill me hard!
	}
}