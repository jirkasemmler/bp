<?php

namespace App\Forms;

use App\Model;
use App\Model\Repositories\GradeRepository;
use App\Model\Repositories\UserRepository;
use Nette,
	Nette\Application\UI\Form,

	Nette\Security\User;
use Instante\Bootstrap3Renderer\BootstrapRenderer;

/**
 * Class GradeFormFactory
 * @package App\Forms
 */
class GradeFormFactory extends Nette\Object
{

	/**
	 * @param GradeRepository $gradeRepository
	 */
	protected $gradeRepository;
	/**
	 * @param UserRepository $userRepository
	 */
	protected $userRepository;

	public function __construct(GradeRepository $gradeRepository, UserRepository $userRepository)
	{
		$this->gradeRepository = $gradeRepository;
		$this->userRepository = $userRepository;
	}

	/**
	 * form for add/edit grade
	 * @return Form
	 */
	public function create($id)
	{
		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());

		$form->getElementPrototype()->class('ajax');
		$form->addText('name', 'Název:')
			->setAttribute("class","medium-text");
		$form->addHidden('id');
		$form->addText('schoolYear', 'Školní rok')
			->setAttribute("class","medium-text");
		$form->addSubmit('save', 'Odeslat')
			->setAttribute("class","button btn-success closing");
		if($id != 0) //edit/add ?
		{
			$data = $this->gradeRepository->getById($id);
			$form->setDefaults(array("name"=>$data->name,"schoolYear"=>$data->schoolYear,"id"=>$data->id));
		}
		return $form;
	}

	public function createTeachersForm($id)
	{
		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());

		$users = $this->userRepository->getAllUsers();

		$form->getElementPrototype()->class('ajax');
		$form->addSelect('user', 'Uživatel:',$users)
			->setAttribute("class","medium-text");
		$form->addHidden("grade",$id);
		$form->addSubmit('save', 'Odeslat')
			->setAttribute("class","button btn-success closing");

		return $form;
	}
}
