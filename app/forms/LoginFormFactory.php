<?php

namespace App\Forms;

use App\Model;
use App\Model\Repositories\UserRepository;
use Nette,
	Nette\Application\UI\Form,

	Nette\Security\User;
use Instante\Bootstrap3Renderer\BootstrapRenderer;

/**
 * Class LoginFormFactory
 * @package App\Forms
 */
class LoginFormFactory extends Nette\Object
{
	/** @var User */
	private $user;

	/**
	 * @param UserRepository $userRepository
	 */
	protected $userRepository;

	public function __construct(User $user,UserRepository $userRepository)
	{
		$this->user = $user;
		$this->userRepository = $userRepository;
	}

	/**
	 * form for login user to system
	 * @return Form
	 */
	public function create()
	{
		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());

		$form->addText('email', 'Email:')
			->setAttribute("class","medium-text")
			->setRequired('Zadejte prosím přihlašovací email.');

		$form->addPassword('password', 'Heslo:')
			->setAttribute("class","medium-text")
			->setRequired('Prosím zadejte Vaše tajné heslo.');

		$form->addCheckbox('remember', 'Neodhlašovat');

		$form->addSubmit('send', 'Přihlásit se!')
			->setAttribute("class","btn btn-success closing");
		return $form;
	}

	/**
	 * form for add device what users use
	 * @param $userId - id of user to add
	 *
	 * @return \Nette\Application\UI\Form
	 */
	public function createDevices($userId)
	{

		$devices = $this->userRepository->getById($userId)->getDevices();

		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());
		$form->getElementPrototype()->class('ajax');

		$i = 1;
		foreach($devices as $device)
		{
			$form->addText("dev".$device->id,"Zařízení ".$i)
				->setDefaultValue($device->name); //show all the insered devices already
				$i++;
		}
		$form->addText(0,"Nové zařízení"); //place for one new device

		$form->addSubmit('send', 'Odeslat')
			->setAttribute("class","btn btn-success closing");

		return $form;
	}
}
