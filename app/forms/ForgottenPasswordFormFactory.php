<?php

namespace App\Forms;

use App\Model;
use App\Model\Repositories\UserRepository;
use App\Model\UserManager;
use Nette,
	Nette\Application\UI\Form,
	Nette\Security\User;
use Instante\Bootstrap3Renderer\BootstrapRenderer;

class ForgottenPasswordFormFactory extends Nette\Object
{
	/** @var User */
	private $user;

	/**
	 * @param UserRepository $userRepository
	 */
	protected $userRepository;
	/**
	 * @param UserManager $userManager
	 */
	protected $userManager;

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	/**
	 * @return Form
	 */
	public function createAskForPass()
	{
		$form = new Form;

		$form->addText('email', 'Email:')
			->setRequired('Zadejte email, na který je účet registrován.')
			->addRule(Form::EMAIL, "Zadejte email ve tvaru name@example.com")
			->setAttribute("class","medium-text");

		$form->addSubmit('send', 'Odeslat!')
			->setAttribute("class","btn btn-success closing");;

		return $form;
	}



	/**
	 * @return Form
	 */
	public function createChangePass()
	{
		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());

		$form->addText('email', 'Email:')
			->setAttribute("class","medium-text")
			->setRequired('Zadejte email, na který je účet registrován.')
			->addRule(Form::EMAIL, "Zadejte email ve tvaru name@example.com")
			;

		$form->addPassword('password1', 'Heslo:')
			->setAttribute("class","medium-text")
			->setRequired('Prosim zadejte Vaše tajné heslo.')
			->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 6)
			->setRequired('Prosím zadejte Vaše tajné heslo.');

		$form->addPassword('password2', 'Ověření hesla:')
			->setAttribute("class","medium-text")
			->setRequired('Prosim zadejte Vaše tajné heslo.')
			->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 6)
			->addConditionOn($form['password1'], Form::VALID)
			->setRequired('Prosím zadejte Vaše tajné heslo.');

		$form->addSubmit('send', 'Resetovat heslo!')
			->setAttribute("class","btn btn-success closing" );

		return $form;
	}
	


}
