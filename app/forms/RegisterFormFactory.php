<?php

namespace App\Forms;

use App\Model;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\GradeRepository;
use App\Model\UserManager;
use Nette,
	Nette\Application\UI\Form,
	Nette\Security\User;
use Instante\Bootstrap3Renderer\BootstrapRenderer;

/**
 * Class RegisterFormFactory
 * @package App\Forms
 */
class RegisterFormFactory extends Nette\Object
{
	/** @var User */
	private $user;

	/**
	 * @param UserRepository $userRepository
	 */
	protected $userRepository;

	/**
	 * @param GradeRepository $gradeRepository
	 */
	public $gradeRepository;

	/**
	 * @param UserManager $userManager
	 */
	protected $userManager;

	public function __construct(User $user,GradeRepository $gradeRepository)
	{
		$this->user = $user;
		$this->gradeRepository = $gradeRepository;
	}

	

	/**
	 * form for registration (or update user)
	 * @return Form
	 */
	public function create($values = NULL)
	{
		$select = $this->gradeRepository->getClasses();

		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());
		if($values != NULL)
		{
			$form->getElementPrototype()->class("ajax");
		}
		$form->addText('name', 'Jméno a příjmení:')
			->setRequired('Zadejte prosím jméno a přijmení.')
			->setAttribute("class","medium-text")
			->setAttribute("placeholder","Jan Novák");

		$form->addText('email', 'Email:')
			->setRequired('Zadejte prosím přihlašovací email.')
			->addRule(Form::EMAIL, "Zadejte email ve tvaru name@example.com")
			->setAttribute("class","medium-text")
			->setAttribute("placeholder","jan.novak@mail.com");

		$form->addPassword('password1', 'Heslo:', 20)
//
			->setAttribute("class","medium-text");

		$form->addPassword('password2', 'Ověření hesla:', 20)
			->setAttribute("class","medium-text")
			->addConditionOn($form['password1'], Form::VALID)
			->addRule(Form::EQUAL, 'Zadaná hesla se neshodují.', $form['password1']);


		$form->addSelect('grade', 'Třída:', $select)->setAttribute("class","medium-text");

		$form->addHidden("id","0");

		//true -> update user | false register

		if($values != NULL) //update
		{
			$form->setDefaults($values);
		}
		else // password is not required for update | -> register
		{

			$form->getElementPrototype()->id("password1")->setRequired('Prosím zadejte Vaše tajné heslo.')
				->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 6)
			;
			$form->getElementPrototype()->id("password2")->setRequired('Prosím zadejte Vaše tajné heslo.');

		}
		$form->addSubmit('send', 'Odeslat')
			->setAttribute("class","btn  btn-success");;

		return $form;
	}




}
