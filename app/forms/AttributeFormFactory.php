<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 28.3.15
 * Time: 15:07
 */

namespace App\Forms;

use App\Model;
use App\Model\Repositories\AttributeRepository;
use Nette,
	Nette\Application\UI\Form;
use Instante\Bootstrap3Renderer\BootstrapRenderer;

class AttributeFormFactory extends Nette\Object
{

	/**
	 * @param AtributeRepository $atributeRepository
	 */
	protected $attributeRepository;

	public function __construct(AttributeRepository $attributeRepository)
	{
		$this->attributeRepository = $attributeRepository;
	}

	/**
	 * form for add/edit attribute
	 * @return Form
	 */
	public function create($id)
	{
		$types = array("input"=>"Textové pole", "textarea"=>"Textová oblast", "select"=>"Rozklikávací nabídka","file"=>"Fotografie"); //translation of types of attributes

		if($id != 0) //if 0 then it is edit
		{
			$data = $this->attributeRepository->getById($id);
			if($data->type == "select")
			{
				$options = $this->attributeRepository->getSelect($id);
			}
		}

		$form = new Form;
		$form->setRenderer(new BootstrapRenderer); //set the bootstrap renderer

		$form->getElementPrototype()->class('ajax');
		$form->addHidden("id",$id); //id of editing atteibute
		$form->addText('name', 'Název:')
			->setAttribute("class","medium-text"); //size of input

		$form->addText('placeholder', 'Placeholder')
			->setAttribute("class","medium-text");

		$form->addText('regexp', 'Regexp')
			->setAttribute("class","medium-text");

		$form->addSelect('type', 'Typ',$types)->setAttribute("class","selectTypeAttribute medium-text");

		$i = 1; //start counting for "volba"
		if($id != 0 && $data->type == "select")
		{
			foreach($options as $opt) //for all the options...
			{
				$form->addText("options".$i)
					->setAttribute("class","medium-text")
					->setAttribute("placeholder","Volba ".$i)
					->setDefaultValue($opt->name)
					->setAttribute("data-num",$i);
			$i++;
			}
		}
		else //it is new att -> than show the first option
		{
			$form->addText("options1")
				->setAttribute("class","hiddenOption medium-text")
				->setAttribute("placeholder","Volba 1")
				->setAttribute("data-num",1);
		}

		$form->addButton("next_option","Další")
			->setAttribute("class","hiddenOption")
			->setAttribute("id","next_option");

		$form->addCheckbox('require', 'Povinné'); //checkboxes
		$form->addCheckbox('latin', 'Latinsky');
		$form->addCheckbox('search', 'Vyhledávat');
		$form->addCheckbox('showInTable', 'Zobraz v tabulce');

		if($id != 0) //if it is editing, than load default values
		{
			$form->setDefaults(array("name"=>$data->name,"placeholder"=>$data->placeholder,"id"=>$data->id,"regexp"=>$data->regexp,"type"=>$data->type,"require"=>$data->require,"latin"=>$data->latin,"search"=>$data->search));
		}
		$form->addSubmit('save', 'Odeslat')
			->setAttribute("class","button btn-success closing");

		return $form;
	}

	/**
	 * just show form, no action
	 * @return \Nette\Application\UI\Form
	 */
	public function showForm()
	{
		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());

		$attributes = $this->attributeRepository->getAttributes();

		foreach($attributes as $attribute)
		{
			$suffix = "";
			if($attribute->latin == true)
			{
				$suffix = " (lat)";
			}
			switch($attribute->type)
			{
				case "input":
				{
					$form->addText("att".$attribute->id,$attribute->name.$suffix)
						->setAttribute("placeholder",$attribute->placeholder);
					if($attribute->require == true)
					{
						$form["att".$attribute->id]->setRequired();
					}
				};break;

				case "textarea":
				{
					$form->addTextArea("att".$attribute->id,$attribute->name.$suffix)
						->setAttribute("placeholder",$attribute->placeholder);
					if($attribute->require == true)
					{
						$form["att".$attribute->id]->setRequired();
					}
				};break;

				case "select":
				{

					$data = $this->attributeRepository->getSelect($attribute->id);
					$ar = array();
					foreach($data as $item)
					{
						$ar[$item->id] = $item->name;
					}
					$form->addSelect("att".$attribute->id,$attribute->name.$suffix,$ar);
				};break;
				//file is not important because it is rendered separetly so do not show it
//				case "file":
//				{

//				};

			}
		}
		return $form;
	}
}
