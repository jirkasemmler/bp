<?php

namespace App\Forms;

use App\Model;
use App\Model\Repositories\TaxonomyRepository;
use Nette,
    Nette\Application\UI\Form;
use Instante\Bootstrap3Renderer\BootstrapRenderer;


class TaxonomyTypeFormFactory extends Nette\Object
{

    /**
     * @param TaxonomyRepository $taxonomyRepository
     */
    protected $taxonomyRepository;

    public function __construct(TaxonomyRepository $taxonomyRepository)
    {
//        $this->user = $user;
        $this->taxonomyRepository = $taxonomyRepository;
    }

    /**
     * add new taxonomy
     * @return Form
     */
    public function create($id)
    {
        $form = new Form;
        $form->setRenderer(new BootstrapRenderer());

        $form->getElementPrototype()->class('ajax');
        $form->addText('name', 'Název:')
            ->setAttribute("class","medium-text");
        $form->addHidden('id');

        $form->addSubmit('save', 'Odeslat')
            ->setAttribute("class","button btn-success closing");
        if($id != 0) //edit?
        {
            $data = $this->taxonomyRepository->getType($id);
            $form->setDefaults(array("name"=>$data->name,"id"=>$data->id));
        }
        return $form;
    }


}
