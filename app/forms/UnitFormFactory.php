<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 5.4.15
 * Time: 2:22
 */

namespace App\Forms;
use App\Model;
use Nette,
	Nette\Application\UI\Form;
use App\Model\Repositories\DirectoryRepository;
use App\Model\Repositories\AttributeRepository;
use App\Model\Repositories\UnitRepository;
use App\Model\Repositories\GradeRepository;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\MessageRepository;
use App\Forms\AttributeFormFactory;
use Instante\Bootstrap3Renderer\BootstrapRenderer;


class UnitFormFactory {

	/**
	 * @param DirectoryRepository $directoryRepository
	 */
	protected $directoryRepository;

	protected $attributeRepository;
	protected $gradeRepository;

	public function __construct(
		DirectoryRepository $directoryRepository,
		AttributeFormFactory $attributeFormFactory,
		AttributeRepository $attributeRepository,
		UnitRepository $unitRepository,
		MessageRepository $messageRepository,
		GradeRepository $gradeRepository,
		UserRepository $userRepository)
	{ //all these repositories I need. really
		$this->unitRepository = $unitRepository;
		$this->userRepository = $userRepository;
		$this->messageRepository = $messageRepository;
		$this->gradeRepository = $gradeRepository;
		$this->directoryRepository = $directoryRepository;
		$this->attributeRepository = $attributeRepository;
		$this->attributeFormFactory = $attributeFormFactory;
	}

	private $types = ["private"=>"Osobní","public"=>"Veřejný","school"=>"Školní"];

	/**
	 * form to add/edit dir
	 * @return Form
	 */
	public function createDir($id)
	{
		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());

		$form->getElementPrototype()->class('ajax');

		$form->addText("name","Název")
			->setAttribute("placeholder","Jarní rostliny")
			->setRequired("Název je povinné pole");

		$form->addTextArea("desc","Popis")
			->setAttribute("placeholder","Můj herbář jarních rostlin");

		$form->addSelect("type","Typ",$this->types);
		$form->addHidden('id',$id);

		$form->addSubmit('save', 'Odeslat')
			->setAttribute("class","button btn-success");

		if($id != 0) //edit or add?
		{
			$data = $this->directoryRepository->getById($id);
			$form->setDefaults(array("name"=>$data->name,"desc"=>$data->desc,"id"=>$data->id,"type"=>$data->type));
		}
		return $form;
	}

	/**
	 * form to add/unit
	 * @param     $dirId - id of dir to add to
	 * @param     $unitId - id of unit to edit
	 * @param int $userId - id of user
	 *
	 * @return \Nette\Application\UI\Form
	 */
	public function create($dirId,$unitId,$userId = 0)
	{
		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());

		$attributes = $this->attributeRepository->getAttributes();

		$addToEnd = [];
		$defaults = [];
		foreach($attributes as $attribute)
		{
			switch($attribute->type)
			{
				case "input":
				{
					$form->addText("att".$attribute->id,$attribute->name)
						->setAttribute("placeholder",$attribute->placeholder)
						->setAttribute("class","medium-text");
					if($attribute->require == true)
					{
						$form["att".$attribute->id]->setRequired("Toto pole je povinné!");
					}
				};break;

				case "textarea":
				{
					$form->addTextArea("att".$attribute->id,$attribute->name)
						->setAttribute("placeholder",$attribute->placeholder)
						->setAttribute("class","medium-text");
					if($attribute->require == true)
					{
						$form["att".$attribute->id]->setRequired("Toto pole je povinné!");
					}
				};break;
				case "select":
				{

					$data = $this->attributeRepository->getSelect($attribute->id); //get values of select
					$ar = array();
					foreach($data as $item)
					{
						$ar[$item->id] = $item->name;
					}
					$form->addSelect("att".$attribute->id,$attribute->name,$ar)
						->setAttribute("class","medium-text");
				};break;
				case "file":
				{
					$tmp = ["id"=>$attribute->id,"req"=>$attribute->require];
					$addToEnd[] = $tmp;
					//just add text inputs. the actual upload will care the JS plugin about. from there inputs I just get IDs of uploaded files
				};

			}
			if($unitId != 0) //Edit
			{
				$defaults["att".$attribute->id] = $this->attributeRepository->getDefaultValue($unitId,$attribute->id);
			}

		}
		if($userId == 0)
		{
			if($dirId == 0)
			{
				$userId = $this->unitRepository->getById($unitId)->getUser();
			}
			else
			{
				$userId = $this->directoryRepository->getById($dirId)->userId;
			}
		}

		$dirs = $this->directoryRepository->findBy(["user_id"=>$userId,"status"=>"open"]);

		$unitsDirs = $this->unitRepository->getUnitsDirs($unitId); //get dirs already set for the unit

		$arDir = [];
		foreach($dirs as $dir) //show all the dirs
		{
			$arDir[$dir->id] = $dir->name;
		}
		if($dirId != 0)
		{
			$unitsDirs[] = $dirId;
		}

		$form->addCheckboxList('dirs', 'Herbáře:', $arDir);

		$form["dirs"]->setDefaultValue($unitsDirs);

		$devices = $this->userRepository->getDevices($userId);

		if(empty($devices)) //did you add a device? no, do it now!
		{
			$form->addText("deviceText","Fotoaparát")
				->setRequired("Toto pole je povinné!")
				->setAttribute("class","medium-text");
		}
		else //choose the device
		{
			$form->addSelect("device","Foťák*")
				->setItems($devices)
				->setAttribute("class","medium-text")
				->setRequired("Toto pole je povinné!");
		}

		$form->addHidden("unit",$unitId);

		$form->addHidden("dir",$dirId);
		$form->addHidden("taxonomySelect",0);
		$form->addSubmit("send","Odeslat")->setAttribute("class","btn-success medium-text");

		foreach($addToEnd as $attToEnd)
		{
			$form->addText("att".$attToEnd["id"])->setAttribute("class","hidden"); //it is text because i need to check if it is null ( requiured) - oh god, coding drunk is not a good idea
			if($attToEnd["req"] == true) //all the hidden inputs get some place because of padding, so add it to the end
			{
				$form["att".$attToEnd["id"]]->setRequired("Nezadal/a jsi některý povinný záběr!");
				$form["att".$attToEnd["id"]]->getLabelPrototype()->addClass("requiredFile");
			}
		}
		$form->setDefaults($defaults);
		return $form;
	}

	/**
	 * form to add rating to unit
	 * @param $id - id of unit to rate
	 *
	 * @return \Nette\Application\UI\Form
	 */
	public function createRating($id)
	{

		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());
		$form->getElementPrototype()->class('ajax');

		$unit = $this->unitRepository->getById($id);
		$form->addText("rating","Hodnocení [%]")->setAttribute("class","sliderValue medium-text");
		$form->addTextArea("comment","Komentář");
		$form->addHidden("unit",$id);
		if($id != 0)
		{
			$defs = ["rating"=>$unit->rating,"comment"=>$unit->comment];
			$form->setDefaults($defs);
		}
		$form->addSubmit('save', 'Odeslat')
			->setAttribute("class","button btn-success closing");

		return $form;
	}

	/**
	 * form to add rating to unit
	 * @param $id - id of unit to rate
	 *
	 * @return \Nette\Application\UI\Form
	 */
	public function createRatingDir($id)
	{

		$form = new Form;
		$form->setRenderer(new BootstrapRenderer());
		$form->getElementPrototype()->class('ajax');

		$dir = $this->directoryRepository->getById($id);
		$form->addText("rating","Hodnocení [%]")->setAttribute("class","sliderValue medium-text");
		$form->addTextArea("comment","Komentář");
		$form->addHidden("dir",$id);
		if($id != 0)
		{
			$defs = ["rating"=>$dir->rating,"comment"=>$dir->comment];
			$form->setDefaults($defs);
		}
		$form->addSubmit('save', 'Odeslat')
			->setAttribute("class","button btn-success closing")
			->setAttribute("data-dismiss","modal");

		return $form;
	}

	/**
	 * form to send a msg
	 * @param     $user
	 * @param int $unit
	 *
	 * @return \Nette\Application\UI\Form
	 */
	public function createMsg($user,$unit = 0){

		$form = new Form;

		$form->setRenderer(new BootstrapRenderer());
		$form->getElementPrototype()->class('ajax');
		if($unit != 0)
		{
			$form->addHidden("unit",$unit);
			$target = $this->unitRepository->getById($unit)->getUser();
			$form->addHidden("target",$target);
		}

		if($user != 0)
		{
			$form->addText("name","Jméno")->setAttribute("readonly");

			$form->addText("email","E-mail")->setAttribute("readonly");
			$userObj = $this->userRepository->getByID($user);
			$defs = ["name"=>$userObj->name,"email"=>$userObj->email];

			$form->setDefaults($defs);
		}
		else
		{
			$form->addText("name","Jméno");
			$form->addText("email","E-mail");
		}


		$form->addTextArea("text","Zpráva");
		$form->addSubmit("Odeslat")->setAttribute("class","closing btn-success");

		return $form;
	}

	/**
	 * form to send a reply
	 * @param $threadId
	 *
	 * @return \Nette\Application\UI\Form
	 */
	public function createReply($threadId)
	{

		$lastReply = $this->messageRepository->findBy(["thread_id"=>$threadId])->orderBy("id DESC")->limit(1);
		$form = new Form;
		$lastId = 0;
		foreach($lastReply as $reply)
		{
			$lastId = $reply->id;
		}
		$form->setRenderer(new BootstrapRenderer());
		$form->getElementPrototype()->class('ajax');
		$form->addTextArea("text","Odpověď");
		$form->addHidden("parentId");
		$form->addHidden("threadId");
		$form->setDefaults(["parentId"=>$lastId,"threadId"=>$threadId]);
		$form->addSubmit("Odeslat")->setAttribute("class","closing btn-success");
		return $form;
	}

	/**
	 * search form
	 * @return \Nette\Application\UI\Form
	 */
	public function createSearch()
	{
		$form = new Form;

		$form->setRenderer(new BootstrapRenderer());

		$atts = $this->attributeRepository->findBySearch("true");

		foreach($atts as $att)
		{
			if($att->type == "input" OR $att->type == "textarea")
			{
				$form->addText("att".$att->id,$att->name)->setAttribute("placeholder",$att->placeholder)->setAttribute("class","medium-text");
			}

			if($att->type == "select")
			{
				$options = $this->attributeRepository->getOptionsArr($att->id);
				$options[0] = "---";
				ksort($options);
				$form->addSelect("sel".$att->id, $att->name,$options)->setAttribute("class","medium-text");
			}
		}

		$gradesAr = [0=>"---"];
		$usersAr = [0=>"---"];
		$grades = $this->gradeRepository->findAll();
		foreach($grades as $grade)
		{
			$gradesAr[$grade->id] = $grade->name;
			$usersAr[$grade->name] = $this->gradeRepository->getUsers($grade->id);
		}
		$form->addSelect("grade","Třída",$gradesAr)->setAttribute("class","medium-text");
		$form->addSelect("user","Student",$usersAr)->setAttribute("class","medium-text");
		$form->addSubmit("Vyhledat")->setAttribute("class","btn-success");

		return $form;
	}
}