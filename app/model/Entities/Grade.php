<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 7.3.15
 * Time: 22:51
 */

namespace App\Model\Entities;

use YetORM\EntityCollection;

/**
 * Class User
 * @package App\Model
 * @property-read int $id
 * @property string $name
 * @property bool $registration
 * @property string $schoolYear -> school_year
 */
class Grade extends Entity{

	/**
	 * returns all users in the grade
	 * @return array
	 */
	public function getUsers()
	{
		return $this->toRecord()->related("user")->fetchAll();
    }

	/**
	 * returns number of students in the class
	 * @return int
	 */
	public function getUsersCount()
	{
		return $this->toRecord()->related("user")->count();
	}

	/**
	 * return teacher of the grade
	 * @return mixed
	 */
	public function getTeacher()
	{
		return  $this->toRecord()->related("user")->select("user.*")->where(["user.user_type"=>"teacher"])->fetch();

	}


	/**
	 * return teacher of the grade
	 * @return mixed
	 */
	public function getTeachers()
	{
		return  $this->toRecord()->related("user")->select("user.*")->where(["user.user_type"=>"teacher"])->fetchAll();

	}

}