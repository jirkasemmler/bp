<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 24.3.15
 * Time: 0:14
 */
namespace App\Model\Entities;

use YetORM\EntityCollection;


/**
 * Class Attribute
 * @package App\Model
 * @property-read int $id
 * @property string $name
 * @property string $type
 * @property bool $require
 * @property string $placeholder
 * @property string $regexp
 * @property int $order
 * @property bool $state
 * @property bool $showInTable -> show_in_table
 * @property bool $latin
 * @property bool $search
 */
class Attribute extends Entity{

}