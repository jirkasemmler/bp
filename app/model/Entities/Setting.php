<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 14.3.15
 * Time: 22:09
 */

namespace App\Model\Entities;

use YetORM\EntityCollection;

/**
 * Class Setting
 * @package App\Model
 * @property-read int $id
 * @property string $key
 * @property string $value
 * @property string|NULL $note
 */
class Setting extends Entity {

}