<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 24.3.15
 * Time: 0:14
 */
namespace App\Model\Entities;

use YetORM\EntityCollection;


/**
 * Class Taxonomy
 * @package App\Model
 * @property-read int $id
 * @property string $name
 * @property int $parentId -> id_parent
 * @property int $type
 */
class Taxonomy extends Entity{

}