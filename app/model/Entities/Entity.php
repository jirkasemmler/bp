<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 7.3.15
 * Time: 22:51
 */

namespace App\Model\Entities;

use Nette\Utils\ArrayHash;
use YetORM\EntityCollection;
use YetORM\Reflection\MethodProperty;

class Entity extends \YetORM\Entity
{
	public static function from(\Traversable $values)
	{
		$self = new static();
		foreach ($values as $name => $value) {
			$self->$name = $value;
		}

		return $self;
	}

	public function loadValues(ArrayHash $values)
	{
		foreach ($values as $name => $field) {
			$this->$name = $field;
		}
	}

	/** @return ArrayHash */
	public function toArray()
	{
		$ref = static::getReflection();
		$values = new ArrayHash();
		foreach ($ref->getEntityProperties() as $name => $property) {
			if ($property instanceof MethodProperty) {
				$value = $this->{'get' . $name}();
			} else {
				$value = $this->$name;
			}
			if (!($value instanceof EntityCollection || $value instanceof Entity)) {
				$values[$name] = $value;
			}
		}

		return $values;
	}

	public static function getClassName()
	{
		return get_called_class();
	}
}