<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 7.3.15
 * Time: 22:51
 */

namespace App\Model\Entities;

use YetORM\EntityCollection;

/**
 * Class User
 * @package App\Model
 * @property-read int $id
 * @property string $email
 * @property string $user
 * @property string $name
 * @property string|NULL $password
 * @property string $type -> user_type
 * @property string|NULL $passToken -> pass_token
 * @property \DateTime $dateReg -> date_reg
 * @property int|NULL $avatar
 */
class User extends Entity{

	/**
	 * get all grades which user have
	 * @return \YetORM\EntityCollection
	 */
	public function getUserGrade()
	{
		$selection = $this->record->related("grade_user");
		return new EntityCollection($selection,Grade::getClassName(),'grade_id');
	}

	/**
	 * get user avatar image
	 * @return \App\Model\Entities\File|bool
	 */
	function getFileAvatar()
	{
		$tmp = $this->record->ref('file', 'avatar');

		if($tmp === NULL)
		{
			return false;
		}
		else
		{
			return new File($tmp);
		}
	}

	/**
	 * get number of units of the user in all public dirs
	 * @return int
	 */
	public function getUnitsCount($type = "public")
	{
		$sum = 0;
		$dirs = $this->toRecord()->related("directory.user_id")->where(["directory.type"=>$type]);
		foreach($dirs as $dir)
		{
			$sum += $dir->related("unit_directory.directory_id")->count();
		}
		return $sum;
	}


	/**
	 * returns first directory of the user
	 * @return int
	 */
	public function getFirstDir()
	{
		if($this->toRecord()->related("directory")->fetch() == NULL){
			return 0;
		}
		return $this->toRecord()->related("directory")->fetch()->id;
	}

	/**
	 * get all the devices which user use
	 * @return \Nette\Database\Table\GroupedSelection
	 */
	public function getDevices()
	{
		return $this->toRecord()->related("device");
	}

}