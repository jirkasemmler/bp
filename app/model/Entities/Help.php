<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 1.5.15
 * Time: 11:30
 */

namespace App\Model\Entities;

use YetORM\EntityCollection;

/**
 * Class User
 * @package App\Model
 * @property-read int $id
 * @property string $key
 * @property string $title
 * @property string $text
 */
class Help extends Entity{

}