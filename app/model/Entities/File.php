<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 31.3.15
 * Time: 22:04
 */

namespace App\Model\Entities;

use YetORM\EntityCollection;


/**
 * Class Attribute
 * @package App\Model
 * @property-read int $id
 * @property int $userId -> user_id
 * @property string $name -> file_name
 * @property bool $checked
 * @property \DateTime $date
 */
class File extends Entity {


}
