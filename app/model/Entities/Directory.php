<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 5.4.15
 * Time: 2:11
 */

namespace App\Model\Entities;


/**
 * Class Attribute
 * @package App\Model
 * @property-read int $id
 * @property int $userId -> user_id
 * @property string $name
 * @property string $desc
 * @property string $type
 * @property string $status
 * @property string $comment
 * @property int|NULL $rating
 */
class Directory extends Entity{

	/**
	 * returns author of the firectory
	 * @return mixed
	 */
	public function getAuthor()
	{
		return $this->record->user->fetch();
	}


	/**
	 * get average of rating and percentage of rated units
	 * @return array
	 */
	public function avgRating()
	{
		$sum = 0;
		$count = 0;
		$total = 0;
		$units = $this->record->related("unit_directory");
		foreach($units as $unit)
		{
			$total++;
			if($unit->unit->rating != 0)
			{
				$sum += $unit->unit->rating;
				$count++;
			}
		}
		$avg = $count == 0 ? 0 : $sum/$count; //to prevent division by zero
		$rated = $total == 0 ? 0 : $count/$total; //to prevent division by zero
		return ["avg"=>$avg, "rated"=>$rated*100];
	}
}