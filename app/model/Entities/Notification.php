<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 11.4.15
 * Time: 16:51
 */

namespace App\Model\Entities;


use YetORM\EntityCollection;

/**
 * Class Notification
 * @package App\Model
 * @property-read int $id
 * @property int|NULL $userId -> user_id
 * @property int|NULL $eventId -> event_id
 * @property \DateTime $date -> date
 * @property string $text
 * @property bool $read
 * @property string $type
 */
class Notification extends Entity {

}