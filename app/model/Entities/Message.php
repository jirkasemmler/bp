<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 11.4.15
 * Time: 11:44
 */

namespace App\Model\Entities;


use YetORM\EntityCollection;

/**
 * Class Message
 * @package App\Model
 * @property-read int $id
 * @property int|NULL $authorId -> author_id
 * @property int|NULL $targetId -> target_id
 * @property int|NULL $unitId -> unit_id
 * @property int|NULL $parentId -> parent_id
 * @property int|NULL $threadId -> thread_id
 * @property \DateTime $date -> date
 * @property string $text
 * @property string $email
 * @property string $name
 * @property bool $read
 */
class Message extends Entity{

}