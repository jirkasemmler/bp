<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 5.4.15
 * Time: 1:01
 */

namespace App\Model\Entities;

use YetORM\EntityCollection;


/**
 * Class Unit
 * @package App\Model
 * @property-read int $id
 * @property int|NULL $taxonomy -> taxonomy_id
 * @property int $rating -> rating
 * @property string $comment -> teacher_comment
 * @property int $device -> device_id
 */
class Unit extends Entity{

	/**
	 * returns ID of owner of the unit
	 * @return mixed
	 */
	public function getUser()
	{
		return $this->record->related("unit_directory")->fetch()->directory->user_id;
	}

	/**
	 * returns Name of te user
	 * @return mixed
	 */
	public function getUserName()
	{
		return $this->record->related("unit_directory")->fetch()->directory->user->name;
	}

	public function getUserEmail()
	{
		return $this->record->related("unit_directory")->fetch()->directory->user->email;
	}

	/**
	 * returns name of the device which the user took the picture with
	 * @return mixed
	 */
	public function getDeviceName()
	{
		return $this->record->device->name;
	}

	/**
	 * get rule if show the directory or not
	 * @return mixed
	 */
	public function getDirRule()
	{
		return $this->record->related("unit_directory")->fetch()->directory->type;
	}

	/**
	 * get the name of the unit. name is the first text attribute from the form
	 * @return mixed
	 */
	public function getName(){
		return $this->record->related("unit_attribute")->where(["attribute.type"=>"input","attribute.show_in_table"=>true])->fetch()["value"];
	}

	/**
	 * returns array of types where is the unit included
	 */
	public function getDirsTypes()
	{
		$ret = [];
		$tmp = $this->toRecord()->related("directory");
		foreach($tmp as $i)
		{
			$ret[] = $i->directory["type"];
		}
		return $ret;
	}

	/**
	 * @return bool
	 */
	public function isSchoolType()
	{
		return in_array("school",$this->getDirsTypes());
	}
}