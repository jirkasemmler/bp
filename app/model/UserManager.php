<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 1.3.15
 * Time: 23:01
 */

namespace App\Model;

use App\Model;
use App\Model\Repositories\UserRepository;
use App\Model\Repositories\SettingRepository;
use Nette,
	App\Model\Entities\User,
	Nette\Utils\Strings,
	Nette\Security\Passwords
	;
use Nette\Extras;

/**
 * Users management.
 */
class UserManager extends Nette\Object
{
	/**
	 * @var $userRepository Model\Repositories\UserRepository
	 */
	public $userRepository;
	/**
	 * @var $settingRepository Model\Repositories\SettingRepository
	 */
	public $settingRepository;
	/**
	 * sets repositories to use
	 * @param Model\Repositories\UserRepository $userRepository
	 * @param Model\Repositories\SettingRepository $settingRepository
	 */
	public function __construct(UserRepository $userRepository, SettingRepository $settingRepository)
	{
		$this->settingRepository = $settingRepository;
		$this->userRepository = $userRepository;
	}

	/**
	 * registers user to the system, sends email about the registration
	 * @param $values
	 * @return string
	 */
	public function registerUser($values)
	{

		if($values->password1 == $values->password2)
		{
			
			/**
			* @param User $user
			*/
			$user = new User(); //new entity user

			$user->email = $values->email;
//			$user->password = $this->generateHash($values->password1);
			$user->password = Passwords::hash($values->password1);
			$user->name = $values->name;
			$user->dateReg = new \DateTime();
			if($this->userRepository->findAll()->count() == 0) //the database is empty
			{
				$user->type = 'teacher'; //user is complete
			}
			else
			{
				$user->type = 'student'; //user is complete
			}
		  

			try{
				$row = $this->userRepository->persist($user,$values->grade); //try to write to DB
			}
			catch(\Exception $e) //duplicate data on UNIQUE index -> email
			{
				if($e->getCode() == 23000)
				{
					return "DUPLICATE"; //duplicate data
				}
				return "ERR";
			}

			return "OK"; //everything is ok
		}
		else
		{
			return "PASS"; //password and passsword verify are not equal. shlould not happend from the form, but shit happen
		}
	}

	/**
	 * tries to find a user due the email in values. If it finds, generates an email with link to set new password
	 * @param $values data with email of user who wants to reset password
	 * @return boolean succ or not
	 */
	public function generateResetLink($values)
	{
		$user = $this->userRepository->getByEmail($values->email);
		if($user)
		{

			$randomCount = rand(50,60);
			$token = Strings::random($randomCount,'0-9a-z');
			$user->passToken = $token;

			try {
				$this->userRepository->persist($user);

				return $token;
			}
			catch(\Exception $e)
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	/**
	 * udpates user
	 * @param $id - user id to reset pass for
	 * @param $values - array with new passwords
	 * @return bool
	 */
	public function updateUser($id,$values)
	{
		//if id == null -> just reset password
		//else update the whole user profile
		if($id == NULL) //dont know the user, get it by email because it is from reset pass
		{
			$user = $this->userRepository->getByEmail($values->email);
			$user->password = Passwords::hash($values->password1);
		}
		else //i know the user -> get it by id
		{
			$user = $this->userRepository->getById($id);
			$user->name = $values->name;
			$user->email = $values->email;
			if($values->password1 != NULL)
			{
				$user->password = Passwords::hash($values->password1);
			}
		}
		$user->passToken = ""; //reset token for reset password
		try{
			$this->userRepository->persist($user,$values->grade);
			return true;
		}
		catch(\Exception $e)
		{
			return false;
		}

	}

	/**
	 * returns boolean if is able to register or not
	 * @return bool
	 */
	public function isRegistrationTime()
	{
		$row = $this->settingRepository->getByKey("registrationTime");

		if($row->value == true)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * @param $token - security token
	 * @return bool
	 */
	public function isTokenValid($token)
	{
		$user = $this->userRepository->getByPassToken($token);
		if($user)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * returns array of profile (due to the ID)
	 * @param $id
	 * @return array
	 */
	public function getProfile($id)
	{

		$row = $this->userRepository->getById($id);
		$row->password = NULL;

		$ret = ["name"=>$row->name, "email"=>$row->email, "user_type"=>$row->type, "date_reg"=>$row->dateReg,"id"=>$row->id];

		return $ret;

	}

	public function updateAvatar($idUser,$idFile)
	{
		$user = $this->userRepository->getById($idUser);
		$user->avatar = $idFile;

		$this->userRepository->persist($user);
	}

}

class DuplicateNameException extends \Exception
{}
