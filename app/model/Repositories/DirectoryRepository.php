<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 5.4.15
 * Time: 2:11
 */

namespace App\Model\Repositories;

use App\Model\Entities\Directory;
use YetORM\Repository;


/**
 * Class DirectoryRepository
 * @table directory
 * @entity Directory
 */
class DirectoryRepository extends Repository{

	/**
	 * add new or update directory
	 * @param $data
	 * @param $userId
	 */
	public function manage($data,$userId)
	{
		if($data["id"] == 0) //new directory
		{
			$dir = new Directory();
		}
		else //update
		{
			$dir = $this->getById($data["id"]);
		}

		$dir->name = $data["name"]; //set data
		$dir->desc = $data["desc"];
		$dir->type = $data["type"];
		$dir->userId = $userId;
		if(isset($data["status"]))
		{
			$dir->status = $data["status"];
		}

		$this->persist($dir);
	}

	/**
	 * delete directory (not to delete just set as deleted
	 * @param $id
	 *
	 * @return bool
	 */
	public function del($id)
	{
			$dir = $this->getById($id);
			$dir->status = "deleted"; //dotn delte but set as deleted
			$this->persist($dir);
		return true;
	}

	/**
	 * saving rating of the directory
	 * @param $data
	 *
	 * @return bool
	 */
	public function saveRating($data)
	{
		$dir = $this->getById($data->dir);
		$dir->comment = $data->comment;
		$dir->rating = (int)$data->rating;
		$this->persist($dir);
		return true;
	}

}