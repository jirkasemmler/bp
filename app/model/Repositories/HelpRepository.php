<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 1.5.15
 * Time: 11:30
 */

namespace App\Model\Repositories;


use App\Model\Entities\Help;

use YetORM\Repository;


/**
 * Class GradeRepository
 * @table help
 * @entity Help
 */
class HelpRepository extends Repository{

	/**
	 * returns title and text of help
	 * @param $key - key to find by
	 */
	public function getHelp($key)
	{
		return $this->getByKey($key);
	}
}