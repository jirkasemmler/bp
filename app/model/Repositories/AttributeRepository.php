<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 28.3.15
 * Time: 11:36
 */

namespace App\Model\Repositories;
use YetORM\Repository;
use App\Model\Entities\Attribute;

/**
 * Class AttributeRepository
 * @table attribute
 * @entity Attribute
 */
class AttributeRepository extends Repository{

	/**
	 * get all attributes in the correct order
	 * @return \YetORM\EntityCollection
	 */
	public function getAttributes()
	{
		return $this->findAll()->orderBy("order");
	}

	/**
	 * insert/update attribute
	 * @param $data - data from form
	 */
	public function manage($data)
	{
		if($data["id"] == 0) //insert -> make new entity
		{
			$attribute = new Attribute();

			$lastItem = $this->getTable("attribute")->select("*")->order("order DESC")->limit(1)->fetch(); //last number of order in the table
			if($lastItem == NULL) //no rows in table -> position is 1
			{
				$newOrder = 1;
			}
			else
			{
				$newOrder = $lastItem->order+1; //inc by 1 -> next order
			}
			$attribute->order = $newOrder; //check the order
		}
		else //it is update
		{
			$attribute = $this->getById($data["id"]);
			$this->getTable("attribute_option")->where(["attribute_id"=>$attribute->id])->delete();
		}

		$attribute->name = $data["name"]; //fill it with data
		$attribute->placeholder = $data["placeholder"];
		$attribute->regexp = $data["regexp"];
		$req = false;
		if(isset($data["require"]))
		{
			$req = true;
		}

		$show = false;
		if(isset($data["showInTable"]))
		{
			$show = true;
		}

		$latin = false;
		if(isset($data["latin"]))
		{
			$latin = true;
		}

		$search = false;
		if(isset($data["search"]))
		{
			$search = true;
		}


		$attribute->require = $req;
		$attribute->showInTable = $show;
		$attribute->latin = $latin;
		$attribute->search = $search;
		$attribute->state = true;
		$attribute->type = $data["type"];

		$this->persist($attribute); //persisting

		if($data["type"] == "select") //making entries in other tables
		{
			$order = 1;
			foreach($data as $key => $val)
			{
				if(strpos($key,"options") !== false)
				{
					$this->getTable("attribute_option")->insert(["attribute_id"=>$attribute->id,"name"=>$val,"order"=>$order]);
				}

				$order++;
			}
		}
	}

	/**
	 * get options for selectboxes
	 * @return array
	 */
	public function getOptions()
	{
		$selectBoxes = $this->findByType("select"); //get all selectboxes
		$ret = [];
		foreach($selectBoxes as $select) //get options for each selectbox
		{
			$ret[$select->id] = $this->getTable("attribute_option")->select("*")->where(["attribute_id"=>$select->id]);
		}
		return $ret;
	}

	/**
	 * setting new order of rows in table. $data are serialized IDs of rows.
	 * @param $data - serialized IDs of rows (in new order)
	 * @return bool - success|fail
	 */
	public function updateAttributesSort($data)
	{
		$i = 1;
		foreach($data as $key => $val) //for each for...
		{
			$this->getTable("attribute")->where(["id"=>$val])->update(["order"=>$i]); //setting new order
			$i++;
		}
		return true;
	}

	/**
	 * delete attribute
	 * @param $id
	 */
	public function del($id)
	{
		$item = $this->getById($id);
		$this->delete($item);
	}

	/**
	 * get data for one selecbox
	 * @param $id
	 *
	 * @return \Nette\Database\Table\Selection
	 */
	public function getSelect($id)
	{
		return $this->getTable("attribute_option")->where(["attribute_id"=>$id]);
	}

	/**
	 * get optoons in array
	 * @param $id
	 *
	 * @return array
	 */
	public function getOptionsArr($id)
	{
		$ret = [];
		$opts =  $this->getTable("attribute_option")->where(["attribute_id"=>$id]);
		foreach($opts as $opt)
		{
			$ret[$opt->id] = $opt->name;
		}
		return $ret;
	}


	/**
	 * get all attributes which are files for the unit
	 * @param int $unitId
	 *
	 * @return array
	 */
	public function getFileAttributes($unitId = 0)
	{
		$atts = $this->findBy(["type"=>"file","state"=>true]); //get all the attributes which are files and are ready to use
		$ret = []; //array to return
			foreach($atts as $att)
			{
				$item = [];
				if($unitId != 0)
				{
					$attributeSet = $this->getTable("unit_attribute")->select("*")->where(["attribute_id"=>$att->id,"unit"=>$unitId])->fetch();
					if($attributeSet["value"] != NULL) //there exists a file for this attribute and $unitId
					{
						$item["img"] = $attributeSet["value"];
					}
				}
				$item["att"] = $att;
				$ret[] = $item;
			}
		return $ret;
	}


	/**
	 *
	 * @param $idUnit - unit
	 * @param $idAtt - attribute ID
	 *
	 * @return mixed
	 */
	public function getDefaultValue($idUnit,$idAtt)
	{
		return $this->getTable("unit_attribute")->select("*")->where(["unit"=>$idUnit,"attribute_id"=>$idAtt])->fetch()["value"];
	}
}

