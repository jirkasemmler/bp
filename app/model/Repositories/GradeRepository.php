<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * Grade: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 7.3.15
 * Time: 23:01
 */

namespace App\Model\Repositories;

use App\Model\Entities\Grade;
use YetORM\Repository;

/**
 * Class GradeRepository
 * @table grade
 * @entity Grade
 */
class GradeRepository extends Repository{

    /**
     * inserts new grade to db or edits an old one
     * @param $data
     * @return bool
     */
    public function insert($data) //insert new or update old entry
    {
        if($data->id != NULL) //new entry or just edit?
        {
            $grade = $this->getById($data->id); //edit
        }
        else
        {
            $grade = new Grade(); // new
        }
        $grade->name = $data->name; //setting new data
        $grade->schoolYear = $data->schoolYear;

        try
        {
            $this->persist($grade); //update
            return true;
        }
        catch(\Exception $e)
        {
            return false;
        }
    }

    /**
     * deletes grade from system (hard delete, no reason to keep in the db with the delete flag)
     * @param $id - id of grade to delete
     * @return bool
     */
    public function del($id)
    {
        $grade = $this->getById($id);
        try
        {
            $this->delete($grade);
            return true;
        }
        catch(\Exception $e)
        {
            return false;
        }
    }

    /**
     * changes right to register for each grade
     * @param $id - id of grade
     * @param $statue - statue to change for
     * @return bool - success|not
     */
    public function changeRegistration($id, $statue)
    {
        $grade = $this->getById($id);

        if($statue == 0)
        {
            $status = true;
        }
        else{
            $status = false;
        }

        $grade->registration = $status;

        try
        {
            $this->persist($grade);
            return true;
        }
        catch(\Exception $e)
        {
            return false;
        }
    }

    /**
     * returns list of grades where is posible to register (have register=true)
     * @return array
     */
    public function getClasses()
    {
        $grades = $this->findByRegistration(true);

        $selectgrades = array();
        foreach($grades as $grade)
        {
            $selectgrades[$grade->id] = $grade->name;
        }

        return $selectgrades;
    }

	/**
	 * gett more info about the grade - count and avg
	 * @return array
	 */
	public function getAdditional()
	{
		$all = $this->findAll();
		$ret = [];
		foreach($all as $grade)
		{
			$count = $grade->getUsersCount();

			$item = ["count"=>$count,"avg"=>50];
			$ret[$grade->id] = $item;
		}
		return $ret;
	}

	/**
	 * get all the users of the grade
	 * @param $id
	 *
	 * @return array
	 */
	public function getUsers($id)
	{
		$ret = [];
		$users = $this->getTable("grade_user")->select("user.name,user.id")->where("grade_id",$id)->fetchAll();

		foreach($users as $user )
		{
			$ret[$user["id"]] = $user["name"];
		}

		return $ret;
	}

	/**
	 * mark the user as a teacher
	 * @param $data
	 *
	 * @return bool
	 */
	public function addTeacher($data)
	{
		$this->getTable("user")->where(["id"=>$data->user])->update(["user_type"=>"teacher"]);
		if($this->getTable("grade_user")->select("*")->where(["user_id"=>$data->user,"grade_id"=>$data->grade])->fetch() == false)
		{
			$this->getTable("grade_user")->insert(["user_id"=>$data->user,"grade_id"=>$data->grade]);
		}
		return true;
	}

}
