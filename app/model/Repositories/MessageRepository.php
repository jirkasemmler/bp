<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 11.4.15
 * Time: 11:44
 */

namespace App\Model\Repositories;


use YetORM\Repository;
use App\Model\Entities\Message;
use
	Nette\Mail\SendmailMailer;
/**
 * Class MessageRepository
 * @table comment
 * @entity Message
 */
class MessageRepository extends Repository{

	/**
	 * sends msg from $userId to $data[target] - sending means save it to table comment
	 * @param      $data - data from form
	 * @param int  $userId - author
	 * @param bool $newThread - make new thread? true = new thread | false = just reply
	 *
	 * @return \App\Model\Entities\Message
	 */
	public function sendMsg($data,$userId = 0,$newThread = true)
	{
		$msg = new Message(); //create new entity Message();

		if(isset($data["unit"])) //it is about unit
		{
			$msg->unitId = (int) $data["unit"];
		}

		/*
		 * if $userId is not % it means that a registred user is sending a message . In other case it is unregistered and I need its email and name vvvvv
		 */
		if($userId == 0)
		{
			$msg->email = $data["email"];
			$msg->name = $data["name"];
		}
		else //it is registred user -> I can save its ID
		{
			$msg->authorId = (int) $userId;
		}

		//if I dont have the target it means that it is a reply
		if(isset($data["target"]))
		{
			$msg->targetId = (int) $data["target"];
		}

		$msg->text = $data["text"];
		$msg->date = new \DateTime();

		if($newThread) //new thread
		{
			$thread = $this->getTable("thread")->insert(["target_id"=>$msg->targetId,"author_id"=>$userId,"date"=>date("Y-m-d H:i:s")]); //saving thread
			$msg->threadId = $thread->id;
		}
		else //it is a reply in existing thread
		{
			$msg->parentId = (int) $data["parentId"]; //saving parentId
			$msg->threadId = (int) $data["threadId"];
			$this->getTable("thread")->where(["id"=>$msg->threadId])->update(["readTarget"=>false,"readAuthor"=>false,"date"=>date("Y-m-d H:i:s")]);

			$this->setThreadRead($msg->threadId,$msg->authorId,true); //if I reply - it means I already read all the msgs
		}

		$this->persist($msg); //saving msg

		return $msg;
	}

	/**
	 * get list of threads
	 * @param $user
	 *
	 * @return \Nette\Database\Table\Selection
	 */
	public function getThreads($user)
	{
		return $this->getTable("thread")->select("*")->where("author_id = ? OR target_id = ?",$user,$user)->order("date DESC");
	}

	/**
	 * get threads, name of the conversation, number of msgs in it in array structure to render in a template . that all for a user
	 * @param $user
	 *
	 * @return array
	 */
	public function getMsgs($user)
	{
		$threads = $this->getThreads($user); //get list of threads
		$ret = []; //array to return
		foreach($threads as $thread) //iterate on all the threads
		{
			$topic = $this->getTopic($thread->id,$user);

			$ret[] = [
				"id"=>$thread->id,
				"name"=>$topic[0]["name"]." - ".$topic[0]["unitName"]." (".count($topic).")",
				"date"=>$thread["date"], //date of set
				"read"=>$this->isReadBy($user,$thread->id) //is the thread read by the user?
			];

		}
		return $ret;
	}

	/**
	 * return boolean if author or target user read the comment. used for marking a thread as read or unread
	 * @param $user
	 * @param $threadId
	 *
	 * @return mixed
	 */
	public function isReadBy($user,$threadId)
	{
		$thread = $this->getTable("thread")->select("*")->where(["id"=>$threadId])->fetch(); //get the thread
		if($user == $thread["author_id"]) //searching if the user is author or target
		{
			return $thread["readAuthor"];
		}
		elseif($user == $thread["target_id"])
		{
			return $thread["readTarget"];
		}
	}

	/**
	 * get all the msgs by one thread
	 * @param $threadId
	 * @param $me - my user id
	 *
	 * @return array
	 */
	public function getTopic($threadId,$me)
	{
		$current = $this->getBy(["thread_id"=>$threadId,"parent_id"=>NULL]); //get the root
		$child = $this->getByParentId($current->id); //get child of root
		$ret = []; //array to return
		$this->appendResult($ret,$current,$me); //appending child to $ret structure. $ret is passed by pointer (&)
		while($child != NULL) //do until there is no child for parent
		{
			$this->appendResult($ret,$child,$me); //append again
			$child = $this->getByParentId($child->id); //get new child (and without sex :(
		}
		return $ret;
	}

	/**
	 *
	 * @param $ret
	 * @param $obj
	 * @param $me
	 */
	private function appendResult(&$ret,$obj,$me)
	{
		if($obj->authorId == NULL) //I dont have author - it is unregistered user
		{
			$name = $obj->name;
			$email = $obj->email;
		}
		else //the user is registered
		{
			$user = $this->getTable("user")->where(["id"=>$obj->authorId])->fetch(); //get name and email from DB
			$name = $user->name;
			$email = $user->email;
		}
		if($me == $obj->authorId) //because output from this method I render in template i need to know where to render each msg to it looks like iphone chat
		{
			$float = "rightMsg";
		}
		else
		{
			$float = "leftMsg";
		}
		$unitName = "";
		if($obj->unitId != NULL) //I have unitId -> i want to have unit name (first text attribute) in title of thread
		{
			$att = $this->getTable("attribute")->where(["type"=>"input"])->order("order")->fetch();
			$unitName = $this->getTable("unit_attribute")->select("*")->where(["unit"=>$obj->unitId,"attribute_id"=>$att->id])->fetch()->value;
		}
		//append
		$ret[] = ["idComment"=>$obj->id,"text"=>$obj->text,"date"=>$obj->date,"name"=>$name,"email"=>$email,"float"=>$float,"unitId"=>$obj->unitId,"unitName"=>$unitName];
	}

	/**
	 * get number of notread msgs -> to notification
	 * @param $user
	 *
	 * @return int
	 */
	public function getNotReadMsg($user)
	{
		return $this->getTable("thread")->select("*")->where("(target_id = ? && readTarget = ?) || (author_id = ? && readAuthor = ?)",$user,false,$user,false)->count();
	}

	/**
	 * set the $idThread as read
	 * @param        $idThread
	 * @param        $user
	 * @param string $setRead
	 */
	public function setThreadRead($idThread,$user,$setRead = true)
	{
		$toUpdate = "";
		$thread = $this->getTable("thread")->select("*")->where(["id"=>$idThread])->fetch();
		if($user == $thread["author_id"])
		{
			$toUpdate = "readAuthor";
		}
		else if($user == $thread["target_id"])
		{
			$toUpdate = "readTarget";
		}
		$this->getTable("thread")->where(["id"=>$idThread])->update([$toUpdate=>$setRead]);

	}

	/**
	 * get one thread
	 * @param $id
	 *
	 * @return bool|mixed|\Nette\Database\Table\IRow
	 */
	public function getThread($id)
	{
		return $this->getTable("thread")->where(["id"=>$id])->fetch();
	}


}