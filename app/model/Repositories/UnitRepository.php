<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 5.4.15
 * Time: 1:01
 */

namespace App\Model\Repositories;

use App\Model\Entities\Unit;

use YetORM\Repository;
use YetORM\Entity;


/**
 * Class UnitRepository
 * @table unit
 * @entity Unit
 */
class UnitRepository extends Repository {

	/**
	 * find all units in directory
	 * @param $id - id of directory. If null -> return all of them
	 *
	 * @return \YetORM\EntityCollection
	 */
	public function findByDirectory($id)
	{
		if($id == NULL)
		{
			$data = $this->findAll();
		}
		else
		{
			$data = $this->findBy([':unit_directory.directory_id' => $id]);
		}
		return $data;
	}

	/**
	 * returns structure of units for one directory
	 * @param $id - id of directory
	 *
	 * @return array - array of units in struture described in the method
	//	 */
	public function getUnits($id,$own = "public")
	{

		/*
		 * structure of units:
		 * [ {"id" :1,
		 *    "atts": {
		 *              idOfAttribute: value,
		 *              idOfAttribute2: value2
		 *             }
		 *   },...
		 */
		if($id == 0) //get all units
		{
			$units = $this->findAll();
		}
		else
		{
			$units = $this->findByDirectory($id); //get units by the directory
		}

		$ret = []; //array to return

		foreach($units as $unit)
		{
			$retUnit = $this->getUnit($unit->id,$own);
			if($retUnit != NULL)
			{
				$ret[] = $retUnit;
			}
		}
//		print_r($ret);
//		exit();
		return $ret;
	}

	/**
	 * get all the information about unit with all the connections ready to render
	 * @param        $id - id of unit
	 * @param string $own - who is the viewer who want to see the unit?
	 *
	 * @return array|null
	 */
	public function getUnit($id,$own = "public")
	{
		$aclDic = ["my"=>0,"private"=>1,"school"=>2,"teacher"=>2,"public"=>3,"guest"=>3];

		$unit = $this->getById($id);
		$rule = $unit->getDirRule();

		if($aclDic[$rule] < $aclDic[$own]) return NULL;
		$data = $unit->toRecord()->related("unit_attribute"); // all set attributes for one unit

		$atts = [];
		foreach($data as $item)
		{
			$value = $item->value;
			if(is_numeric($item->value))//value is integer so it can be key to another table
			{
				$att = $this->getTable("attribute")->select("*")->where(["id"=>$item->attribute_id])->fetch();
				if($att->type == "select")
				{
					$value = $this->getTable("attribute_option")->where(["id"=>$item->value])->fetch()["name"];
				}
				elseif($att->type == "file")
				{
					$file = $this->getTable("file")->select("*")->where(["id"=>$item->value])->fetch();
					if($file == NULL)
					{
						$fileId = 0;
					}
					else
					{
						$fileId = $file->id;
					}
					$value = ["type"=>"img","id"=>$fileId];
				}
			}
			$atts[$item->attribute_id] = $value;
		}

		$ret = ["id"=>$unit->id,"atts"=>$atts,"taxonomy"=>$unit->taxonomy];
		return $ret;
	}


	/**
	 * insert new unit and its attributes
	 * @param $data
	 *
	 * @return bool - success?
	 */
	public function manage($data)
	{
		if($data["unit"] != 0) //it is edit
		{
			return $this->editUnit($data["unit"],$data);
		}
		else
		{
			return $this->insertUnit($data);
		}

	}

	/**
	 * edits unit
	 * @param $unitId - id odf unit
	 * @param $data - data from form
	 *
	 * @return bool
	 */
	public function editUnit($unitId,$data)
	{
		$unit = $this->getById($unitId);
		$unit->taxonomy = (int) $data->taxonomySelect;
		$this->persist($unit,$data->dirs); //ok unit is saved, lets do attributes of the unit

		foreach($data as $key => $value)
		{
			if(substr($key,0,3) == "att") //it is attribute
			{
				$attId = substr($key,3);
				if($this->getTable("unit_attribute")->where(["unit"=>$unitId,"attribute_id"=>$attId])->fetch() == NULL)
				{
					$this->getTable("unit_attribute")->insert(["unit"=>$unitId,"attribute_id"=>$attId,"value"=>$value]);
				}
				else
				{
					$this->getTable("unit_attribute")->where(["unit"=>$unitId,"attribute_id"=>$attId])->update(["value"=>$value]);
				}

			}
		}
		return true;
	}

	/**
	 * insert new unit
	 * @param $data
	 *
	 * @return bool
	 */
	private function insertUnit($data)
	{
		$unit = new Unit();

		$unit->taxonomy = (int) $data->taxonomySelect;

		if(isset($data["deviceText"])) //device was set as a text - make new one
		{
			$userId = $this->getTable("directory")->where(["id"=>$data->dir])->fetch()->user_id;
			$row = $this->getTable("device")->insert(["user_id"=>$userId,"name"=>$data["deviceText"]]);
			$device = $row->id;
		}
		else
		{
			$device = $data->device; //insert key of old one
		}
		$unit->device = (int) $device;

		$this->persist($unit,$data->dirs); //ok unit is saved, lets do attributes of the unit

		$toInsert = []; //array of attributes of the unit
		foreach($data as $key => $val) //all the sent attributes
		{
			if(substr($key,0,3) == "att") //it is attribute
			{
				$id = substr($key,3);
				$item = ["attribute_id"=>$id,"unit"=>$unit->id,"value"=>$val];
				$toInsert[] = $item;
			}
		}

		try{
			$this->getTable("unit_attribute")->insert($toInsert);
		}
		catch(\Exception $e)
		{
			return false;
		}
		return true;
	}

	/**
	 * delete unit with all their data of attributes
	 * @param $id
	 *
	 * @return bool
	 */
	public function del($id)
	{
		try {
			$attributes = $this->getTable("unit_attribute")->select("*")->where(["unit" => $id]);
			foreach ($attributes as $attribute) {
				if ($attribute->attribute->type == "file") //it is a file, I need to delete it
				{
					$file = $this->getTable("file")->select("*")->where(["id" => $attribute->value])->fetch();

					$prefix = ["..", "data"];

					$urlAr = array_merge($prefix, [$file["user_id"], $file["file_name"]]);
					$url = join("/",$urlAr);
						if (is_file($url))
						{
							unlink($url);
						}
				}
			}

			$unit = $this->getById($id);

			$this->delete($unit);
			return true;
		}
		catch(\Exception $e)
		{
			return false;
		}
	}


	/**
	 * save rating of the unit
	 * @param $data
	 *
	 * @return bool
	 */
	public function saveRating($data)
	{
		$unit = $this->getById($data->unit);
		$unit->comment = $data->comment;
		$unit->rating = (int)$data->rating;
		$this->persist($unit);
		return true;
	}

	/**
	 * get random $count units from database - uses for the index page
	 * @param $count
	 * @param $units
	 *
	 * @return array
	 */
	private function getRandom($count,$units)
	{
		$tmp = [];
		foreach($units as $unit)
		{
			$tmp[] = $unit;
		}

		$max = $units->count()-1;
		$ret = [];
		for ( $i = 0;$i < $count; $i++)
		{
			$ret[] = $tmp[rand(0,$max)]["id"];
		}

		return $ret;
	}

	/**
	 * get name
	 * @param $idUnit
	 *
	 * @return mixed
	 */
	public function getUnitName($idUnit)
	{
		$att = $this->getTable("attribute")->where(["type"=>"input"])->order("order")->fetch();
		$unitName = $this->getTable("unit_attribute")->select("*")->where(["unit"=>$idUnit,"attribute_id"=>$att->id])->fetch()->value;

		return $unitName;
	}


	/**
	 * get all the images of the unit to one array
	 * @param $idUnit
	 *
	 * @return mixed
	 */
	public function getUnitImg($idUnit)
	{
		$att = $this->getTable("attribute")->where(["type"=>"file"])->order("order")->fetch();
		$unitFile = $this->getTable("unit_attribute")->select("*")->where(["unit"=>$idUnit,"attribute_id"=>$att->id])->fetch()->value;

		return $unitFile;
	}

	public function getRandomUnits()
	{
		$allUnits = $this->getTable("unit_directory")->select("unit_id")->where(["directory.type"=>"public"]);

		if($allUnits->count() == 0) return NULL;

		$randomIDs = $this->getRandom(9,$allUnits);
		$randomUnits = $this->getTable("unit")->where(["id"=>$randomIDs]);
		$ret = [];
		foreach ($randomUnits as $unit)
		{
			$item["name"] = $this->getUnitName($unit["id"]);
			$item["img"] = $this->getUnitImg($unit["id"]);
			$item["id"] = $unit["id"];

			$ret [] = $item;
		}
		return $ret;
	}

	/**
	 * rewrite persist method to insert to unit_directory table with relation too - it is because I always update this relation when I use persist($unit)
	 * @param Entity $user
	 * @param null $dirId
	 * @return Entity
	 * @throws \Exception
	 */
	public function persist(Entity $unit, array $dirIds = NULL)
	{
		$this->transaction(function () use ($unit, $dirIds) //the all is a transaction
		{

			parent::persist($unit); //write just the user

			$alreadySet = $this->getTable("unit_directory")->where(["unit_id"=>$unit->id]);
			foreach($alreadySet as $item)
			{
				if($dirIds != NULL)
				{
					if(!in_array($item["directory_id"],$dirIds))
					{
						$this->getTable("unit_directory")->where(["id"=>$item["id"]])->delete();
					}
					else
					{
						$key = array_search($item["directory_id"],$dirIds);
						if($key !== false)
						{
							unset($dirIds[$key]);
						}
					}
				}
			}
			if($dirIds != NULL)
			{
				foreach ($dirIds as $dirId) //this could be more efective but how often do you add or edit unit? nobody cares
				{
					$this->getTable("unit_directory")->insert(["unit_id" => $unit->id, "directory_id" => $dirId]);
				}
			}


		});
		return $unit;
	}

	/**
	 * get all dirs of une unit
	 * @param $unit
	 *
	 * @return array
	 */
	public function getUnitsDirs($unit)
	{
		$ret = [];
		$dirs = $this->getTable("unit_directory")->where(["unit_id"=>$unit]);
		foreach($dirs as $dir)
		{
			$ret[] = $dir["directory_id"];
		}
		return $ret;
	}

	/**
	 * search in unit due the searching data - I can search by full text in all the atts or in detail separeted
	 * @param $data
	 * @param $own
	 *
	 * @return array
	 */
	public function search( $data,$own)
	{
		if(isset($data["user"]))
		{
			$somethingFound = false;
			$units = [];

			foreach($data as $key => $val) //for all the data
			{
				if($val == "" OR $val == "0") continue;

				$somethingFound = true;
				if(substr($key, 0, 3) == "att") //it is input value
				{
					$regexp = "REGEXP '[[:<:]]" . addslashes(strtoupper($val)) . "[[:>:]]'"; //creating regexp for fulltext searching

					$unitsA = $this->getTable("unit_attribute")->select("*")->where("MATCH(value) AGAINST ( ? IN BOOLEAN MODE) AND attribute_id = ?",$regexp,substr($key,3))->fetchAll();
					foreach($unitsA as $unitA)
					{
						$units[] = ["unit"=>$unitA["unit"]];
					}
				}
				else if(substr($key, 0, 3) == "sel") //it is input value
				{
					$regexp = "REGEXP '[[:<:]]" . addslashes(strtoupper($val)) . "[[:>:]]'";

					$unitsA = $this->getTable("unit_attribute")->select("*")->where("MATCH(value) AGAINST ( ? IN BOOLEAN MODE) AND attribute_id = ?", $regexp, substr($key, 3))->fetchAll();
					foreach ($unitsA as $unitA) {
						$keys[] = $unitA["unit"];
					}
				}
			}
			if(!$somethingFound) // i didnt found anything, lets return everything
			{
				return $this->getUnits(0); //get all units
			}
		}
		else if(!isset($data["search"]) || ($data["search"] == "" && count($data) == 1))
			{
				return $this->getUnits(0); //get all units
			}
		else
		{
			$searchExp = $data["search"]; //it is just from search form

			$regexp = "REGEXP '[[:<:]]" . addslashes(strtoupper($searchExp)) . "[[:>:]]'";

			$selects = $this->getTable("attribute_option")->where("MATCH(name) AGAINST ( ? IN BOOLEAN MODE)",$regexp);

			$selectAtts = [0];
			foreach($selects as $select)
			{
				$selectAtts[] = $select->attribute["id"];
			}

			$units = $this->getTable("unit_attribute")->select("unit")->where("value = ? OR attribute_id IN ?" ,$searchExp,$selectAtts); //cares about values in select options and attributes

		}

		$ret = [];

		foreach($units as $unit)
		{
			$ret[] = $this->getUnit($unit["unit"],$own);
		}
		return $ret;
	}
}