<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 14.3.15
 * Time: 22:09
 */

namespace App\Model\Repositories;
use YetORM\Repository;
use App\Model\Entities\Setting;
/**
 * Class SettingRepository
 * @table setting
 * @entity Setting
 */
class SettingRepository extends Repository{

	public function getValue($key)
	{
		return $this->getByKey($key)->value;
	}
}