<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 31.3.15
 * Time: 22:04
 */

namespace App\Model\Repositories;

use App\Model\Entities\File;

use YetORM\Repository;


/**
 * Class FileRepository
 * @table file
 * @entity File
 */
class FileRepository extends Repository{

	/**
	 * saves link to file to database
	 * @param $name - name of file
	 * @param $userId - id of user
	 *
	 * @return \App\Model\Entities\File
	 */
	public function insertFile($name,$userId)
	{
		$file = new File();
		$file->userId = $userId;
		$file->name = $name;
		$file->date = new \DateTime();

		$this->persist($file); //saving

		return $file; //Return entity
	}

	/**
	 * file is checked for plagiarism so set it as checked
	 * @param $file
	 */
	public function setAsChecked($file)
	{
		$file->checked = true;
		$this->persist($file);
	}
}