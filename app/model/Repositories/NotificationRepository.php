<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 11.4.15
 * Time: 11:44
 */

namespace App\Model\Repositories;


use YetORM\Repository;
use App\Model\Entities\Notification;

use App\Model\Repositories\UnitRepository;
use App\Model\Repositories\DirectoryRepository;
use App\Model\Repositories\GradeRepository;

use    Nette\Mail\Message,
	Nette\Mail\SendmailMailer;

/**
 * Class NotificationRepository
 * @table notification
 * @entity Notification
 */
class NotificationRepository extends Repository{


//	public $unitRepository;
	/**
	 * sets repositories to use
	 * @param \App\Model\Repositories\UnitRepository $userRepository
	 */

	/**
	 * generate notification for rating of unit
	 * @param $data - date from rating
	 *
	 * @return bool
	 */
	public function ratingDir($data)
	{
		$not = new Notification();

		$dir = $this->getTable("directory")->select("*")->where(["id"=>$data->dir])->fetch();


		$not->date = new \DateTime();
		$not->userId = $dir["user_id"];
		$not->eventId = (int)$data->dir;
		$not->text = "Váš herbář <b>".$dir["name"]."</b> byl ohodnocena učitelem s následujícím výsledkem:
		<table><tr>
					<td>Procentuální úspěch:</td>
					<td><b>".$dir["rating"]."%</b></td>
			</tr>
				<tr>
					<td>Detailní komentář:</td>
					<td><b>".$dir["comment"]."</b></td>
				</tr><br>
				</table>"; //text for mail
		$not->type = "dirRating"; //type of rating

		$this->persist($not); //saving rating
		return true;
	}

	/**
	 * generate notification for rating of unit
	 * @param $data - date from rating
	 *
	 * @return bool
	 */
	public function rating($data)
	{
		$not = new Notification();

		$att = $this->getTable("attribute")->where(["type"=>"input"])->order("order")->fetch(); //get att what to use as name of unit
		$unit = $this->getTable("unit")->select("*")->where(["id"=>$data->unit])->fetch(); //get unit which was rated
		$not->userId = $unit->related("unit_directory")->fetch()->directory->user_id;; //set unit for rating

		$title = $this->getTable("unit_attribute")->select("*")->where(["unit"=>$unit->id,"attribute_id"=>$att->id])->fetch()->value;

		$not->date = new \DateTime();
		$not->eventId = (int)$data->unit;
		$not->text = "Vaše položka <b>".$title."</b> byla hodnocena učitelem s následujícím výsledkem:
		<table><tr>
					<td>Procentuální úspěch:</td>
					<td><b>".$unit->rating."%</b></td>
			</tr>
				<tr>
					<td>Detailní komentář:</td>
					<td><b>".$unit->teacher_comment."</b></td>
				</tr><br>
				</table>"; //text for mail
		$not->type = "unitRating"; //type of rating

		$this->persist($not); //saving rating
		return true;
	}

	/**
	 * generates notification of plagiarism detection
	 *
	 * @param $idChecked - id of file where was found plagiarism (this file was checked)
	 * @param $ids - array IDs of files (table file) where have been found similar pictures
	 *
	 * @return bool
	 */
	public function plagiarism($idChecked, $ids)
	{
		if(in_array($idChecked,$ids) && count($ids) == 1) return false;
		$gradeRepository = new GradeRepository($this->database);

		$not = new Notification();

		$authordata = $this->getPlagData($idChecked);
		if($authordata == false) return false;
		$gradeId = $this->getTable("grade_user")->where(["user_id"=>$authordata["authorId"]])->fetch()["grade_id"];

		$teacher = $gradeRepository->getById($gradeId)->getTeacher();

		$othersData = [];
		foreach($ids as $id)
		{
			$tmp = $this->getPlagData($id);
			if($tmp == false || $id == $idChecked) continue;
			$othersData[] = $tmp;
		}

		$text = "Byl detekován plagiát záběru <b>".$authordata["zaber"]."</b> položky <b>".$authordata["unitName"]."</b> (autor ".$authordata["author"]." ) shoda s <ul>"; //text for mail

		foreach($othersData as $item)
		{
			$text .= "<li>".$item["zaber"]." - ".$item["unitName"]." (autor ".$item["author"].")</li>";
		}
		$text .= "</ul>";

		$not->date = new \DateTime();
		$not->eventId = (int)$authordata["authorId"];
		$not->userId = $teacher["id"];
		$not->text = $text;
		$not->type = "plagiarism"; //type of rating

		$msg = new Message;
		$msg->setFrom("noreply@herbarium-system.eu")
			->addTo( $teacher["email"])
			->setSubject("Plagiát na herbarium-system.eu")
			->setBody($text);

		$mailer = new SendmailMailer;
		$mailer->send($msg);
		$this->persist($not); //saving rating
		return true;
	}

	/**
	 * @param $idFile
	 *
	 * @return array
	 */
	private function getPlagData($idFile)
	{

		$unitRepository = new UnitRepository($this->database);
		$fileChecked = $this->getTable("file")->where(["id"=>$idFile])->fetch();
		$attribute = $this->getTable("unit_attribute")->where(["unit_attribute.value"=>$idFile, "attribute.type"=>"file"])->fetch();

		$unit = $unitRepository->getById($attribute["unit"]);
		if($unit == NULL)
		{
			return false;
		}
		return ["unitName"=>$unit->getName(),"zaber"=>$attribute->attribute->name,"author"=>$unit->getUserName(),"authorId"=>$fileChecked["user_id"]];
	}


	/**
	 * get list of notifications for $idUser
	 * @param $idUser
	 *
	 * @return array - it is array because I render it straight in template
	 */
	public function getNots($idUser)
	{
		$unitRepository = new UnitRepository($this->database);
		$directoryRepository = new DirectoryRepository($this->database);

		$types = ["unitRating"=>"Hodnocení položky","plagiarism"=>"Nalezení plagiátu", "dirRating"=>"Hodnocení herbáře"];
		$nots = $this->findBy(["user_id"=>$idUser])->orderBy("id DESC"); //get collection of nots
		$ret = [];
		foreach($nots as $not) //iterate
		{
			$item["name"] = $types[$not->type];
			$item["date"] = $not->date;
			$item["content"] = $not->text;
			$item["read"] = $not->read;
			$item["id"] = $not->id;

			switch($not->type) //I can have different types of nots
			{
				case "unitRating": //rating
				{
					$item["title"] = $unitRepository->getByID($not->eventId)->getName();
					$item["content"] = $not->text;
				};break;

				case "dirRating":
				{
					$item["title"] = $directoryRepository->getByID($not->eventId)->name;
					$item["content"] = $not->text;
				};break;

				case "plagiarism": //rating
				{
					$item["title"] = $this->getTable("user")->where(["id",$not->eventId])->fetch()["name"];

					$item["content"] = $not->text;
				};break;
			}
		$ret[] = $item;
		}

		$this->getTable("notification")->where(["user_id"=>$idUser])->update(["read"=>true]); //beacuse I want to show it, user will read it so lets mark them as read
		return $ret;
	}
}