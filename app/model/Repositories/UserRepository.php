<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * User: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 7.3.15
 * Time: 23:01
 */

namespace App\Model\Repositories;


use App\Model\Entities\User;

use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\Passwords;
use YetORM\Repository;
use YetORM\Entity;

/**
 * Class UserRepository
 * @table user
 * @entity User
 */
class UserRepository extends Repository implements IAuthenticator{

	/**
	 * Performs an authentication.
	 *
	 * @param array $credentials - email and password
	 *
	 * @return \Nette\Security\Identity
	 * @throws \Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{

		list($email, $password) = $credentials;

		/** @var User $user */
		$user = $this->getByEmail($email);

		if ($user != NULL && Passwords::verify($password, $user->password)) {

			return new Identity($user->id, $user->type, ["name"=>$user->name]);
		} else {
			throw new AuthenticationException('Kombinace hesla a emailu není správná.');
		}
	}

	/**
	 * get students by grade
	 * @param $id of grade
	 * @return \YetORM\EntityCollection
	 */
	public function getStudents($id)
	{
		if($id == NULL)
		{
			$data = $this->findAll();
		}
		else
		{
			$data = $this->findBy([':grade_user.grade_id' => $id]);
		}

		return $data;
	}

	/**
	 * rewrite persist method to insert to grade_user table with relation too
	 * @param Entity $user
	 * @param null $grade_id
	 * @return Entity
	 * @throws \Exception
	 */
	public function persist(Entity $user, $grade_id = NULL)
	{

		$this->transaction(function () use ($user, $grade_id) //the all is a transaction
		{
			parent::persist($user); //write just the user


			if ($grade_id !== NULL)
			{
				if(!is_array($grade_id)) //just 1:N because student:grades is 1:N
				{
					$row = $this->getTable("grade_user")->select("id")->where(["user_id"=>$user->id])->fetch();

					if($row == NULL)
					{
						$this->getTable('grade_user')->insert(['user_id' => $user->id, 'grade_id' => $grade_id]); //write the relation
					}
					else
					{
						$this->getTable('grade_user')->where(["id"=>$row->id])->update(["grade_id"=>$grade_id]);
					}
				}
				else // for teachers because teachers(table user):grades is N:M
				{

				}

				$directoryName = $user->name." herbář";
				$this->getTable("directory")->insert(["user_id"=>$user->id,"name"=>$directoryName,"status"=>"open","type"=>"school"]);
			}
		});
		return $user;
	}


	/**
	 * returns entity collection of all users
	 */
	public function getAllStudents()
	{
		$data = $this->findAll();

		$ret = [];
		foreach($data as $item)
		{
			$arItem = [];
			$arItem["user"] = $item;

			$arItem["grades"] = $item->toRecord()->related("grade_user");
			$ret[] = $arItem;
		}

		return $ret;
	}

	/**
	 * get more information about the user
	 * @param        $grade
	 * @param string $type
	 *
	 * @return array
	 */
	public function getAdditionalUsers($grade,$type = "public")
	{
		$all = $this->getStudents($grade);
		$ret = [];
		foreach($all as $user)
		{
			$count = $user->getUnitsCount($type);

			$item = ["count"=>$count,"avg"=>50,"dir"=>$user->getFirstDir()];
			$ret[$user->id] = $item;
		}
		return $ret;
	}

	/**
	 * update devices of the user
	 * @param $data - from form
	 * @param $userId usr id
	 *
	 * @return bool
	 */
	public function updateDevices($data,$userId)
	{
		foreach($data as $key => $val)
		{

			if(substr($key,0,3) == "dev")
			{

				$devId = substr($key,3);
				if($val == "") //it is deleted
				{
					$this->getTable("device")->where(["id"=>$devId,"user_id"=>$userId])->delete();
				}
				else
				{
					$this->getTable("device")->where(["id"=>$devId,"user_id"=>$userId])->update(["name"=>$val]);
				}
			}
			else if($key == 0 && $val != "")
			{
				$this->getTable("device")->insert(["user_id"=>$userId,"name"=>$val]);
			}
		}
		return true;
	}

	/**
	 * get all devices of the user
	 * @param $userId
	 *
	 * @return array
	 */
	public function getDevices($userId)
	{
		$ret = [];

		$devices = $this->getTable("device")->where(["user_id"=>$userId]);
		foreach($devices as $device)
		{
			$ret[$device->id] = $device->name;
		}

		return $ret;
	}

	/**
	 * get all users in array to add it to selectbox
	 * @return array
	 */
	public function getAllUsers()
	{
		$tmp = $this->findAll();
		$ret = [];
		foreach($tmp as $user)
		{
			$ret[$user->id] = $user->name;
		}
		return $ret;
	}

	/**
	 * set the user as a student
	 * @param $id - id of user
	 */
	public function setAsStudent($id)
	{
		$user = $this->getByID($id);
		$user->type = "student";
		$this->persist($user);
	}

}
