<?php
/**
 * Created by PhpStorm.
 * Bachelor thesis of Jiri Semmler, FIT VUT 2015
 * Grade: stet (xsemml01@stud.fit.vutbr.cz)
 * Date: 7.3.15
 * Time: 23:01
 */

namespace App\Model\Repositories;


use App\Model\Entities\Taxonomy;

use YetORM\Repository;


/**
 * Class TaxonomyRepository
 * @table taxonomy
 * @entity Taxonomy
 */
class TaxonomyRepository extends Repository
{

	/** vvv TaxonomyTree vvv **/


	/**
	 * returns tree (or subtree) of taxonomy
	 * @param $id - id of root node (if zero -> get the whole tree - node with parent id = 0
	 * @return array
	 */
	public function getTaxonomy($id)
	{
		if($id == 0) // root
		{
			$row = $this->getByParentId($id);
		}
		else
		{
			$row = $this->getByID($id);
		}

		$ar = ["id"=>$row->id,"text"=>$row->name]; //this is the root


		$tmp2 = $this->getTable("taxonomy_type")->select("*")->where(["order"=>1])->fetch();
		if($tmp2 != NUll)
		{
			$ar["text"] .= " - ".$tmp2->name;
		}

		$ar["children"] = $this->getChilds($row->id,2); //get childs
		return $ar;
	}

	/**
	 * returns array of childs of set parentId
	 * @param $parentId
	 * @param $lvl
	 * @param $do - 'set' the type to taxonomy table or 'null' to show . setting to taxonomy table is because of using in other parts of the system
	 * @return array|bool
	 */
	public function getChilds($parentId,$lvl,$do = NULL)
	{
		$intLevel = $lvl;
		$ret = [];
		$row = $this->findByParentId($parentId); //returns entity collection of
		if($row != NULL)
		{
			$lvl++;
			foreach($row as $item)
			{
				$tmp = [];
				$tmp["id"]=$item->id; //sets structure for return
				$tmp["text"]=$item->name;

				$tmp2 = $this->getTable("taxonomy_type")->select("*")->where(["order"=>$intLevel])->fetch();


				if($tmp2 != NULL)
				{
					$tmp["text"] .= " - ".$tmp2->name;

					if($do == 'set')
					{
						$item->type = $tmp2->id;
						$this->persist($item);
					}
				}

				$childs = $this->getChilds($item->id,$lvl,$do); //recursive calling for next childs
				if($childs != false) //because getChilds returns false if it can not find anything
				{
					$tmp["children"] = $childs; //append childs
				}
				$ret[] = $tmp; //apending to the final structure
			}
			return $ret;
		}
		else
		{
			return false;
		}
	}

	/**
	 * updates tree - the column "type" where is connection to table "taxonomy_type"
	 */
	private function updateTree()
	{

	   $row = $this->getByParentId(0);

	   $tmp2 = $this->getTable("taxonomy_type")->select("*")->where(["order"=>1])->fetch();

		$row->type = $tmp2->id;
		$this->persist($row);

		$this->getChilds($row->id,2,'set');
	}

	/**
	 * add or edit node of tree
	 * @param $id - id of node to edit. if zero -> update
	 * @param $parent - id of parent node
	 * @param $text - name of node
	 * @return bool - success||fail
	 */
	public function updateNode($id,$parent,$text)
	{
		if($id[0] == "j") //because new id from jstree is j<rootID>_<count>
		{
			$item = new Taxonomy(); //going to add new node
		}
		else
		{
			$item = $this->getByID($id); //just update old node
		}

		$item->name = $text;
		$item->parentId = (int) $parent; //set up new data

		try
		{
			$this->persist($item); //safe node
			$this->updateTree();
			return true;
		}
		catch(\Exception $e)
		{
			return false;
		}
	}

	/**
	 * recursive delete of nodes - main selected node and all the child nodes
	 * @param $idNode - id of node to delete
	 * @return bool - success|| fail
	 */
	public function deleteNodes($idNode)
	{
		$data = $this->getTaxonomy($idNode); //get tree (array) of nodes to delete
		
		try {
			$this->getTable("taxonomy")->where(["id" => $data["id"]])->delete(); //delete the selected node
			if (isset($data["children"])) // if the selected node has any child nodes
			{
				foreach ($data["children"] as $item) //iteration on childs
				{
					$this->deleteNodes($item); //recursive deleting of childes -> because childs can have next childs... its a tree
				}
			}

			$this->updateTree();
			return true;
		} catch (\Exception $e) {
			return false;
		}

	}

	/** ^^^ taxonomy tree ^^^ **/

	/** vvv taxonomy types vvv **/

	/**
	 * returns list of types in taxonomy. using nette database because taxonomy_type is so small that there is no reason to make an entity for that
	 * @return \Nette\Database\Table\Selection
	 */
	public function getTaxonomyTypes()
	{
		return $this->getTable("taxonomy_type")->select("*")->order("order"); //order is very important because order makes relation between types and tree
	}

	/**
	 * inserts or updates one type ($data). data are just from the form
	 * @param $data - from form
	 */
	public function manageType($data)
	{
		if($data->id == 0) //insert
		{
			$lastItem = $this->getTable("taxonomy_type")->select("*")->order("order DESC")->limit(1)->fetch(); //last number of order in the table
			if($lastItem == NULL) //no rows in table -> position is 1
			{
				$newOrder = 0;
			}
			else
			{
				$newOrder = $lastItem->order+1; //inc by 1 -> next order
			}
			$this->getTable("taxonomy_type")->insert(["name"=>$data->name,"order"=>$newOrder]); //inserting new one
		}
		else
		{
			$this->getTable("taxonomy_type")->where(["id"=>$data->id])->update(["name"=>$data->name]); //updating old one
		}
		$this->updateTree();
	}

	/**
	 * setting new order of rows in table. $data are serialized IDs of rows.
	 * @param $data - serialized IDs of rows (in new order)
	 * @return bool - success|fail
	 */
	public function updateTypesSort($data)
	{
		$i = 1;
		foreach($data as $key => $val) //for each for...
		{
			$this->getTable("taxonomy_type")->where(["id"=>$val])->update(["order"=>$i]); //setting new order
			$i++;
		}
		$this->updateTree();

		return true;
	}

	/**
	 * setting new order of rows in table. $data are serialized IDs of rows.
	 * @param $data - serialized IDs of rows (in new order)
	 * @return bool - success|fail
	 */
	public function updateTypesSortIntern($position)
	{
		$this->database->query("update `taxonomy_type` set `order`=`order`-1 where `order`>$position"); //i dont know how to do it better
		$this->updateTree();

		return true;
	}

	/**
	 * deletes type of taxonomy (table taxonomy_type)
	 * @param $id
	 * @return bool
	 */
	public function deleteType($id)
	{
	   try{
		   $row = $this->getTable("taxonomy_type")->select("*")->where(["id"=>$id])->fetch(); //only one row was deleted
		   $pos = $row->order;
		   $this->getTable("taxonomy_type")->where(["id"=>$id])->delete(); //only one row was deleted
		   $this->updateTypesSortIntern($pos);
			return true;
		}
		catch(\Exception $e)
		{
			return false;
		}
	}

	/**
	 * just give me one type
	 * @param $id
	 * @return bool|mixed|\Nette\Database\Table\IRow
	 */
	public function getType($id)
	{
		return $this->getTable("taxonomy_type")->select("*")->where(["id"=>$id])->fetch();
	}

	/**
	 * get path of the taxonomy tree - from list to the root
	 * @param        $id
	 * @param string $what
	 *
	 * @return array|string
	 */
	public function getPath($id,$what = "string")
	{
		$tmpId = $id;
		$pathAr = []; //array for path
		$tmpObj = $this->getById($tmpId);

		$pathAr[] = $tmpObj->name;
		while($tmpObj->parentId != 0) //goiing from list to the root
		{
			$tmpId = $tmpObj->parentId;
			$pathAr[] = $tmpObj->name;
			$tmpObj = $this->getById($tmpId);
		}

		$pathAr = array_reverse($pathAr); //reverse it
		if($what == "string")
		{
			$ret = join(" / ",$pathAr); //want it as string -> join it
		}
		elseif ($what == "array")
		{
			$ret = $pathAr;
		}
		return $ret;
	}

	
}