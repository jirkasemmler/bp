-- Adminer 4.2.0 MySQL dump

SET NAMES utf8mb4;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `attribute`;
CREATE TABLE `attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `type` enum('select','input','textarea','file') COLLATE utf8_czech_ci NOT NULL,
  `require` tinyint(1) NOT NULL DEFAULT '0',
  `placeholder` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `regexp` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `order` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'true = ok , false = delted',
  `show_in_table` tinyint(1) NOT NULL DEFAULT '0',
  `latin` tinyint(1) NOT NULL DEFAULT '0',
  `search` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `attribute` (`id`, `name`, `type`, `require`, `placeholder`, `regexp`, `order`, `state`, `show_in_table`, `latin`, `search`) VALUES
(1,	'Český název druhu',	'input',	0,	'smetanka lékařská',	'',	1,	1,	1,	0,	1),
(2,	'Latinský název druhu',	'input',	1,	'např. Taraxacum officinale',	'',	3,	1,	1,	1,	1),
(3,	'Český název čeledi',	'input',	1,	'např. krtičníkovité',	'',	2,	1,	1,	0,	1),
(4,	'Latinský název čeledi',	'input',	1,	'např. Scrophulariaceae',	'',	4,	1,	1,	1,	1),
(5,	'Popis stanoviště',	'textarea',	1,	'zelená louka',	'',	16,	1,	0,	0,	0),
(6,	'Záběr - stanoviště',	'file',	1,	'les',	'',	6,	1,	0,	0,	1),
(9,	'Záběr květu',	'file',	1,	'',	'',	5,	1,	0,	0,	0),
(10,	'Celkový záběr rostliny',	'file',	1,	'',	'',	7,	1,	0,	0,	0),
(11,	'Záběr na lodyhu',	'file',	0,	'',	'',	11,	1,	0,	0,	0),
(14,	'Barva květu - druhá',	'select',	1,	'',	'',	8,	1,	0,	0,	1),
(15,	'Záběr na list',	'file',	1,	'',	'',	10,	1,	0,	0,	0),
(16,	'Záběr - další 1',	'file',	0,	'',	'',	12,	1,	0,	0,	0),
(17,	'Záběr - další 2',	'file',	0,	'',	'',	13,	1,	0,	0,	0),
(18,	'Záběr - další 3',	'file',	0,	'',	'',	14,	1,	0,	0,	0),
(19,	'Autor fotografie',	'input',	1,	'',	'',	15,	1,	0,	0,	1),
(20,	'Kdo určil',	'input',	0,	'',	'',	9,	1,	0,	0,	1),
(21,	'Lokalita - zeměpisné určení',	'textarea',	1,	'',	'',	17,	1,	0,	0,	0),
(22,	'Souřadnice lokality',	'input',	1,	'',	'',	18,	1,	0,	0,	0),
(23,	'Poznámka k položce',	'textarea',	0,	'',	'',	19,	1,	0,	0,	0);

DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_id` int(11) DEFAULT NULL COMMENT 'if NULL -> just IM messaging like "hey bro, how are you"',
  `author_id` int(10) DEFAULT NULL COMMENT 'if NULL -> not registered user',
  `target_id` int(10) DEFAULT NULL COMMENT 'if NULL -> not registered user',
  `parent_id` int(11) DEFAULT NULL COMMENT 'if null -> first element of list',
  `thread_id` int(11) DEFAULT NULL,
  `date` datetime NOT NULL,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `unit_id` (`unit_id`),
  KEY `author_id` (`author_id`),
  KEY `target_id` (`target_id`),
  KEY `parent_id` (`parent_id`),
  KEY `thread_id` (`thread_id`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`) ON DELETE CASCADE,
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `comment_ibfk_3` FOREIGN KEY (`target_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `comment_ibfk_4` FOREIGN KEY (`parent_id`) REFERENCES `comment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `comment_ibfk_5` FOREIGN KEY (`thread_id`) REFERENCES `thread` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `comment` (`id`, `unit_id`, `author_id`, `target_id`, `parent_id`, `thread_id`, `date`, `text`, `email`, `name`, `read`) VALUES
(1,	3,	1,	2,	NULL,	1,	'2015-05-18 23:19:26',	'Dobrý den, zajímavá fotka, odkud jste čerpal?',	NULL,	NULL,	0),
(2,	NULL,	1,	NULL,	1,	1,	'2015-05-18 23:23:37',	'Z internetu',	NULL,	NULL,	0),
(3,	2,	NULL,	1,	NULL,	2,	'2015-05-27 11:24:13',	'',	'',	'',	0),
(4,	NULL,	2,	NULL,	2,	1,	'2015-05-29 11:11:51',	'Děkuji',	NULL,	NULL,	0);

DROP TABLE IF EXISTS `device`;
CREATE TABLE `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `name` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `id` (`id`),
  CONSTRAINT `device_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `device` (`id`, `user_id`, `name`) VALUES
(1,	1,	'Nikon 7700'),
(2,	2,	'Olympus X2');

DROP TABLE IF EXISTS `directory`;
CREATE TABLE `directory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `desc` text COLLATE utf8_czech_ci NOT NULL,
  `rating` int(11) DEFAULT NULL COMMENT '0-100% - mark for user',
  `status` enum('open','waiting','closed','deleted') COLLATE utf8_czech_ci NOT NULL COMMENT 'status due to teacher',
  `type` enum('public','private','school') COLLATE utf8_czech_ci NOT NULL COMMENT 'type due to public',
  `comment` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `directory_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `directory` (`id`, `user_id`, `name`, `desc`, `rating`, `status`, `type`, `comment`) VALUES
(1,	1,	'Jiří Semmler herbář',	'',	NULL,	'open',	'public',	''),
(2,	2,	'Radek Popik herbář',	'',	NULL,	'open',	'school',	''),
(3,	2,	'Biologie 2015',	'',	NULL,	'open',	'private',	''),
(4,	3,	'test herbář',	'',	NULL,	'open',	'school',	''),
(5,	3,	'test herbář',	'',	NULL,	'deleted',	'school',	'');

DROP TABLE IF EXISTS `file`;
CREATE TABLE `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `date` int(11) NOT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `id` (`id`),
  CONSTRAINT `file_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `file` (`id`, `user_id`, `file_name`, `date`, `checked`) VALUES
(1,	1,	'o9no42gpem.jpg',	2015,	0),
(2,	1,	'bmfb8z3a1u.jpg',	2015,	0),
(3,	1,	'de935hk30x.jpg',	2015,	0),
(4,	1,	'idthtvjnio.jpg',	2015,	0),
(5,	1,	'364o845uhk.jpg',	2015,	0),
(6,	1,	'ni8hrth0un.jpg',	2015,	0),
(7,	1,	'vdrqj7d392.jpg',	2015,	0),
(8,	1,	'mnlqxi8fq8.jpg',	2015,	0),
(9,	1,	'apmkkvbqir.jpg',	2015,	0),
(10,	2,	'l8g0e257fj.jpg',	2015,	0),
(11,	2,	'xbidg1mkgs.jpg',	2015,	0),
(12,	2,	'd5b7c9vq8b.jpg',	2015,	0),
(13,	2,	'2ahtr3u5kh.jpg',	2015,	0),
(14,	2,	'bjdpejdpcv.jpg',	2015,	0),
(15,	3,	'ac44tgxe20.jpg',	2015,	0),
(16,	3,	'w9ic6npkqb.jpg',	2015,	0);

DELIMITER ;;

CREATE TRIGGER `file_Tr` AFTER DELETE ON `file` FOR EACH ROW
begin 
  delete UA from unit_attribute UA left join attribute on UA.attribute_id = attribute.id  WHERE UA.value=OLD.id AND attribute.type = 'file';
 end;;

DELIMITER ;

DROP TABLE IF EXISTS `grade`;
CREATE TABLE `grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `school_year` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `registration` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `grade` (`id`, `name`, `school_year`, `registration`) VALUES
(1,	'Prima',	'2014/2015',	1),
(2,	'Sekunda',	'2014/2015',	1);

DROP TABLE IF EXISTS `grade_user`;
CREATE TABLE `grade_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `grade_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `grade_id` (`grade_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `grade_user_ibfk_5` FOREIGN KEY (`grade_id`) REFERENCES `grade` (`id`) ON DELETE CASCADE,
  CONSTRAINT `grade_user_ibfk_6` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `grade_user` (`id`, `user_id`, `grade_id`) VALUES
(1,	1,	1),
(2,	2,	1),
(3,	3,	1);

DROP TABLE IF EXISTS `help`;
CREATE TABLE `help` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `help` (`id`, `key`, `title`, `text`) VALUES
(1,	'homepage',	'Vítejte v Herbarium-System',	'<h3>Vítejte v herbáriu!</<h3>\r\nNeváhejte a přihlašte se nebo se registrujte\r\n<ul>\r\n<li><i class=\"fa fa-user-plus\"></i>Registrace</li><li><i class=\"fa fa-sign-in\"></i>Přihlášení</li><li></ul>'),
(2,	'unitsOwner',	'Položky',	'Položky můžete vkládat, prohlížet nebo editovat'),
(3,	'reg',	'Registrace',	'Vyplňte prosím následující položky\r\n<ul>\r\n<li>Jméno a příjmení</li>\r\n<li>Email</li>\r\n<li>Heslo</li>\r\n<li>Třídu - pokud nevíte jakou máte vyplnit, nebo Vaše se v seznamu neukazuje, kontakujte vašeho učitele</li>\r\n</ul>\r\n'),
(4,	'login',	'Přihlášení',	'Vyplňte email a heslo pro přihlášení do systému. Pokud si heslo nepamatujete, požádejte o resetování hesla.'),
(5,	'fogpass',	'Zapomenuté heslo',	'Vyplňte email, který jste zadal(a) jako přihlašovací. Na tento email vám dorazí odkaz pro obnovení hesla.');

DROP TABLE IF EXISTS `notification`;
CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  `event_id` int(11) DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `text` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `notification` (`id`, `user_id`, `read`, `date`, `event_id`, `type`, `text`) VALUES
(1,	2,	1,	'2015-05-18 23:28:23',	3,	'unitRating',	'Vaše položka <b>orsej jarní hlíznatý</b> byla hodnocena učitelem s následujícím výsledkem:\n		<table><tr>\n					<td>Procentuální úspěch:</td>\n					<td><b>44%</b></td>\n			</tr>\n				<tr>\n					<td>Detailní komentář:</td>\n					<td><b>Docela dobré!</b></td>\n				</tr><br>\n				</table>');

DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `setting` (`id`, `key`, `value`, `note`) VALUES
(1,	'registrationTime',	'true',	''),
(2,	'plagServerPath',	'http://localhost/similar/similar.php',	'server for check the plagiarism'),
(3,	'thisURL',	'http://localhost/bp/www/',	'');

DROP TABLE IF EXISTS `taxonomy`;
CREATE TABLE `taxonomy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_parent` (`id_parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `taxonomy` (`id`, `id_parent`, `name`, `type`) VALUES
(1,	0,	'kořen',	2);

DROP TABLE IF EXISTS `taxonomy_type`;
CREATE TABLE `taxonomy_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `taxonomy_type` (`id`, `name`, `order`) VALUES
(1,	'říše',	0),
(2,	'kmen',	1),
(3,	'třída',	2),
(4,	'rod',	3),
(5,	'druh',	4);

DROP TABLE IF EXISTS `thread`;
CREATE TABLE `thread` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(10) DEFAULT NULL,
  `target_id` int(10) NOT NULL,
  `readAuthor` tinyint(1) NOT NULL DEFAULT '0',
  `readTarget` tinyint(1) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL COMMENT 'date of last activity',
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`),
  KEY `target_id` (`target_id`),
  CONSTRAINT `thread_ibfk_3` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `thread_ibfk_4` FOREIGN KEY (`target_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `thread` (`id`, `author_id`, `target_id`, `readAuthor`, `readTarget`, `date`) VALUES
(1,	1,	2,	0,	1,	'2015-05-29 11:11:51'),
(2,	NULL,	1,	0,	1,	'2015-05-27 11:24:13');

DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taxonomy_id` int(11) DEFAULT NULL,
  `rating` int(11) NOT NULL COMMENT '0-100 % rating',
  `teacher_comment` text COLLATE utf8_czech_ci NOT NULL COMMENT 'comment by a teacher',
  `device_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `taxonomy_id` (`taxonomy_id`),
  KEY `device_id` (`device_id`),
  CONSTRAINT `unit_ibfk_1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `unit` (`id`, `taxonomy_id`, `rating`, `teacher_comment`, `device_id`) VALUES
(1,	1,	0,	'',	1),
(2,	0,	0,	'',	1),
(3,	0,	44,	'Docela dobré!',	2);

DROP TABLE IF EXISTS `unit_directory`;
CREATE TABLE `unit_directory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_id` int(11) NOT NULL,
  `directory_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `unit_id` (`unit_id`),
  KEY `directory_id` (`directory_id`),
  CONSTRAINT `unit_directory_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`) ON DELETE CASCADE,
  CONSTRAINT `unit_directory_ibfk_2` FOREIGN KEY (`directory_id`) REFERENCES `directory` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `unit_directory` (`id`, `unit_id`, `directory_id`) VALUES
(1,	1,	1),
(2,	2,	1),
(3,	3,	2),
(4,	3,	3);

DELIMITER ;;

CREATE TRIGGER `unit_directory_au` AFTER UPDATE ON `unit_directory` FOR EACH ROW
begin 
  insert into setting(note) 
  values (NEW.id); 
end;;

DELIMITER ;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL COMMENT 'as John Doe',
  `user_type` enum('student','teacher') COLLATE utf8_czech_ci NOT NULL COMMENT 'set type of account - student or teacher',
  `date_reg` datetime NOT NULL COMMENT 'date of registration',
  `pass_token` varchar(255) COLLATE utf8_czech_ci NOT NULL COMMENT 'token for change pass',
  `avatar` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `avatar` (`avatar`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`avatar`) REFERENCES `file` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `user` (`id`, `email`, `password`, `name`, `user_type`, `date_reg`, `pass_token`, `avatar`) VALUES
(1,	'jirka.semmler@gmail.com',	'$2y$10$yFNWvHADDQGMkoYinF/iJuIECxcHULXsTsqAZ/v27e8RN139EbYbe',	'Jiří Semmler',	'teacher',	'2015-05-18 22:50:17',	'',	1),
(2,	'xsemml01@stud.fit.vutbr.cz',	'$2y$10$5J2i5.mMaXfdrx/Jsu6gC.QdG.639UoExH7bzc4zWE3cQDNUEV3y2',	'Radek Popik',	'student',	'2015-05-18 23:14:13',	'',	NULL),
(3,	'test@test.com',	'$2y$10$Jv5UjhUYonWIxYvNTFdUneC6Wj9Az55qhLUgyWMI7wbbnI4KSrU1e',	'test',	'student',	'2015-05-23 22:24:54',	'',	NULL);

-- 2015-09-19 19:24:10
