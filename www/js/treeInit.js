

function vypis (action,obj) {
    console.log(action+"== id: "+obj.id+" | text: "+obj.text+" | parent : "+obj.parent);
    if(action == "delete")
    {
        $.get("?do=deleteNode",{idNode: obj.id}, function (data) {
            //alert(data.status);
            location.reload();

        });
    }
    else
    {

//alert(obj.text);
    $.get("?do=updateNode",{idNode: obj.id, text:obj.text,id_parent:obj.parent}, function (data) {
        location.reload();
    });
    }
}

function setTree()
{

    if($("#ajax").hasClass("admin"))
    {
        var plugins = ["dnd","contextmenu"];
    }
    else
    {
        var plugins = [];
    }
    // interaction and events
    $('#evts_button').on("click", function () {
        var instance = $('#evts').jstree(true);
        instance.deselect_all();
        instance.select_node('1');
    });
    $('#ajax')
        .on("rename_node.jstree", function (e, data) {
            vypis("rename",data.node);
        })
        .on("delete_node.jstree", function (e, data) {
            //alert("sdfsf");
            vypis("delete",data.node);

        })
        .on("move_node.jstree", function (e, data) {
            vypis("move",data.node);

        })
        .on("select_node.jstree", function (e, data) {
            $("input[name='taxonomySelect']").val(data.node.id);
        })
        .jstree({
            'core' : {
                "check_callback" : true,
                'data' : {
                    "url" : "?do=getTree",
                    "dataType" : "json" // needed only if you do not supply JSON headers
                }
            },
            "plugins": plugins
        });
}

function initGalery()
{
    hs.graphicsDir = "../../img/highslide/graphics/";

    hs.align = 'center';
    hs.transitions = ['expand', 'crossfade'];
    hs.fadeInOut = true;
    hs.dimmingOpacity = 0.8;
    hs.wrapperClassName = 'borderless floating-caption';
    hs.captionEval = 'this.thumb.alt';
    hs.marginLeft = 100; // make room for the thumbstrip
    hs.marginBottom = 80 // make room for the controls and the floating caption
    hs.numberPosition = 'caption';
    hs.lang.number = '%1/%2';

    // Add the slideshow providing the controlbar and the thumbstrip
    hs.addSlideshow({
        //slideshowGroup: 'group1',
        interval: 5000,
        repeat: false,
        useControls: true,
        overlayOptions: {
            className: 'text-controls',
            position: 'bottom center',
            relativeTo: 'viewport',
            offsetX: 50,
            offsetY: -5

        },
        thumbstrip: {
            position: 'middle left',
            mode: 'vertical',
            relativeTo: 'viewport'
        }
    });

    hs.registerOverlay({
        html: '<div class="closebutton" onclick="return hs.close(this)" title="Close"></div>',
        position: 'top right',
        fade: 2 // fading the semi-transparent overlay looks bad in IE
    });
}

