$(function(){
    initNotifications();
    $.nette.init();

    $(".tomodal").click(function(){
        var what = $(this).data("target");
        $('#'+what).modal();
    });

    $(".button").click(function(){
       $(".modal").modal("hide");
    });

    $('[data-toggle="confirmation"]').confirmation();

    dataTable();
    initSort();
    setTree();
    $("#tabs").tabs();

    $('.image-uploader').each(function() {
        var url = $(this).data("url");
        var delurl = $(this).data("delurl");
        $(this).fineUploader({
            debug: false, //for debug
            request: {
                endpoint:  url //where send data to?
            },
            retry: {
                enabled: true
            },
            deleteFile: {
                enabled: true,
                endpoint: delurl //url where to delete the uploaded file
            },
            cancel: {
                enabled: true
            },
            validation: {
                acceptFiles: 'image/*', //set datatypes to enable to upload
                allowedExtensions: ['jpe', 'jpg', 'jpeg', 'png', 'raw']
            }
        })
            .on('complete', function(event, id, fileName, responseJSON){
                $(this).find(".qq-upload-button-selector").hide();
                if(responseJSON.attId != undefined)
                {
                    $("input[name=att"+responseJSON.attId+"]").val(responseJSON.idFile);
                }
            })
        .on('deleteComplete', function(id, isError){
            $(this).find(".qq-upload-button-selector").show();
            });
        });
    $( "a" ).tooltip();

    $( "#myModal" ).on('shown.bs.modal', function (e) {
        $(".closing").click(function(){
            $(".modal").modal("hide");
        });
    });

});

function initNotifications()
{
    $(".notification").click(function(){

        var id = $(this).data("id");
        $("#not"+id).toggle();
    });

}

function sendSearch(e,obj)
{
    if(e.keyCode == 13)
    {
        $("form").submit();
        e.preventDefault();
    }
}
function submitForm()
{
    $("form.search").submit();
}

function setProgress(val)
{
    var percent;
    if(val == "-")
    {
        percent = 0;
    }
    else
    {
        percent = val.split(" ")[0];
    }

    $("#sliderMin").slider({
        range: "min",
        value: percent,
        min: 1,
        max: 100,
        slide: function( event, ui ) {
            $(".sliderValue").val( ui.value);
        }
    });
}
function checkFiles()
{
    $( "input[type=hidden]" ).each(function( index ) {
       if($(this).val() == 0)
       {
           alert("Nejsou vyplnena pole");
           return false;
       }
    });

}
function initSelectAttribute()
{
    $(".hiddenOption").hide();
    $(".selectTypeAttribute").change(function(){

        if( $(this).val() == "select")
        {
            $(".hiddenOption").show();
            initNextOption();
        }
    });
}

function initNextOption()
{
    $("#next_option").click(function(){
 // todo - asi by to chtelo prepsat nejak hezky...
        var int = $("#frm-attributeForm-options1").data("num");
        int++;
        $("#frm-attributeForm-options1").data("num",int);

var data = "<div id='frm-attributeForm-options"+int+"-pair' class='form-group'>"+
            "<label class=\"control-label col-sm-2\" for=\"frm-attributeForm-options\"></label>"+
            "<div class=\"col-sm-10\">"+
            "<input type=\"text\" name=\"options"+int+"\" class=\"hiddenOption form-control medium-text\" placeholder=\"Volba "+int+"\" style=\"display: block;\" id='frm-attributeForm-options"+int+"' >"+
            "</div>"+
            "</div>";
        var int2 = int-1;
        $("#frm-attributeForm-options"+int2+"-pair").after(data);
    });
}
function update()
{
    dataTable();
    $('[data-toggle="confirmation"]').confirmation();
    $(".tomodal").click(function(){
        var what = $(this).data("target");
        $('#'+what).modal();
    });

}
function del(id)
{
    var url = $("#del"+id).data("url");
    $.get(url,{},function(data){
        //alert(data.status);
        if(data.status == "ok")
        {
            $("#row_"+id).hide("slow");
        }

        if($("#del"+id).hasClass("notable"))
        {
            location.reload();
        }
    });
}


var fixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
};

function editPhoto(id)
{
    $("#img"+id).hide();
    $("#add"+id).removeClass("hidden");
    $("#add"+id).show();
}
function initSort()
{
    $("#tableBody").sortable({
        helper: fixHelper,
        items: "tr",
        cursor: 'move',
        opacity: 0.6,
        update: function() {
            sendOrderToServer();
        }
    });
}

function sendOrderToServer() {
    var order = $("#tableBody").sortable("serialize");
    var url = $("#tableBody").data("url");
//alert(url);
    $.get(url+"&"+order,{},function(data)
    {
        if(data.status == "err")
        {
            alert("Bohužel došlo k chybě");
        }

    });
}

function reg(id,statue)
{
    var url = $("#reg"+id).data("url");
    $.get(url,{},function(data){
        if(data.status == "ok")
        {
            if(statue == true)
            {
                $("#reg"+id+"_icon").removeClass("fa-lock");
                $("#reg"+id+"_icon").parent().removeClass("btn-warning");
                $("#reg"+id+"_icon").addClass("fa-unlock");
                $("#reg"+id+"_icon").parent().addClass("btn-success");
            }
            else
            {
                $("#reg"+id+"_icon").removeClass("fa-unlock");
                $("#reg"+id+"_icon").parent().removeClass("btn-success");
                $("#reg"+id+"_icon").addClass("fa-lock");
                $("#reg"+id+"_icon").parent().addClass("btn-warning");
            }
            $(".popover").hide();
        }
    });
}

function dataTable(){
    var asInitVals = [];

     var settings = {
        "aaSorting": [[ 0, "asc" ]],
        "sDom": "<'box-content'<'col-sm-6'f><'col-sm-6 text-right'l><'clearfix'>>rt<'box-content'<'col-sm-6'i><'col-sm-6 text-right'p><'clearfix'>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sSearch": "",
            "sLengthMenu": '_MENU_'
        },
        columnDefs: [{
            targets: "action-header",
            orderable: false
        }],

        bAutoWidth: false
    };


    if($("#datatable").hasClass("notSort"))
    {
        settings["bSort"] =  false;
    }
    var oTable = $('#datatable').dataTable(settings);

    var header_inputs = $("#datatable thead input");
    header_inputs.on('keyup', function(){
        /* Filter on the column (the index) of this element */
        oTable.fnFilter( this.value, header_inputs.index(this) );
    })
        .on('focus', function(){
            if ( this.className == "search_init" ){
                this.className = "";
                this.value = "";
            }
        })
        .on('blur', function (i) {
            if ( this.value == "" ){
                this.className = "search_init";
                this.value = asInitVals[header_inputs.index(this)];
            }
        });
    header_inputs.each( function (i) {
        asInitVals[i] = this.value;
    });
}

function setUrl(url)
{
    window.history.pushState({id:2},'',url);
}